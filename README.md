# FastOTP
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

Simple and secure TOTP token generator application.

It is backed by a library conceived to allow easy GUI reimplementation.

There's an initial GUI implementation using [Iced](https://github.com/hecrj/iced) library in [`application_iced`](application_iced).

## License

[AGPL-3.0-or-later](LICENSE.AGPL)
