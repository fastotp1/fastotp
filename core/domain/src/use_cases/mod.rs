/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! Domain logic that encompasses all the functionality offered by the application

pub mod change_password;
pub mod create_entry;
pub mod delete_entry;
pub mod otp_generation;
pub mod qr_generation;
pub mod unlocking;

use std::sync::Arc;

use async_lock::RwLock as AsyncRwLock;

use crate::repository::{storage, EntriesRepository};

/// A single shared instance of the repository.
pub type SharedRepository = Arc<AsyncRwLock<dyn EntriesRepository>>;

/// A single shared instance of the storage.
pub type SharedStorage = Arc<AsyncRwLock<dyn storage::Encrypted>>;
