/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! This use case ensures the creation of a new [Entry] in a repository

use std::sync::Arc;

use futures::future::BoxFuture;

use crate::{
   entities::entry::{Entry, Id},
   repository,
   use_cases::SharedRepository,
};

/// Occurs when trying to create an [Entry]
///
/// [Entry]: ../../entities/entry/struct.Entry.html
#[derive(Clone, Debug, thiserror::Error, Eq, PartialEq)]
pub enum Error {
   /// [Entry] has an empty issuer
   #[error("issuer mustn't be empty")]
   NoIssuer,

   /// For repository errors
   #[error("error saving into repository")]
   Repository(#[from] repository::Error),
}

pub type EntryCreationResult = Result<Entry, Error>;

pub type EntryFindResult = Result<Option<Entry>, Error>;

/// Used to create new [Entry] in the system
pub struct EntryCreation {
   repository: SharedRepository,
}

impl EntryCreation {
   #[inline]
   pub fn new(repository: SharedRepository) -> Self {
      Self { repository }
   }

   /// Create a new [Entry]. The secret will be cleaned up (trimmed and upper cased)
   pub fn create_entry(&mut self, entry: Entry) -> BoxFuture<'static, EntryCreationResult> {
      let repository = Arc::clone(&self.repository);

      Box::pin(Self::async_create_entry(repository, entry))
   }

   async fn async_create_entry(repository: SharedRepository, entry: Entry) -> EntryCreationResult {
      if entry.issuer.is_empty() {
         return Err(Error::NoIssuer);
      }

      let entry_created = entry.clone();

      Ok(repository
         .write()
         .await
         .save_or_update(entry)
         .await
         .map(move |_| entry_created)?)
   }

   /// Finds an [Entry] given its [Id]
   ///
   /// [EntryId]: ../../entities/entry/struct.EntryId.html
   pub fn get_entry(&self, id: &Id) -> BoxFuture<'static, EntryFindResult> {
      let repository = Arc::clone(&self.repository);

      Box::pin(Self::async_get_entry(repository, *id))
   }

   async fn async_get_entry(repository: SharedRepository, id: Id) -> EntryFindResult {
      Ok(repository.read().await.find(&id).await?)
   }
}

#[cfg(test)]
mod tests {
   use std::{convert::TryInto, sync::Arc, time::Duration};

   use async_lock::RwLock as AsyncRwLock;
   use async_std::task;

   use crate::{
      entities::{
         create_test_entry,
         entry::{Entry, Id},
      },
      repository::InMemory,
      use_cases::{
         create_entry::{EntryCreation, Error},
         otp_generation::DEFAULT_TIME_STEP,
         SharedRepository,
      },
   };

   #[test]
   fn fails_for_entry_with_empty_issuer() {
      task::block_on(async {
         let repository = Arc::new(AsyncRwLock::new(InMemory::new()));
         let mut ec = EntryCreation::new(repository);

         let entry = Entry {
            id: Id::generate(),
            issuer: String::new(),
            description: "a".to_owned(),
            secret: "a".try_into().unwrap(),
            time_step: Duration::from_secs(DEFAULT_TIME_STEP).try_into().unwrap(),
         };

         let actual = ec.create_entry(entry).await;

         assert_eq!(actual, Err(Error::NoIssuer));
      });
   }

   #[test]
   fn creating_a_new_entry_succeeds_and_returns_created_entry() {
      task::block_on(async {
         let repository: SharedRepository = Arc::new(AsyncRwLock::new(InMemory::new()));
         let mut ec = EntryCreation::new(Arc::clone(&repository));

         let entry = create_test_entry(0);
         let expected = create_test_entry(0);

         let actual = ec.create_entry(entry).await;
         let saved = repository.write().await.all().await;

         assert!(actual.is_ok());
         assert_eq!(actual.unwrap(), expected);
         assert!(saved.unwrap().values.contains(&expected));
      });
   }

   #[test]
   fn updating_an_existing_entry_succeeds_and_returns_updated_entry() {
      task::block_on(async {
         let repository: SharedRepository = Arc::new(AsyncRwLock::new(InMemory::new()));
         let mut ec = EntryCreation::new(Arc::clone(&repository));

         let id = Id::generate();
         // New scope to temporarily allow inserting into the repository
         {
            let mut entry = create_test_entry(0);
            entry.id = id;

            assert!(repository.write().await.save_or_update(entry).await.is_ok());
         }

         let mut updated_entry = create_test_entry(1);
         updated_entry.id = id;

         let actual = ec.create_entry(updated_entry.clone()).await;

         let saved = repository.write().await.all().await;

         assert!(actual.is_ok());
         assert_eq!(actual.unwrap(), updated_entry);
         assert!(saved.unwrap().values.contains(&updated_entry));
      });
   }
}
