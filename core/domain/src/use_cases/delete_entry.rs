/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! This use case ensures the deletion of an existing [Entry](super::super::entities::entry::Entry) in the repository

use core::result::Result as StdResult;
use std::sync::Arc;

use futures::future::BoxFuture;

use crate::{entities::entry::Id, repository, use_cases::SharedRepository};

/// Occurs when trying to delete an [Entry](super::super::entities::entry::Entry)
#[derive(Clone, Debug, thiserror::Error, Eq, PartialEq)]
pub enum Error {
   /// For repository errors
   #[error("error deleting from repository")]
   Repository(#[from] repository::Error),
}

pub type Result = StdResult<(), Error>;

/// Used to delete an [Entry](super::super::entities::entry::Entry) from the application
pub struct EntryDeletion {
   repository: SharedRepository,
}

impl EntryDeletion {
   #[inline]
   pub fn new(repository: SharedRepository) -> Self {
      Self { repository }
   }

   pub fn delete_entry(&mut self, id: &Id) -> BoxFuture<'static, Result> {
      let repository = Arc::clone(&self.repository);

      Box::pin(Self::async_delete_entry(repository, *id))
   }

   async fn async_delete_entry(repository: SharedRepository, id: Id) -> Result {
      Ok(repository.write().await.delete(&id).await?)
   }
}

#[cfg(test)]
mod tests {
   use std::sync::Arc;

   use async_lock::RwLock as AsyncRwLock;
   use async_std::task;

   use crate::{
      entities::{create_test_entry, entry::Id},
      repository,
      repository::InMemory,
      use_cases::{
         delete_entry::{EntryDeletion, Error},
         SharedRepository,
      },
   };

   #[test]
   fn for_non_existing_entry_it_fails_with_doesnt_exist_error() {
      task::block_on(async {
         let repository = Arc::new(AsyncRwLock::new(InMemory::new()));
         let mut ed = EntryDeletion::new(repository);

         let actual = ed.delete_entry(&Id::generate()).await;

         assert_eq!(actual, Err(Error::Repository(repository::Error::EntryDoesntExist)));
      });
   }

   #[test]
   fn deleting_an_existing_entry_succeeds() {
      task::block_on(async {
         let repository: SharedRepository = Arc::new(AsyncRwLock::new(InMemory::new()));
         let mut ed = EntryDeletion::new(Arc::clone(&repository));

         let entry_id = Id::generate();
         // Create new scope to temporarily allow inserting into the repository
         {
            let mut entry = create_test_entry(0);
            entry.id = entry_id;

            assert!(repository.write().await.save_or_update(entry).await.is_ok());
         }

         let actual = ed.delete_entry(&entry_id).await;

         let saved = repository.write().await.all().await;

         assert!(actual.is_ok(), "{:?}", actual);
         assert!(saved.unwrap().values.is_empty());
      });
   }
}
