/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! See [TimeSinceEpochStream]

use core::{
   future::Future,
   pin::Pin,
   task::{Context, Poll},
   time::Duration,
};
use std::time::{SystemTime, SystemTimeError};

use futures::stream::Stream;
use futures_timer::Delay;

/// This stream emits every second the elapsed time since Unix epoch.\
/// If the system time is set before the Unix epoch it will return an error
#[must_use = "streams do nothing unless polled"]
pub struct TimeSinceEpochStream {
   refresh_interval: Duration,
   refresh_delay: Delay,
}

impl TimeSinceEpochStream {
   pub fn new(refresh_interval: Duration) -> Self {
      Self {
         refresh_interval,
         refresh_delay: Delay::new(Duration::new(0, 0)),
      }
   }
}

impl Stream for TimeSinceEpochStream {
   type Item = Result<Duration, SystemTimeError>;

   fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
      if Pin::new(&mut self.refresh_delay).poll(cx).is_pending() {
         return Poll::Pending;
      }

      let interval = self.refresh_interval;
      self.refresh_delay.reset(interval);

      Poll::Ready(Some(SystemTime::now().duration_since(SystemTime::UNIX_EPOCH)))
   }
}

#[cfg(test)]
mod tests {
   use std::time::Duration;

   use async_std::task;
   use chrono::Local;
   use futures::{join, select_biased, FutureExt, StreamExt};
   use futures_timer::Delay;

   use crate::use_cases::otp_generation::time_since_epoch_stream::TimeSinceEpochStream;

   #[test]
   #[cfg_attr(not(feature = "manual-test"), ignore)]
   #[allow(clippy::print_stdout, clippy::use_debug)]
   fn manual_test() {
      task::block_on(async {
         let handle_concurrent_task = task::spawn(async {
            println!("Testing...");
            task::sleep(Duration::from_millis(500)).await;
         });

         let handle_stream = task::spawn(async {
            let refresh_interval = Duration::from_millis(250);
            let mut interval = TimeSinceEpochStream::new(refresh_interval);
            let mut delay = Delay::new(Duration::from_secs(3)).fuse();

            loop {
               let mut event = interval.next().fuse();

               select_biased! {
                   _ = delay => break,
                   maybe_event = event => {
                       match maybe_event {
                           Some(v) => println!("{:?} {:?}", Local::now().time(), v),
                           None => break,
                       }
                   }
               };
            }
         });

         join!(handle_stream, handle_concurrent_task);
      });
   }
}
