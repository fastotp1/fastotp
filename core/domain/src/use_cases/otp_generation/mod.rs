/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! This use case contains the main logic for generating TOTP codes at predictable time intervals
//! and taking into account repository updates

use core::{
   fmt::{Display, Formatter},
   hash::{Hash, Hasher},
   result::Result as StdResult,
   time::Duration,
};
use std::{fmt, sync::Arc, time::SystemTimeError};

use futures::{stream::BoxStream, FutureExt, Stream, StreamExt};

use crate::{
   entities::entry::{Entries, Entry, Id},
   use_cases::{
      otp_generation::{filtering::filter, time_since_epoch_stream::TimeSinceEpochStream},
      SharedRepository,
   },
};

mod filtering;
mod otp_calculation;
mod time_since_epoch_stream;

pub use otp_calculation::OtpCode;

/// Occurs when trying to create an [Entry]
#[derive(Clone, Debug, Hash, thiserror::Error, Eq, PartialEq)]
pub enum Error {
   /// The current time is set before Unix epoch
   #[error("system clock is before Unix epoch")]
   CurrentTimeBeforeUnixEpoch,

   /// For totp calculation errors
   #[error("totp calculation error '{0}'")]
   CalculatingOtp(String),
}

/// The default time step in seconds for the calculation of the TOTPs
pub const DEFAULT_TIME_STEP: u64 = 30;

pub type Result = StdResult<GeneratedOtps, Error>;

/// Used to query stored [Entries] to be able to generate a stream of TOTP values for all queried [Entries]
pub struct OtpGeneration {
   repository: SharedRepository,
}

impl OtpGeneration {
   #[inline]
   pub fn new(repository: SharedRepository) -> Self {
      Self { repository }
   }

   /// Calculates a periodic stream of totp codes given a filtering pattern.
   /// The update interval is set to 14ms.
   pub fn generate_otps<'stream>(&self, filter_stream: BoxStream<'stream, String>) -> BoxStream<'stream, Result> {
      let repository = Arc::clone(&self.repository);

      Box::pin(
         Self::async_generate_otps(repository, filter_stream)
            .into_stream()
            .flatten(),
      )
   }

   async fn async_generate_otps(
      repository: SharedRepository,
      filter_stream: BoxStream<'_, String>,
   ) -> impl Stream<Item = Result> + Send + '_ {
      use streams_utils::{combine_latest, distinct_until_changed::DistinctUntilChangeExt};

      const UPDATE_INTERVAL: Duration = Duration::from_millis(14);

      let repository_changes = repository.read().await.changes();

      let filtered_entries = combine_latest::combine_latest(repository_changes, filter_stream).map(
         |(entries, filter_pattern): (Entries, String)| {
            let filter_ref = &filter_pattern;
            if filter_ref.is_empty() {
               let mut sorted_entries = entries.values.into_iter().collect::<Vec<Entry>>();
               sorted_entries.sort_by(|a, b| a.partial_cmp(b).unwrap());
               sorted_entries
            }
            else {
               let prefiltered_entries = entries.values.iter().collect::<Vec<_>>();

               filter(filter_ref, &prefiltered_entries)
                  .into_iter()
                  .cloned()
                  .collect::<Vec<Entry>>()
            }
         },
      );

      let time_since_epoch_stream = TimeSinceEpochStream::new(UPDATE_INTERVAL);

      let generated_otps = combine_latest::combine_latest(filtered_entries, time_since_epoch_stream).map(
         |(filtered_repository, time_since_epoch): (Vec<Entry>, StdResult<Duration, SystemTimeError>)| {
            time_since_epoch
               .map_err(|_| Error::CurrentTimeBeforeUnixEpoch)
               .map(|time| {
                  let codes = filtered_repository
                     .into_iter()
                     .map(|entry| {
                        let otp = otp_calculation::calculate_totp(&entry.secret, entry.time_step, time)
                           .map_err(|e| Error::CalculatingOtp(format!("{e:?}")));

                        GeneratedOtp {
                           entry_id: entry.id,
                           issuer: entry.issuer,
                           description: entry.description,
                           otp,
                        }
                     })
                     .collect();

                  let max_time_step = Duration::from_secs(DEFAULT_TIME_STEP);
                  let time_step_in_ms = max_time_step.as_millis();
                  let expires_in = time_step_in_ms - (time.as_millis() % time_step_in_ms);
                  let expires_in = Duration::from_millis(expires_in as u64);

                  GeneratedOtps {
                     max_time_step,
                     expires_in,
                     otps: codes,
                  }
               })
         },
      );

      generated_otps.distinct_until_changed()
   }
}

/// Currently generated TOTPs according to current filter and time step
#[derive(Clone, Debug, Eq)]
pub struct GeneratedOtps {
   /// Max time step for all totp codes
   pub max_time_step: Duration,

   /// How many seconds left until re-calculation of codes
   pub expires_in: Duration,

   /// TOTP codes generated
   pub otps: Vec<GeneratedOtp>,
}

impl PartialEq for GeneratedOtps {
   fn eq(&self, other: &Self) -> bool {
      self.max_time_step == other.max_time_step
         && self.otps == other.otps
         && self.expires_in.as_millis() == other.expires_in.as_millis()
   }
}

impl Hash for GeneratedOtps {
   fn hash<H: Hasher>(&self, state: &mut H) {
      self.max_time_step.as_millis().hash(state);
      self.expires_in.as_millis().hash(state);
      self.otps.hash(state);
   }
}

/// Represents a calculated TOTP value
#[derive(Clone, Debug, Hash, Eq, PartialEq)]
pub struct GeneratedOtp {
   /// Id of the [Entry]
   pub entry_id: Id,

   /// Description used of the TOTP `Entry`
   pub issuer: String,

   /// Description used for the TOTP `Entry`
   pub description: String,

   /// Result of generating the totp value
   pub otp: StdResult<OtpCode, Error>,
}

impl Display for GeneratedOtp {
   fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
      write!(
         f,
         "{}{} {}",
         self.issuer,
         if self.description.is_empty() {
            String::new()
         }
         else {
            format!("({})", self.description)
         },
         self
            .otp
            .as_ref()
            .map_or_else(|_| "error".to_owned(), |&otp_v| otp_v.to_string()),
      )
   }
}

#[cfg(test)]
mod tests {
   use std::{convert::TryInto, sync::Arc, time::Duration};

   use async_lock::RwLock as AsyncRwLock;
   use async_std::task;
   use chrono::Local;
   use futures::{join, StreamExt};
   use futures_signals::signal::{Mutable, SignalExt};

   use crate::{
      entities::entry::{Entry, Id},
      repository::InMemory,
      use_cases::{
         otp_generation::{OtpGeneration, DEFAULT_TIME_STEP},
         SharedRepository,
      },
   };

   /// Marked as a manual test since correct behavior is validated visually
   #[test]
   #[cfg_attr(not(feature = "manual-test"), ignore)]
   #[allow(clippy::print_stdout, clippy::use_debug)]
   fn manual_test() {
      task::block_on(async {
         let entries = vec![
            Entry {
               id: Id::generate(),
               issuer: String::from("64 chars no spaces"),
               description: String::from(""),
               secret: "AAEMBYIAC6ETKSOWZQNCVGMQVIS3SHPUGG2FNNWATUUP3LM4B5UVY4RCYL736AAA"
                  .try_into()
                  .unwrap(),
               time_step: Duration::from_secs(DEFAULT_TIME_STEP).try_into().unwrap(),
            },
            Entry {
               id: Id::generate(),
               issuer: String::from("64 chars lowercase with spaces"),
               description: String::from(""),
               secret: "aaem byia c6etk sowz qncv gmqv is3s hpug g2fn nwat uup3 lm4b 5uvy 4rcy l736 aaa"
                  .try_into()
                  .unwrap(),
               time_step: Duration::from_secs(DEFAULT_TIME_STEP).try_into().unwrap(),
            },
            Entry {
               id: Id::generate(),
               issuer: String::from("32 chars no spaces"),
               description: String::from(""),
               secret: "X25KJJES4BZREEJBYXXP7PV6UUJ45ZDH".try_into().unwrap(),
               time_step: Duration::from_secs(DEFAULT_TIME_STEP).try_into().unwrap(),
            },
            Entry {
               id: Id::generate(),
               issuer: String::from("32 chars lowercase with spaces"),
               description: String::from(""),
               secret: "x25k jjes 4bzr eejb yxxp 7pv6 uuj4 5zdh".try_into().unwrap(),
               time_step: Duration::from_secs(DEFAULT_TIME_STEP).try_into().unwrap(),
            },
            Entry {
               id: Id::generate(),
               issuer: String::from("26 chars no spaces"),
               description: String::from(""),
               secret: "CEJ35PV6XKV55UEVTP7PCFQSUQ".try_into().unwrap(),
               time_step: Duration::from_secs(DEFAULT_TIME_STEP).try_into().unwrap(),
            },
            Entry {
               id: Id::generate(),
               issuer: String::from("26 chars lowercase with spaces"),
               description: String::from(""),
               secret: "cej3 5pv6 xkv5 5uev tp7p cfqs uq".try_into().unwrap(),
               time_step: Duration::from_secs(DEFAULT_TIME_STEP).try_into().unwrap(),
            },
            Entry {
               id: Id::generate(),
               issuer: String::from("16 chars no spaces"),
               description: String::from(""),
               secret: "33PAUENL5PSR477T".try_into().unwrap(),
               time_step: Duration::from_secs(DEFAULT_TIME_STEP).try_into().unwrap(),
            },
            Entry {
               id: Id::generate(),
               issuer: String::from("16 chars lowercase with spaces"),
               description: String::from(""),
               secret: "33pa uenl 5psr 477t".try_into().unwrap(),
               time_step: Duration::from_secs(DEFAULT_TIME_STEP).try_into().unwrap(),
            },
         ];

         let repository: SharedRepository = Arc::new(AsyncRwLock::new(InMemory::new()));

         let repo = Arc::clone(&repository);
         let repo_events_simulation = task::spawn(async move {
            for entry in entries.into_iter() {
               task::sleep(Duration::from_millis(1500)).await;
               repo.write().await.save_or_update(entry.clone());
               println!(
                  "------------------------------------ Added: '{}({})' ------------------------------------",
                  &entry.issuer, &entry.description
               );
            }
         });

         let mutable = Mutable::new(String::new());
         let filter = Box::pin(mutable.signal_cloned().to_stream());
         let filter_events_simulation = task::spawn(async move {
            let filters = vec!["aws", "b", "bi", "bit", "g", "gnu", "p", "per", "fync"];
            let mut i = 0;
            loop {
               let filter = filters[i];
               task::sleep(Duration::from_millis(2000)).await;
               mutable.set(filter.to_owned());
               println!(
                  "------------------------------------ New filter: '{}' ------------------------------------",
                  &filter
               );
               i = (i + 1) % filters.len();
            }
         });

         let otp_generation = OtpGeneration::new(Arc::clone(&repository));
         let otp_generation_simulation = task::spawn(async move {
            let mut otp_stream = otp_generation.generate_otps(filter);
            loop {
               let item = otp_stream.next().await;
               match item {
                  None => println!("[{:?}] None", Local::now().time()),
                  Some(otps) => println!("[{:?}] {:?}", Local::now().time(), otps),
               }
            }
         });

         join!(
            repo_events_simulation,
            filter_events_simulation,
            otp_generation_simulation
         );
      });
   }
}
