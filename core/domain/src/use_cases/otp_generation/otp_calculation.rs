/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! Available functions to calculate TOTP codes

use core::{convert::TryInto, time::Duration};

use hmac::{Hmac, Mac};
use sha1::Sha1;

use crate::entities::entry::{Secret, TimeStep};

/// `Error` Represents possible errors generated while generating an TOTP.
#[derive(Clone, Debug, thiserror::Error, Eq, PartialEq)]
pub enum Error {
   /// If the generated HMAC can't be encoded
   #[error("invalid digest provided: {0:?}")]
   HmacEncoding(Vec<u8>),

   /// For an invalid key length
   #[error("invalid secret length (greater than 2^64)")]
   HmacKeyLength,
}

/// Transforms an HMAC-SHA1 digest into a 6-digit integer.
fn digest_to_6_digits(digest: &[u8]) -> Result<u32, Error> {
   let hmac_encoding_error = || Error::HmacEncoding(Vec::from(digest));

   let offset = digest
      .last()
      .map(|&x| (x & 0xf) as usize)
      .ok_or_else(hmac_encoding_error)?;

   let bytes_slice = digest.get(offset..offset + 4).ok_or_else(hmac_encoding_error)?;

   let code_bytes: [u8; 4] = bytes_slice.try_into().map_err(|_| hmac_encoding_error())?;

   let code_with_sign_bit = u32::from_be_bytes(code_bytes);
   let code_without_sign_bit = code_with_sign_bit & 0x7fff_ffff;
   let code = code_without_sign_bit % 1_000_000;

   Ok(code)
}

/// Calculates the HMAC-SHA1 for the given secret and counter.
fn calculate_hmac_sha1(decoded_secret: &[u8], counter: u64) -> Result<Vec<u8>, Error> {
   let mut mac = Hmac::<Sha1>::new_from_slice(decoded_secret).map_err(|_| Error::HmacKeyLength)?;
   mac.update(&counter.to_be_bytes());
   let bytes = mac.finalize().into_bytes();
   Ok(bytes.into_iter().collect())
}

/// Calculates the HOTP given a base32 encoded secret and a counter value according to [RFC4226](https://tools.ietf.org/html/rfc4226) specification
fn calculate_hotp(secret: &Secret, counter: u64) -> Result<u32, Error> {
   // Safely unwrap since Secret ensures valid Base32 encoded data
   let decoded = base32::decode(base32::Alphabet::RFC4648 { padding: false }, secret.as_ref()).unwrap();

   let signed = calculate_hmac_sha1(decoded.as_slice(), counter)?;

   digest_to_6_digits(signed.as_slice())
}

/// TOTP code generated
pub type OtpCode = u32;

/// Calculates the TOTP given a time step and the time since UNIX epoch according to [RFC6238](https://tools.ietf.org/html/rfc6238) specification
pub fn calculate_totp(secret: &Secret, time_step: TimeStep, time_since_epoch: Duration) -> Result<OtpCode, Error> {
   let time_step: Duration = time_step.into();
   calculate_hotp(secret, time_since_epoch.as_secs() / time_step.as_secs())
}

#[cfg(test)]
mod tests {
   use std::{convert::TryInto, time::Duration};

   use super::calculate_totp;
   use crate::{
      entities::entry::{Secret, TimeStep},
      use_cases::otp_generation::{otp_calculation::OtpCode, DEFAULT_TIME_STEP},
   };

   #[test]
   fn totp_is_calculated_for_different_valid_secrets() {
      struct TestCase<'a> {
         name: &'a str,
         secret: Secret,
         time_step: TimeStep,
         time_since_epoch: Duration,
         expected_totp: OtpCode,
      }

      let cases = vec![
         TestCase {
            name: "64 chars no spaces",
            secret: "AAEMBYIAC6ETKSOWZQNCVGMQVIS3SHPUGG2FNNWATUUP3LM4B5UVY4RCYL736AAA"
               .try_into()
               .unwrap(),
            time_step: Duration::from_secs(DEFAULT_TIME_STEP).try_into().unwrap(),
            time_since_epoch: Duration::from_secs(1585956366),
            expected_totp: 188548,
         },
         TestCase {
            name: "64 chars lowercase with spaces",
            secret: "aaem byia c6etk sowz qncv gmqv is3s hpug g2fn nwat uup3 lm4b 5uvy 4rcy l736 aaa"
               .try_into()
               .unwrap(),
            time_step: Duration::from_secs(DEFAULT_TIME_STEP).try_into().unwrap(),
            time_since_epoch: Duration::from_secs(1585956366),
            expected_totp: 188548,
         },
         TestCase {
            name: "32 chars no spaces",
            secret: "X25KJJES4BZREEJBYXXP7PV6UUJ45ZDH".try_into().unwrap(),
            time_step: Duration::from_secs(DEFAULT_TIME_STEP).try_into().unwrap(),
            time_since_epoch: Duration::from_secs(1585956366),
            expected_totp: 513561,
         },
         TestCase {
            name: "32 chars lowercase with spaces",
            secret: "x25k jjes 4bzr eejb yxxp 7pv6 uuj4 5zdh".try_into().unwrap(),
            time_step: Duration::from_secs(DEFAULT_TIME_STEP).try_into().unwrap(),
            time_since_epoch: Duration::from_secs(1585956366),
            expected_totp: 513561,
         },
         TestCase {
            name: "26 chars no spaces",
            secret: "CEJ35PV6XKV55UEVTP7PCFQSUQ".try_into().unwrap(),
            time_step: Duration::from_secs(DEFAULT_TIME_STEP).try_into().unwrap(),
            time_since_epoch: Duration::from_secs(1585956366),
            expected_totp: 200602,
         },
         TestCase {
            name: "26 chars lowercase with spaces",
            secret: "cej3 5pv6 xkv5 5uev tp7p cfqs uq".try_into().unwrap(),
            time_step: Duration::from_secs(DEFAULT_TIME_STEP).try_into().unwrap(),
            time_since_epoch: Duration::from_secs(1585956366),
            expected_totp: 200602,
         },
         TestCase {
            name: "16 chars no spaces",
            secret: "33PAUENL5PSR477T".try_into().unwrap(),
            time_step: Duration::from_secs(DEFAULT_TIME_STEP).try_into().unwrap(),
            time_since_epoch: Duration::from_secs(1585956366),
            expected_totp: 834828,
         },
         TestCase {
            name: "16 chars lowercase with spaces",
            secret: "33pa uenl 5psr 477t".try_into().unwrap(),
            time_step: Duration::from_secs(DEFAULT_TIME_STEP).try_into().unwrap(),
            time_since_epoch: Duration::from_secs(1585956366),
            expected_totp: 834828,
         },
      ];

      for TestCase {
         name,
         secret,
         time_step,
         time_since_epoch: time,
         expected_totp,
      } in cases
      {
         match calculate_totp(&secret, time_step, time) {
            Ok(totp) => assert_eq!(totp, expected_totp, "Testing {}:", name),
            Err(e) => panic!("Error for case {} = {:?}", name, e),
         }
      }
   }
}
