/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

use core::cmp::{Ord, Ordering};

use crate::entities::entry::Entry;

/// Used during filtering to allow selecting the best matches\
/// The scoring order is ascending, where higher is better, this translates to:\
/// concatenate(Issuer, Description) < Description < Issuer
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum Score {
   Combined(i64),
   Description(i64),
   Issuer(i64),
}

/// Used during filtering to allow selecting the best matches
#[derive(Debug, Eq)]
pub struct ScoredItem<'entry> {
   /// Score after filtering with fuzzy matcher algorithm
   pub score: Score,

   /// Associated `Entry` to a score
   pub entry: &'entry Entry,
}

impl<'entry> PartialEq for ScoredItem<'entry> {
   fn eq(&self, other: &Self) -> bool {
      self.score == other.score
   }
}

impl<'entry> PartialOrd for ScoredItem<'entry> {
   fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
      self.score.partial_cmp(&other.score)
   }
}

impl<'entry> Ord for ScoredItem<'entry> {
   fn cmp(&self, other: &Self) -> Ordering {
      self.score.cmp(&other.score)
   }
}

#[cfg(test)]
mod tests {
   use std::{cmp::Ordering, convert::TryInto, time::Duration};

   use crate::{
      entities::entry::{Entry, Id},
      use_cases::otp_generation::filtering::scored_item::{Score, ScoredItem},
   };

   const SCORE: i64 = 0;

   fn entry_a() -> Entry {
      Entry {
         id: Id::generate(),
         issuer: String::from("a"),
         description: String::from("b"),
         secret: "c".try_into().unwrap(),
         time_step: Duration::from_secs(1).try_into().unwrap(),
      }
   }

   fn entry_b() -> Entry {
      Entry {
         id: Id::generate(),
         issuer: String::from("c"),
         description: String::from("d"),
         secret: "e".try_into().unwrap(),
         time_step: Duration::from_secs(2).try_into().unwrap(),
      }
   }

   #[test]
   fn two_scored_items_are_equal_if_their_scores_type_are_equal_and_their_score_too() {
      // Combined
      let a = ScoredItem {
         entry: &entry_a(),
         score: Score::Combined(SCORE),
      };

      let b = ScoredItem {
         entry: &entry_b(),
         score: Score::Combined(SCORE),
      };

      assert_eq!(a, b);

      // Description
      let a = ScoredItem {
         entry: &entry_a(),
         score: Score::Description(SCORE),
      };

      let b = ScoredItem {
         entry: &entry_b(),
         score: Score::Description(SCORE),
      };

      assert_eq!(a, b);

      // Issuer
      let a = ScoredItem {
         entry: &entry_a(),
         score: Score::Issuer(SCORE),
      };

      let b = ScoredItem {
         entry: &entry_b(),
         score: Score::Issuer(SCORE),
      };

      assert_eq!(a, b);
   }

   #[test]
   fn two_scored_items_are_different_if_their_scores_type_are_equal_but_their_score_is_different() {
      // Combined
      let a = ScoredItem {
         entry: &entry_a(),
         score: Score::Combined(SCORE),
      };

      let b = ScoredItem {
         entry: &entry_a(),
         score: Score::Combined(SCORE + 1),
      };

      assert_ne!(a, b);

      // Description
      let a = ScoredItem {
         entry: &entry_a(),
         score: Score::Description(SCORE),
      };

      let b = ScoredItem {
         entry: &entry_a(),
         score: Score::Description(SCORE + 1),
      };

      assert_ne!(a, b);

      // Issuer
      let a = ScoredItem {
         entry: &entry_a(),
         score: Score::Issuer(SCORE),
      };

      let b = ScoredItem {
         entry: &entry_a(),
         score: Score::Issuer(SCORE + 1),
      };

      assert_ne!(a, b);
   }

   #[test]
   fn two_scored_items_are_different_if_their_scores_type_are_different() {
      let a = ScoredItem {
         entry: &entry_a(),
         score: Score::Combined(SCORE),
      };

      let b = ScoredItem {
         entry: &entry_a(),
         score: Score::Description(SCORE),
      };

      assert_ne!(a, b);

      let a = ScoredItem {
         entry: &entry_a(),
         score: Score::Description(SCORE),
      };

      let b = ScoredItem {
         entry: &entry_a(),
         score: Score::Issuer(SCORE),
      };

      assert_ne!(a, b);

      let a = ScoredItem {
         entry: &entry_a(),
         score: Score::Issuer(SCORE),
      };

      let b = ScoredItem {
         entry: &entry_a(),
         score: Score::Combined(SCORE),
      };

      assert_ne!(a, b);
   }

   #[test]
   fn a_scored_item_is_less_than_another_if_their_scores_type_are_equal_and_its_score_is_the_lesser() {
      // Combined
      let a = ScoredItem {
         entry: &entry_a(),
         score: Score::Combined(SCORE),
      };

      let b = ScoredItem {
         entry: &entry_a(),
         score: Score::Combined(SCORE + 1),
      };

      let actual = a.partial_cmp(&b);
      assert_eq!(actual, Some(Ordering::Less));
      assert_eq!(a.cmp(&b), Ordering::Less);

      // Description
      let a = ScoredItem {
         entry: &entry_a(),
         score: Score::Description(1),
      };

      let b = ScoredItem {
         entry: &entry_a(),
         score: Score::Description(200),
      };

      let actual = a.partial_cmp(&b);
      assert_eq!(actual, Some(Ordering::Less));
      assert_eq!(a.cmp(&b), Ordering::Less);

      // Issuer
      let a = ScoredItem {
         entry: &entry_a(),
         score: Score::Issuer(1),
      };

      let b = ScoredItem {
         entry: &entry_a(),
         score: Score::Issuer(200),
      };

      let actual = a.partial_cmp(&b);
      assert_eq!(actual, Some(Ordering::Less));
      assert_eq!(a.cmp(&b), Ordering::Less);
   }

   #[test]
   fn score_type_combined_has_lower_precedence_than_issuer_type() {
      let a = ScoredItem {
         entry: &entry_a(),
         score: Score::Combined(SCORE),
      };

      let b = ScoredItem {
         entry: &entry_a(),
         score: Score::Issuer(SCORE),
      };

      let actual = a.partial_cmp(&b);
      assert_eq!(actual, Some(Ordering::Less));
      assert_eq!(a.cmp(&b), Ordering::Less);
   }

   #[test]
   fn score_type_combined_has_lower_precedence_than_description_type() {
      let a = ScoredItem {
         entry: &entry_a(),
         score: Score::Combined(SCORE),
      };

      let b = ScoredItem {
         entry: &entry_a(),
         score: Score::Description(SCORE),
      };

      let actual = a.partial_cmp(&b);
      assert_eq!(actual, Some(Ordering::Less));
      assert_eq!(a.cmp(&b), Ordering::Less);
   }

   #[test]
   fn score_type_description_has_lower_precedence_than_issuer_type() {
      let a = ScoredItem {
         entry: &entry_a(),
         score: Score::Description(SCORE),
      };

      let b = ScoredItem {
         entry: &entry_a(),
         score: Score::Issuer(SCORE),
      };

      let actual = a.partial_cmp(&b);
      assert_eq!(actual, Some(Ordering::Less));
      assert_eq!(a.cmp(&b), Ordering::Less);
   }

   #[test]
   fn a_scored_item_is_greater_than_another_if_their_scores_type_are_equal_and_its_score_is_the_greater() {
      // Combined
      let a = ScoredItem {
         entry: &entry_a(),
         score: Score::Combined(SCORE + 1),
      };

      let b = ScoredItem {
         entry: &entry_a(),
         score: Score::Combined(SCORE),
      };

      let actual = a.partial_cmp(&b);
      assert_eq!(actual, Some(Ordering::Greater));
      assert_eq!(a.cmp(&b), Ordering::Greater);

      // Description
      let a = ScoredItem {
         entry: &entry_a(),
         score: Score::Description(SCORE + 1),
      };

      let b = ScoredItem {
         entry: &entry_a(),
         score: Score::Description(SCORE),
      };

      let actual = a.partial_cmp(&b);
      assert_eq!(actual, Some(Ordering::Greater));
      assert_eq!(a.cmp(&b), Ordering::Greater);

      // Issuer
      let a = ScoredItem {
         entry: &entry_a(),
         score: Score::Issuer(SCORE + 1),
      };

      let b = ScoredItem {
         entry: &entry_a(),
         score: Score::Issuer(SCORE),
      };

      let actual = a.partial_cmp(&b);
      assert_eq!(actual, Some(Ordering::Greater));
      assert_eq!(a.cmp(&b), Ordering::Greater);
   }

   #[test]
   fn score_type_issuer_has_greater_precedence_than_description_type() {
      let a = ScoredItem {
         entry: &entry_a(),
         score: Score::Issuer(SCORE),
      };

      let b = ScoredItem {
         entry: &entry_a(),
         score: Score::Description(SCORE),
      };

      let actual = a.partial_cmp(&b);
      assert_eq!(actual, Some(Ordering::Greater));
      assert_eq!(a.cmp(&b), Ordering::Greater);
   }

   #[test]
   fn score_type_issuer_has_greater_precedence_than_combined_type() {
      let a = ScoredItem {
         entry: &entry_a(),
         score: Score::Issuer(SCORE),
      };

      let b = ScoredItem {
         entry: &entry_a(),
         score: Score::Combined(SCORE),
      };

      let actual = a.partial_cmp(&b);
      assert_eq!(actual, Some(Ordering::Greater));
      assert_eq!(a.cmp(&b), Ordering::Greater);
   }

   #[test]
   fn score_type_description_has_greater_precedence_than_combined_type() {
      let a = ScoredItem {
         entry: &entry_a(),
         score: Score::Description(SCORE),
      };

      let b = ScoredItem {
         entry: &entry_a(),
         score: Score::Combined(SCORE),
      };

      let actual = a.partial_cmp(&b);
      assert_eq!(actual, Some(Ordering::Greater));
      assert_eq!(a.cmp(&b), Ordering::Greater);
   }
}
