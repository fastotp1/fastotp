/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! Main filtering operation used to get best matching TOTP when searching using criteria linked to
//! an [Entry]

use core::cmp::max;
use std::collections::BinaryHeap;

use fuzzy_matcher::{skim::SkimMatcherV2, FuzzyMatcher};

use crate::{
   entities::entry::Entry,
   use_cases::otp_generation::filtering::scored_item::{Score, ScoredItem},
};

mod scored_item;

/// Fuzzy filters the given [Entry] slice according to the provided pattern.
/// Will prefer more precise matches over the issuer, the over the description, and finally
/// over the concatenation of both.
///
/// During comparison will ignore casing, thus "AAA" is scored the same as "aaa".
pub fn filter<'entry>(pattern: &'entry str, entries: &[&'entry Entry]) -> Vec<&'entry Entry> {
   let skim_matcher = SkimMatcherV2::default().ignore_case();

   let diacriticless_pattern = remove_diacritcs(pattern);

   let mut matches: BinaryHeap<ScoredItem<'entry>> = entries
      .iter()
      .filter_map(|entry| {
         let diacriticless_issuer = remove_diacritcs(&entry.issuer);
         let diacriticless_description = remove_diacritcs(&entry.description);

         let issuer_score = skim_matcher
            .fuzzy_match(&diacriticless_issuer, &diacriticless_pattern)
            .map(Score::Issuer);

         let description_score = skim_matcher
            .fuzzy_match(&diacriticless_description, &diacriticless_pattern)
            .map(Score::Description);

         let combined_score = skim_matcher
            .fuzzy_match(
               format!("{}{}", &diacriticless_issuer, &diacriticless_description).as_str(),
               &diacriticless_pattern,
            )
            .map(Score::Combined);

         let score = max(max(issuer_score, description_score), combined_score);

         score.map(|score| ScoredItem { score, entry })
      })
      .collect();

   let mut filtered_and_ranked = Vec::with_capacity(matches.len());
   while let Some(choice) = matches.pop() {
      filtered_and_ranked.push(choice.entry);
   }
   filtered_and_ranked
}

fn remove_diacritcs(pattern: &str) -> String {
   pattern
      .chars()
      .map(|c| {
         match c {
            'À' | 'Á' | 'Â' | 'Ã' | 'Ä' | 'Å' => 'A',
            'à' | 'á' | 'â' | 'ã' | 'ä' | 'å' => 'a',
            'Ç' | 'Č' => 'C',
            'ç' | 'č' => 'c',
            'Ď' | 'Ð' => 'D',
            'ď' | 'ð' => 'd',
            'Ě' | 'È' | 'É' | 'Ê' | 'Ë' => 'E',
            'ě' | 'è' | 'é' | 'ê' | 'ë' => 'e',
            'Ì' | 'Í' | 'Î' | 'Ï' => 'I',
            'ì' | 'í' | 'î' | 'ï' => 'i',
            'Ň' | 'Ñ' => 'N',
            'ň' | 'ñ' => 'n',
            'Ò' | 'Ó' | 'Ô' | 'Õ' | 'Ö' | 'Ø' => 'O',
            'ò' | 'ó' | 'ô' | 'õ' | 'ö' | 'ø' => 'o',
            'Ř' => 'R',
            'ř' => 'r',
            'Š' => 'S',
            'š' => 's',
            'Ť' => 'T',
            'ť' => 't',
            'Ů' | 'Ù' | 'Ú' | 'Û' | 'Ü' => 'U',
            'ů' | 'ù' | 'ú' | 'û' | 'ü' => 'u',
            'Ý' => 'Y',
            'ý' | 'ÿ' => 'y',
            'Ž' => 'Z',
            'ž' => 'z',
            _ => c,
         }
      })
      .fold(String::with_capacity(pattern.len()), |mut concat, item| {
         concat.push(item);
         concat
      })
}

#[cfg(test)]
mod tests {
   use std::{convert::TryInto, time::Duration};

   use crate::{
      entities::entry::{Entry, Id},
      use_cases::otp_generation::filtering::filter,
   };

   #[test]
   fn empty_entries_list_returns_empty() {
      let actual = filter("doesn't matter", &[]);

      assert!(actual.is_empty());
   }

   #[test]
   fn single_matching_element_returns_the_element() {
      let entry = Entry {
         id: Id::generate(),
         issuer: String::from("Google"),
         description: String::from("Personal email"),
         secret: "c".try_into().unwrap(),
         time_step: Duration::from_secs(1).try_into().unwrap(),
      };

      let actual = filter("Google", &[&entry]);

      let expected = [&entry];

      assert_eq!(actual, expected);
   }

   #[test]
   fn single_matching_element_with_diacritics_returns_the_element() {
      let entry = Entry {
         id: Id::generate(),
         issuer: String::from("ÀÁÂÃÄÅàáâãäåÇČçčĎÐďðĚÈÉÊËěèéêëÌÍÎÏìíîïŇÑňñÒÓÔÕÖØòóôõöøŘřŠšŤťŮÙÚÛÜůùúûüÝýÿŽž"),
         description: String::from("Entry with diacritics"),
         secret: "c".try_into().unwrap(),
         time_step: Duration::from_secs(1).try_into().unwrap(),
      };

      let actual = filter(
         "AAAAAAaaaaaaCCccDDddEEEEEeeeeeIIIIiiiiNNnnOOOOOOooooooRrSsTtUUUUUuuuuuYyyZz",
         &[&entry],
      );

      let expected = [&entry];

      assert_eq!(actual, expected);
   }

   #[test]
   fn a_single_non_matching_element_returns_empty_list() {
      let entry = Entry {
         id: Id::generate(),
         issuer: String::from("not matching"),
         description: String::from("ever"),
         secret: "c".try_into().unwrap(),
         time_step: Duration::from_secs(1).try_into().unwrap(),
      };

      let actual = filter("0123", &[&entry]);

      assert!(actual.is_empty());
   }

   #[test]
   fn single_matching_element_returns_the_element_for_empty_filter() {
      let entry = Entry {
         id: Id::generate(),
         issuer: String::from("Google"),
         description: String::from("Personal email"),
         secret: "c".try_into().unwrap(),
         time_step: Duration::from_secs(1).try_into().unwrap(),
      };

      let actual = filter("", &[&entry]);

      let expected = [&entry];

      assert_eq!(actual, expected);
   }

   #[test]
   #[allow(non_snake_case)]
   fn matching_on_issuer() {
      let AxxBzz = Entry {
         id: Id::generate(),
         issuer: String::from("AxxBzz"),
         description: String::from(""),
         secret: "c".try_into().unwrap(),
         time_step: Duration::from_secs(1).try_into().unwrap(),
      };

      let axxBzz = Entry {
         id: Id::generate(),
         issuer: String::from("axxBzz"),
         description: String::from(""),
         secret: "c".try_into().unwrap(),
         time_step: Duration::from_secs(1).try_into().unwrap(),
      };

      let axxbzz = Entry {
         id: Id::generate(),
         issuer: String::from("axxbzz"),
         description: String::from(""),
         secret: "c".try_into().unwrap(),
         time_step: Duration::from_secs(1).try_into().unwrap(),
      };

      let actual = filter("AB", &[&axxBzz, &axxbzz, &AxxBzz]);

      let expected = [&AxxBzz, &axxBzz, &axxbzz];

      assert_eq!(actual, expected);
   }

   #[test]
   #[allow(non_snake_case)]
   fn matching_on_description() {
      let AxxBzz = Entry {
         id: Id::generate(),
         issuer: String::from("1"),
         description: String::from("AxxBzz"),
         secret: "c".try_into().unwrap(),
         time_step: Duration::from_secs(1).try_into().unwrap(),
      };

      let axxBzz = Entry {
         id: Id::generate(),
         issuer: String::from("2"),
         description: String::from("axxBzz"),
         secret: "c".try_into().unwrap(),
         time_step: Duration::from_secs(1).try_into().unwrap(),
      };

      let axxbzz = Entry {
         id: Id::generate(),
         issuer: String::from("3"),
         description: String::from("axxbzz"),
         secret: "c".try_into().unwrap(),
         time_step: Duration::from_secs(1).try_into().unwrap(),
      };

      let actual = filter("AB", &[&axxBzz, &axxbzz, &AxxBzz]);

      let expected = [&AxxBzz, &axxBzz, &axxbzz];

      assert_eq!(actual, expected);
   }

   #[test]
   #[allow(non_snake_case)]
   fn matching_issuer_or_description_gives_priority_to_issuer() {
      let axxBzz = Entry {
         id: Id::generate(),
         issuer: String::from("axxBzz"),
         description: String::from("2"),
         secret: "c".try_into().unwrap(),
         time_step: Duration::from_secs(1).try_into().unwrap(),
      };

      let axxbzz = Entry {
         id: Id::generate(),
         issuer: String::from("axxbzz"),
         description: String::from("1"),
         secret: "c".try_into().unwrap(),
         time_step: Duration::from_secs(1).try_into().unwrap(),
      };

      let AxxBzz = Entry {
         id: Id::generate(),
         issuer: String::from("1"),
         description: String::from("AxxBzz"),
         secret: "c".try_into().unwrap(),
         time_step: Duration::from_secs(1).try_into().unwrap(),
      };

      let actual = filter("AB", &[&AxxBzz, &axxBzz, &axxbzz]);

      let expected = [&axxBzz, &axxbzz, &AxxBzz];

      assert_eq!(actual, expected);
   }

   #[test]
   #[allow(non_snake_case)]
   fn issuer_matches_have_more_weight_than_description_matches() {
      let issuer_AxxBzz = Entry {
         id: Id::generate(),
         issuer: String::from("AxxBzz"),
         description: String::from("1"),
         secret: "issuer".try_into().unwrap(),
         time_step: Duration::from_secs(1).try_into().unwrap(),
      };

      let description_AxxBzz = Entry {
         id: Id::generate(),
         issuer: String::from("1"),
         description: String::from("AxxBzz"),
         secret: "description".try_into().unwrap(),
         time_step: Duration::from_secs(1).try_into().unwrap(),
      };

      let actual = filter("AB", &[&description_AxxBzz, &issuer_AxxBzz]);

      let expected = [&issuer_AxxBzz, &description_AxxBzz];

      assert_eq!(actual, expected);
   }

   #[test]
   #[allow(non_snake_case)]
   fn matching_split_between_issuer_and_description() {
      let AxxBzz = Entry {
         id: Id::generate(),
         issuer: String::from("Axx"),
         description: String::from("Bzz"),
         secret: "c".try_into().unwrap(),
         time_step: Duration::from_secs(1).try_into().unwrap(),
      };

      let axxBzz = Entry {
         id: Id::generate(),
         issuer: String::from("axx"),
         description: String::from("Bzz"),
         secret: "c".try_into().unwrap(),
         time_step: Duration::from_secs(1).try_into().unwrap(),
      };

      let axxbzz = Entry {
         id: Id::generate(),
         issuer: String::from("axx"),
         description: String::from("bzz"),
         secret: "c".try_into().unwrap(),
         time_step: Duration::from_secs(1).try_into().unwrap(),
      };

      let actual = filter("AxBz", &[&axxBzz, &axxbzz, &AxxBzz]);

      let expected = [&AxxBzz, &axxBzz, &axxbzz];

      assert_eq!(actual, expected);
   }
}
