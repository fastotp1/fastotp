/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! This use case ensures changing the master password is possible.

use core::result::Result as StdResult;
use std::sync::Arc;

use futures::future::BoxFuture;

use crate::{
   repository::{storage::Error as StorageError, Error as RepoError},
   use_cases::{SharedRepository, SharedStorage},
};

/// In case a change of password fails
#[derive(Clone, Debug, thiserror::Error, Eq, PartialEq)]
pub enum Error {
   #[error("couldn't write encrypted data to storage")]
   Storage(#[from] StorageError),

   #[error("couldn't get all saved entries")]
   Repository(#[from] RepoError),
}

pub type Result = StdResult<(), Error>;

/// Changes the password of used to decrypt the [Entries](super::super::entities::entry::Entries). Doing so re-encrypts the file with
/// the new password.
pub struct ChangePassword {
   repository: SharedRepository,
   storage: SharedStorage,
}

impl ChangePassword {
   #[inline]
   pub fn new(repository: SharedRepository, storage: SharedStorage) -> Self {
      Self { repository, storage }
   }

   pub fn change_password(&mut self, new_password: String) -> BoxFuture<'static, Result> {
      let repository = Arc::clone(&self.repository);
      let storage = Arc::clone(&self.storage);

      Box::pin(Self::async_change_password(repository, storage, new_password))
   }

   async fn async_change_password(
      repository: SharedRepository,
      storage: SharedStorage,
      new_password: String,
   ) -> Result {
      let entries = repository.read().await.all().await?;

      Ok(storage.write().await.update_encryption(new_password, entries).await?)
   }
}

#[cfg(test)]
mod tests {
   use std::sync::{Arc, RwLock};

   use async_lock::RwLock as AsyncRwLock;
   use futures::{future::BoxFuture, FutureExt};

   use crate::{
      entities::{encrypted, encrypted::EncryptEntries, entry::Entries},
      repository::{
         storage::{self, Error as StorageError},
         InMemory,
      },
      use_cases::change_password::{ChangePassword, Error},
   };

   #[derive(Debug)]
   struct TestStorage {
      should_succeed: Arc<RwLock<bool>>,
   }

   impl EncryptEntries for TestStorage {
      fn encrypt(&self, _: &Entries) -> Result<Vec<u8>, encrypted::Error> {
         unimplemented!()
      }
   }

   impl storage::Encrypted for TestStorage {
      fn store(&mut self, _: &Entries) -> BoxFuture<'static, Result<(), StorageError>> {
         return async { Ok(()) }.boxed();
      }

      fn update_encryption(&mut self, _: String, _: Entries) -> BoxFuture<'static, Result<(), StorageError>> {
         if *self.should_succeed.read().unwrap() {
            return async { Ok(()) }.boxed();
         }

         async { Err(StorageError("error".to_owned())) }.boxed()
      }
   }

   #[test]
   fn returns_error_storage_if_changing_password_storage_fails() {
      async_std::task::block_on(async {
         let repository = Arc::new(AsyncRwLock::new(InMemory::new()));
         let storage = Arc::new(AsyncRwLock::new(TestStorage {
            should_succeed: Arc::new(RwLock::new(false)),
         }));

         let mut cp = ChangePassword::new(repository, storage);

         let change_password_res = cp.change_password("new password".to_owned()).await.unwrap_err();

         assert_eq!(change_password_res, Error::Storage(StorageError("error".to_owned())));
      });
   }
}
