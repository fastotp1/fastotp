/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

use std::{io::SeekFrom, sync::Arc};

use aes_gcm_siv::{
   aead::{generic_array::GenericArray, AeadInPlace},
   Aes256GcmSiv,
   KeyInit,
};
use async_lock::RwLock as AsyncRwLock;
use futures::{future::BoxFuture, AsyncReadExt, AsyncSeekExt};

use crate::{
   entities::{encrypted::EncryptedData, entry::Entries, Serdelizer},
   repository::{storage::Encrypted, DurableRepository},
   use_cases::{
      unlocking,
      unlocking::{aes_gcm_siv_storage::AesGcmSivStorage, AsyncDataSource, Error, PasswordHasher},
   },
};

pub struct Unlocking<ADS, PH>
where
   ADS: AsyncDataSource,
   PH: 'static + PasswordHasher,
{
   data_source: Arc<AsyncRwLock<ADS>>,
   password_hasher: Arc<PH>,
}

impl<ADS, PH> Unlocking<ADS, PH>
where
   ADS: AsyncDataSource,
   PH: 'static + PasswordHasher,
{
   #[inline]
   pub fn new(data_source: ADS, password_hasher: PH) -> Self {
      Self {
         data_source: Arc::new(AsyncRwLock::new(data_source)),
         password_hasher: Arc::new(password_hasher),
      }
   }

   /// Tries to read and decrypt the stored [Entries] using AES256-GCM-SIV.
   /// Decryption password is generated using Argon2i.\
   /// If successful, returns an [unlocking::Result] containing a [SharedRepository](super::SharedRepository) with the read [Entries] loaded,
   /// and the backing [SharedStorage](super::SharedStorage).\
   /// Will re-encrypt the loaded entries to disk immediately to update the PHC string and nonce used to have an unique use per execution.\
   /// Otherwise returns an [Error].
   pub fn unlock<EDS, ES>(&self, plain_password: String) -> BoxFuture<'static, unlocking::Result>
   where
      EDS: 'static + Serdelizer<EncryptedData>,
      ES: 'static + Serdelizer<Entries>,
   {
      let f = Self::async_unlock::<EDS, ES>(
         Arc::clone(&self.data_source),
         plain_password,
         Arc::clone(&self.password_hasher),
      );

      Box::pin(f)
   }

   async fn async_unlock<EDS, ES>(
      data_source: Arc<AsyncRwLock<ADS>>,
      plain_password: String,
      password_hasher: Arc<PH>,
   ) -> unlocking::Result
   where
      EDS: 'static + Serdelizer<EncryptedData>,
      ES: 'static + Serdelizer<Entries>,
   {
      let mut raw_data = Vec::<u8>::new();

      {
         let mut locked_data_source = data_source.write().await;

         locked_data_source
            .seek(SeekFrom::Start(0))
            .await
            .map_err(|e| Error::ReadingDataSource(e.to_string()))?;

         locked_data_source
            .read_to_end(&mut raw_data)
            .await
            .map_err(|e| Error::ReadingDataSource(e.to_string()))?;
      }

      let mut encrypted_data = EDS::deserialize(raw_data.as_slice()).map_err(Error::RawDataDeserialization)?;

      let password = password_hasher
         .hash_password(encrypted_data.header.phc_str.as_str(), plain_password.as_bytes())
         .map_err(|_| Error::Decrypting)?;

      let key = GenericArray::from_slice(password.as_ref());
      let cipher = Aes256GcmSiv::new(key);
      let nonce = GenericArray::from_slice(encrypted_data.header.nonce.as_slice());

      if !encrypted_data.payload.is_empty() {
         cipher
            .decrypt_in_place(nonce, unlocking::AAD, &mut encrypted_data.payload)
            .map_err(|_| Error::Decrypting)?;
      }

      let entries =
         ES::deserialize(encrypted_data.payload.as_slice()).map_err(Error::DecryptedEntriesDeserialization)?;

      let mut storage: AesGcmSivStorage<ADS, EDS, ES> = AesGcmSivStorage::new(
         data_source,
         password_hasher,
         key,
         encrypted_data.header.phc_str.clone(),
         nonce,
      );

      // Generate new PHC string and nonce everytime the application is started.
      // Ignoring error since failing to re-encrypt data doesn't affect normal application functioning.
      let _ = storage.update_encryption(plain_password, entries.clone()).await;

      let storage = Arc::new(AsyncRwLock::new(storage));
      let repository = DurableRepository::new(Arc::clone(&storage), entries);
      let repository = Arc::new(AsyncRwLock::new(repository));

      Ok((repository, storage))
   }
}

#[cfg(test)]
mod tests {
   use std::{
      cmp::min,
      collections::HashSet,
      io::{Error as IoError, ErrorKind as IoErrorKind, Result as IoResult, SeekFrom},
      pin::Pin,
      sync::{Arc, Mutex},
      task::{Context, Poll},
   };

   use aes_gcm_siv::KeyInit;
   use futures::{AsyncRead, AsyncSeek, AsyncWrite, StreamExt};

   use crate::{
      entities::{
         self,
         encrypted::{EncryptedData, EncryptionHeader},
         entry::Entries,
         Deserializer,
         Serializer,
      },
      scaffold_deserialize,
      scaffold_serdelizer,
      use_cases::unlocking::{unlocking::Unlocking, Error, PasswordHasher, StubPasswordHasher},
   };

   #[derive(Debug, thiserror::Error)]
   #[error("Error")]
   struct TestError;

   #[pin_project::pin_project]
   #[derive(Debug)]
   struct TestDataSource {
      pub state: DSReadState,
      pub written: Arc<Mutex<Vec<u8>>>,
   }

   #[derive(Debug)]
   enum DSReadState {
      Error,
      Successful(Vec<u8>),
   }

   impl AsyncRead for TestDataSource {
      fn poll_read(self: Pin<&mut Self>, _: &mut Context<'_>, buf: &mut [u8]) -> Poll<IoResult<usize>> {
         let this = self.project();
         match this.state {
            DSReadState::Error => Poll::Ready(Err(IoError::new(IoErrorKind::Other, TestError))),
            DSReadState::Successful(data) => {
               let bytes_to_write = buf.len();
               let bytes_to_read = min(bytes_to_write, data.len());

               if bytes_to_read == 0 {
                  *this.state = DSReadState::Successful(vec![]);
                  return Poll::Ready(Ok(0));
               }

               let (to_copy, left) = data.split_at(bytes_to_read);

               let mut bytes_read = Vec::with_capacity(bytes_to_write);
               bytes_read.extend_from_slice(to_copy);

               if bytes_read.len() < bytes_to_write {
                  bytes_read.extend(vec![0; bytes_to_write - bytes_read.len()])
               }

               buf.copy_from_slice(&bytes_read);

               *this.state = DSReadState::Successful(left.to_vec());

               Poll::Ready(Ok(bytes_to_read))
            }
         }
      }
   }

   impl AsyncSeek for TestDataSource {
      fn poll_seek(self: Pin<&mut Self>, _: &mut Context<'_>, _: SeekFrom) -> Poll<IoResult<u64>> {
         let this = self.project();
         this.written.lock().unwrap().clear();

         Poll::Ready(Ok(0))
      }
   }

   impl AsyncWrite for TestDataSource {
      fn poll_write(self: Pin<&mut Self>, _: &mut Context<'_>, buf: &[u8]) -> Poll<IoResult<usize>> {
         let this = self.project();
         this.written.lock().unwrap().extend_from_slice(buf);

         Poll::Ready(Ok(buf.len()))
      }

      fn poll_flush(self: Pin<&mut Self>, _: &mut Context<'_>) -> Poll<IoResult<()>> {
         Poll::Ready(Ok(()))
      }

      fn poll_close(self: Pin<&mut Self>, _: &mut Context<'_>) -> Poll<IoResult<()>> {
         Poll::Ready(Ok(()))
      }
   }

   scaffold_deserialize!(EncDataFails, EncryptedData, _buf, { Err("Error".to_owned()) });
   scaffold_deserialize!(EntriesFails, Entries, _buf, { Err("Error".to_owned()) });

   scaffold_serdelizer!(
      EncDataWorks,
      EncryptedData,
      input,
      {
         let mut buffer: Vec<u8> = Vec::new();

         buffer.push(input.header.phc_str.as_bytes().len() as u8);
         buffer.push(input.header.nonce.len() as u8);
         buffer.push(input.payload.len() as u8);

         buffer.extend_from_slice(input.header.phc_str.as_bytes());
         buffer.extend_from_slice(input.header.nonce.as_slice());
         buffer.extend_from_slice(input.payload.as_slice());

         Ok(buffer)
      },
      {
         if input.is_empty() {
            return Ok(EncryptedData::new());
         }

         let pos_phc_str = (3 + input[0]) as usize;
         let pos_nonce = pos_phc_str + input[1] as usize;

         let enc_data = EncryptedData {
            header: EncryptionHeader {
               phc_str: String::from_utf8_lossy(&input[3..pos_phc_str]).into_owned(),
               nonce: (&input[pos_phc_str..pos_nonce]).to_vec(),
            },
            payload: input[pos_nonce..].to_vec(),
         };

         Ok(enc_data)
      }
   );

   scaffold_serdelizer!(
      EntriesWorks,
      Entries,
      input,
      {
         if input.values.is_empty() {
            return Ok(vec![]);
         }

         Ok(vec![30, 31, 32, 33, 34, 35, 36, 37, 38])
      },
      {
         if input.is_empty() {
            return Ok(Entries::default());
         }

         let mut values = HashSet::new();
         values.insert(entities::create_test_entry(1));
         values.insert(entities::create_test_entry(2));

         Ok(Entries { values })
      }
   );

   #[test]
   fn fails_if_reading_data_source_fails_and_returns_reading_data_source_error() {
      async_std::task::block_on(async {
         let tds = TestDataSource {
            state: DSReadState::Error,
            written: Arc::new(Mutex::new(Vec::new())),
         };

         let unlock_res = Unlocking::<TestDataSource, StubPasswordHasher>::new(tds, StubPasswordHasher)
            .unlock::<EncDataWorks, EntriesWorks>("ignored".to_owned())
            .await;

         assert_eq!(unlock_res.unwrap_err(), Error::ReadingDataSource("Error".to_owned()))
      });
   }

   #[test]
   fn fails_if_binary_format_deserialization_fails_and_returns_raw_data_deserialization_error() {
      async_std::task::block_on(async {
         const PASSWORD: &str = "01234567";
         let serialized: Vec<u8> = vec![];

         let tds = TestDataSource {
            state: DSReadState::Successful(serialized),
            written: Arc::new(Mutex::new(Vec::new())),
         };

         let unlock_res = Unlocking::<TestDataSource, StubPasswordHasher>::new(tds, StubPasswordHasher)
            .unlock::<EncDataFails, EntriesWorks>(PASSWORD.to_owned())
            .await;

         assert_eq!(
            unlock_res.unwrap_err(),
            Error::RawDataDeserialization("Error".to_owned())
         )
      });
   }

   #[test]
   fn works_with_empty_data_source() {
      async_std::task::block_on(async {
         const PASSWORD: &str = "01234567";
         let serialized: Vec<u8> = vec![];

         let tds = TestDataSource {
            state: DSReadState::Successful(serialized),
            written: Arc::new(Mutex::new(Vec::new())),
         };

         let unlock_res = Unlocking::<TestDataSource, StubPasswordHasher>::new(tds, StubPasswordHasher)
            .unlock::<EncDataWorks, EntriesWorks>(PASSWORD.to_owned())
            .await;

         let entries = Entries {
            values: HashSet::default(),
         };

         assert!(unlock_res.is_ok(), "{:?}", unlock_res);

         let changes = unlock_res.unwrap().0.read().await.changes().next().await;

         assert_eq!(changes, Some(entries))
      });
   }

   #[test]
   fn works_with_empty_password_and_empty_data_source() {
      async_std::task::block_on(async {
         const EMPTY_PASSWORD: &str = "";
         let serialized: Vec<u8> = vec![];

         let tds = TestDataSource {
            state: DSReadState::Successful(serialized),
            written: Arc::new(Mutex::new(Vec::new())),
         };

         let unlock_res = Unlocking::<TestDataSource, StubPasswordHasher>::new(tds, StubPasswordHasher)
            .unlock::<EncDataWorks, EntriesWorks>(EMPTY_PASSWORD.to_owned())
            .await;

         assert!(unlock_res.is_ok(), "{:?}", unlock_res);

         let changes = unlock_res.unwrap().0.read().await.changes().next().await;

         let entries = Entries {
            values: HashSet::default(),
         };

         assert_eq!(changes, Some(entries))
      });
   }

   #[test]
   fn decrypts_with_empty_password() {
      async_std::task::block_on(async {
         const EMPTY_PASSWORD: &str = "";
         const SERIALIZED: &[u8; 93] = &[
            53_u8, 12, 25, // sizes
            36, 97, 114, 103, 111, 110, 50, 100, 36, 118, 61, 49, 57, 36, 109, 61, 53, 50, 52, 50, 56, 56, 44,
            116, 61, 51, 44, 112, 61, 51, 36, 65, 81, 73, 68, 66, 65, 85, 71, 66, 119, 103, 74, 67, 103, 115, 77,
            68, 81, 52, 80, 69, 65, // PHC
            17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, // Nonce
            // decrypted payload is: 30, 31, 32, 33, 34, 35, 36, 37, 38
            197, 4, 50, 139, 240, 169, 141, 93, 210, 239, 236, 67, 120, 19, 19, 131, 220, 243, 207, 140, 64, 127,
            209, 18, 119,
         ];
         let tds = TestDataSource {
            state: DSReadState::Successful(SERIALIZED.to_vec()),
            written: Arc::new(Mutex::new(Vec::new())),
         };

         let unlock_res = Unlocking::<TestDataSource, StubPasswordHasher>::new(tds, StubPasswordHasher)
            .unlock::<EncDataWorks, EntriesWorks>(EMPTY_PASSWORD.to_owned())
            .await;

         assert!(unlock_res.is_ok(), "{:?}", unlock_res);

         let changes = unlock_res.unwrap().0.read().await.changes().next().await;

         let mut values = HashSet::new();
         values.insert(entities::create_test_entry(1));
         values.insert(entities::create_test_entry(2));

         let entries = Entries { values };

         assert_eq!(changes, Some(entries))
      });
   }

   #[test]
   fn decrypts_with_password_less_than_256_bits() {
      async_std::task::block_on(async {
         const SHORT_PASSWORD: &str = "12345678";
         const SERIALIZED: &[u8; 93] = &[
            53_u8, 12, 25, // sizes
            36, 97, 114, 103, 111, 110, 50, 100, 36, 118, 61, 49, 57, 36, 109, 61, 53, 50, 52, 50, 56, 56, 44,
            116, 61, 51, 44, 112, 61, 51, 36, 65, 81, 73, 68, 66, 65, 85, 71, 66, 119, 103, 74, 67, 103, 115, 77,
            68, 81, 52, 80, 69, 65, // PHC
            17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, // Nonce
            // decrypted payload is: 30, 31, 32, 33, 34, 35, 36, 37, 38
            206, 191, 6, 34, 110, 68, 155, 151, 32, 73, 203, 188, 22, 218, 238, 182, 72, 111, 171, 234, 243, 135,
            216, 100, 147,
         ];
         let tds = TestDataSource {
            state: DSReadState::Successful(SERIALIZED.to_vec()),
            written: Arc::new(Mutex::new(Vec::new())),
         };

         let unlock_res = Unlocking::<TestDataSource, StubPasswordHasher>::new(tds, StubPasswordHasher)
            .unlock::<EncDataWorks, EntriesWorks>(SHORT_PASSWORD.to_owned())
            .await;

         assert!(unlock_res.is_ok(), "{:?}", unlock_res);

         let changes = unlock_res.unwrap().0.read().await.changes().next().await;

         let mut values = HashSet::new();
         values.insert(entities::create_test_entry(1));
         values.insert(entities::create_test_entry(2));

         let entries = Entries { values };

         assert_eq!(changes, Some(entries))
      });
   }

   #[test]
   fn decrypts_data_with_password_longer_than_256_bits() {
      async_std::task::block_on(async {
         const LONG_PASSWORD: &str = "12345678901234567890123456789012345678901234567890";
         const SERIALIZED: &[u8; 93] = &[
            53_u8, 12, 25, // sizes
            36, 97, 114, 103, 111, 110, 50, 100, 36, 118, 61, 49, 57, 36, 109, 61, 53, 50, 52, 50, 56, 56, 44,
            116, 61, 51, 44, 112, 61, 51, 36, 65, 81, 73, 68, 66, 65, 85, 71, 66, 119, 103, 74, 67, 103, 115, 77,
            68, 81, 52, 80, 69, 65, // PHC
            17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, // Nonce
            // decrypted payload: 30, 31, 32, 33, 34, 35, 36, 37, 38
            64, 185, 102, 249, 228, 59, 155, 188, 207, 39, 197, 109, 68, 108, 241, 148, 142, 41, 42, 208, 70, 133,
            74, 255, 233,
         ];
         let tds = TestDataSource {
            state: DSReadState::Successful(SERIALIZED.to_vec()),
            written: Arc::new(Mutex::new(Vec::new())),
         };

         let unlock_res = Unlocking::<TestDataSource, StubPasswordHasher>::new(tds, StubPasswordHasher)
            .unlock::<EncDataWorks, EntriesWorks>(LONG_PASSWORD.to_owned())
            .await;

         assert!(unlock_res.is_ok(), "{:?}", unlock_res);

         let changes = unlock_res.unwrap().0.read().await.changes().next().await;

         let mut values = HashSet::new();
         values.insert(entities::create_test_entry(1));
         values.insert(entities::create_test_entry(2));

         let entries = Entries { values };

         assert_eq!(changes, Some(entries))
      });
   }

   #[test]
   fn fails_if_wrong_password_is_used_and_returns_decrypting_error() {
      async_std::task::block_on(async {
         const WRONG_PASSWORD: &str = "0000";
         const SERIALIZED: &[u8; 62] = &[
            22_u8, 12, 25, // sizes
            65, 81, 73, 68, 66, 65, 85, 71, 66, 119, 103, 74, 67, 103, 115, 77, 68, 81, 52, 80, 69,
            65, // Salt
            17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, // Nonce
            // decrypted payload is: 30, 31, 32, 33, 34, 35, 36, 37, 38
            // payload is encrypted with empty password
            197, 4, 50, 139, 240, 169, 141, 93, 210, 239, 236, 67, 120, 19, 19, 131, 220, 243, 207, 140, 64, 127,
            209, 18, 119,
         ];
         let tds = TestDataSource {
            state: DSReadState::Successful(SERIALIZED.to_vec()),
            written: Arc::new(Mutex::new(Vec::new())),
         };

         let unlock_res = Unlocking::<TestDataSource, StubPasswordHasher>::new(tds, StubPasswordHasher)
            .unlock::<EncDataWorks, EntriesWorks>(WRONG_PASSWORD.to_owned())
            .await;

         assert_eq!(unlock_res.unwrap_err(), Error::Decrypting)
      });
   }

   #[test]
   fn fails_if_decrypted_payload_cant_be_deserialized_and_returns_decrypted_entries_deserialization_error() {
      async_std::task::block_on(async {
         const EMPTY_PASSWORD: &str = "";
         const SERIALIZED: &[u8; 93] = &[
            53_u8, 12, 25, // sizes
            36, 97, 114, 103, 111, 110, 50, 100, 36, 118, 61, 49, 57, 36, 109, 61, 53, 50, 52, 50, 56, 56, 44,
            116, 61, 51, 44, 112, 61, 51, 36, 65, 81, 73, 68, 66, 65, 85, 71, 66, 119, 103, 74, 67, 103, 115, 77,
            68, 81, 52, 80, 69, 65, // PHC
            17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, // Nonce
            // decrypted payload: 30, 31, 32, 33, 34, 35, 36, 37, 38
            // encrypted payload with empty password
            197, 4, 50, 139, 240, 169, 141, 93, 210, 239, 236, 67, 120, 19, 19, 131, 220, 243, 207, 140, 64, 127,
            209, 18, 119,
         ];
         let tds = TestDataSource {
            state: DSReadState::Successful(SERIALIZED.to_vec()),
            written: Arc::new(Mutex::new(Vec::new())),
         };

         let unlock_res = Unlocking::<TestDataSource, StubPasswordHasher>::new(tds, StubPasswordHasher)
            .unlock::<EncDataWorks, EntriesFails>(EMPTY_PASSWORD.to_owned())
            .await;

         let error = unlock_res.unwrap_err();

         assert!(matches!(error, Error::DecryptedEntriesDeserialization(_)));
      });
   }

   #[test]
   fn data_source_contents_are_reencrypted_with_new_parameters_on_success() {
      use aes_gcm_siv::{aead::AeadInPlace, Aes256GcmSiv};
      use generic_array::GenericArray;

      use crate::use_cases;

      async_std::task::block_on(async {
         const EMPTY_PASSWORD: &str = "";
         const SERIALIZED: &[u8; 93] = &[
            53_u8, 12, 25, // sizes
            36, 97, 114, 103, 111, 110, 50, 100, 36, 118, 61, 49, 57, 36, 109, 61, 53, 50, 52, 50, 56, 56, 44,
            116, 61, 51, 44, 112, 61, 51, 36, 65, 81, 73, 68, 66, 65, 85, 71, 66, 119, 103, 74, 67, 103, 115, 77,
            68, 81, 52, 80, 69, 65, // PHC
            17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, // Nonce
            // decrypted payload is: 30, 31, 32, 33, 34, 35, 36, 37, 38
            197, 4, 50, 139, 240, 169, 141, 93, 210, 239, 236, 67, 120, 19, 19, 131, 220, 243, 207, 140, 64, 127,
            209, 18, 119,
         ];

         let written = Arc::new(Mutex::new(Vec::new()));
         let tds = TestDataSource {
            state: DSReadState::Successful(SERIALIZED.to_vec()),
            written: Arc::clone(&written),
         };

         let unlock_res = Unlocking::<TestDataSource, StubPasswordHasher>::new(tds, StubPasswordHasher)
            .unlock::<EncDataWorks, EntriesWorks>(EMPTY_PASSWORD.to_owned())
            .await;

         assert!(unlock_res.is_ok(), "{:?}", unlock_res);

         let mut reencrypted_data = EncDataWorks::deserialize(&written.lock().unwrap()).unwrap();

         let old_data = EncDataWorks::deserialize(SERIALIZED).unwrap();

         assert_ne!(reencrypted_data, old_data);
         assert!(!reencrypted_data.payload.is_empty());

         assert_ne!(
            reencrypted_data.header.phc_str.as_bytes()[..],
            [
               36_u8, 97, 114, 103, 111, 110, 50, 100, 36, 118, 61, 49, 57, 36, 109, 61, 53, 50, 52, 50, 56, 56,
               44, 116, 61, 51, 44, 112, 61, 51, 36, 65, 81, 73, 68, 66, 65, 85, 71, 66, 119, 103, 74, 67, 103,
               115, 77, 68, 81, 52, 80, 69, 65
            ]
         );
         assert_ne!(
            reencrypted_data.header.nonce[..],
            [17_u8, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28]
         );

         let password = StubPasswordHasher
            .hash_password(reencrypted_data.header.phc_str.as_str(), EMPTY_PASSWORD.as_bytes())
            .unwrap();

         let nonce = GenericArray::from_slice(&reencrypted_data.header.nonce);
         let result = Aes256GcmSiv::new(GenericArray::from_slice(password.as_ref())).decrypt_in_place(
            nonce,
            use_cases::unlocking::AAD,
            &mut reencrypted_data.payload,
         );

         assert!(result.is_ok(), "{:?}", result);
         assert_eq!(reencrypted_data.payload[..], [30_u8, 31, 32, 33, 34, 35, 36, 37, 38]);
      });
   }

   #[test]
   #[ignore]
   #[allow(clippy::print_stdout, clippy::use_debug)]
   fn generate_test_data() {
      use aes_gcm_siv::{
         aead::{generic_array::GenericArray, AeadInPlace},
         Aes256GcmSiv,
      };

      println!();

      let mut values = HashSet::new();
      values.insert(entities::create_test_entry(1));
      values.insert(entities::create_test_entry(2));

      // "AQIDBAUGBwgJCgsMDQ4PEA" obtained from doing B64.encode([1_u8, 2, ..., 15, 16], STANDARD)
      // 16 bytes are recommended for salt length in Argon2d
      let phc_str: &[u8; 53] = b"$argon2d$v=19$m=524288,t=3,p=3$AQIDBAUGBwgJCgsMDQ4PEA";
      println!("Test PHC string = {:?}, length = {}", phc_str, phc_str.len());

      let password = StubPasswordHasher
         .hash_password(String::from_utf8_lossy(phc_str).as_ref(), "".as_bytes())
         .unwrap();

      const EXPECTED_AAD: &[u8; 20] = b"fastotp-verification";

      let key = GenericArray::from_slice(password.as_slice());
      let nonce = GenericArray::from_slice(&[17u8, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28]);

      let mut encrypted: Vec<u8> = vec![30, 31, 32, 33, 34, 35, 36, 37, 38];

      Aes256GcmSiv::new(key)
         .encrypt_in_place(nonce, EXPECTED_AAD, &mut encrypted)
         .expect("encryption failure");

      println!("Encrypted buffer = {:?}, length = {}", encrypted, encrypted.len());
   }
}
