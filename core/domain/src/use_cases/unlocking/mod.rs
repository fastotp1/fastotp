/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! This use case unlocks the encrypted user data of the application.\
//! If the user data doesn't exist because the program is being executed for the first time it will
//! simply return.

use core::result::Result as StdResult;

use futures::AsyncRead;

use crate::{
   repository::storage::AsyncSink,
   use_cases::{SharedRepository, SharedStorage},
};

mod aes_gcm_siv_storage;
pub mod unlocking;

/// Additional associated data for AES-GCM-SIV-256 algorithm
const AAD: &[u8; 20] = b"fastotp-verification";

/// Errors that occur when trying to unlock the encrypted user data
#[derive(Clone, Debug, thiserror::Error, Eq, PartialEq)]
pub enum Error {
   /// If for any reason we are couldn't read from the data source
   #[error("couldn't read from data source: {0}")]
   ReadingDataSource(String),

   /// If the read data has a malformed format
   #[error("read data has a malformed format: {0}")]
   RawDataDeserialization(String),

   /// Whenever the password is wrong or could not be hashed
   #[error("error decrypting data")]
   Decrypting,

   /// If the decrypted serialized [Entries](super::super::entities::entry::Entries) can't be deserialized
   #[error("decrypted entries are serialized in a malformed format: {0}")]
   DecryptedEntriesDeserialization(String),
}

pub type Result = StdResult<(SharedRepository, SharedStorage), Error>;

pub trait AsyncDataSource: AsyncRead + AsyncSink {}

impl<T> AsyncDataSource for T where T: AsyncRead + AsyncSink {}

/// Errors of password hashing
#[derive(Clone, Debug, thiserror::Error, Eq, PartialEq)]
#[error("password hashing failed: {0}")]
pub struct PasswordHasherError(pub String);

pub trait PasswordHasher: Sync + Send {
   fn hash_password(&self, hashless_phc: &str, plain_password: &[u8]) -> StdResult<Vec<u8>, PasswordHasherError>;
}

#[derive(Debug)]
pub struct StubPasswordHasher;

impl PasswordHasher for StubPasswordHasher {
   fn hash_password(&self, hashless_phc: &str, plain_password: &[u8]) -> StdResult<Vec<u8>, PasswordHasherError> {
      let mut iv = b"00112233445566778899AABBCCDDEEFF".to_vec();

      let phc_bytes = hashless_phc.as_bytes();

      for (i, x) in iv.iter_mut().enumerate() {
         *x ^= phc_bytes[i % phc_bytes.len()];

         if !plain_password.is_empty() {
            *x ^= plain_password[i % plain_password.len()];
         }
      }

      Ok(iv)
   }
}

/// Implementation uses Argon2d and not Argon2i nor Argon2id.
///
/// Argon2i benefits when there are side-channel timing attacks, but in the
/// case of the user's device it shouldn't be accessible by a malicious observer
/// taking note of timings during password verification.
/// The most worrying attack vector is GPU/ASIC bruteforce attacks.
///
///
/// `KeePass` explains in more detail: <https://keepass.info/help/base/security.html#secdictprotect>
#[derive(Debug)]
pub struct Argon2dHasher;

impl PasswordHasher for Argon2dHasher {
   fn hash_password(&self, hashless_phc: &str, plain_password: &[u8]) -> StdResult<Vec<u8>, PasswordHasherError> {
      use argon2::Argon2;
      use password_hash::{PasswordHash, PasswordHasher};

      let password_hash = PasswordHash::new(hashless_phc).map_err(|e| PasswordHasherError(e.to_string()))?;

      let salt = password_hash
         .salt
         .ok_or_else(|| PasswordHasherError("No salt found".to_owned()))?;

      let params = <Argon2<'_> as PasswordHasher>::Params::try_from(&password_hash)
         .map_err(|e| PasswordHasherError(e.to_string()))?;

      let output = Argon2::default()
         .hash_password_customized(
            plain_password,
            Some(password_hash.algorithm),
            password_hash.version,
            params,
            salt,
         )
         .map_err(|e| PasswordHasherError(e.to_string()))?
         .hash
         .ok_or_else(|| PasswordHasherError("Password hashing failed".to_owned()))?;

      Ok(output.as_bytes().to_vec())
   }
}
