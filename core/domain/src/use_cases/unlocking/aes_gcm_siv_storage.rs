/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! See [AesGcmSivStorage]

use core::{
   fmt::{Debug, Formatter},
   marker::PhantomData,
};
use std::sync::Arc;

use aes_gcm_siv::{aead::AeadInPlace, Aes256GcmSiv, KeyInit};
use async_lock::RwLock as AsyncRwLock;
use futures::{future::BoxFuture, io::SeekFrom, AsyncSeekExt, AsyncWriteExt, FutureExt, TryFutureExt};
use generic_array::{
   typenum::{U12, U32},
   GenericArray,
};

use crate::{
   entities::{
      encrypted,
      encrypted::{EncryptEntries, EncryptedData, EncryptionHeader},
      entry::Entries,
      Serializer,
   },
   repository::storage::{self, AsyncSink, Error},
   use_cases,
   use_cases::unlocking::PasswordHasher,
};

/// Implementation of [EncryptEntries] using AES-GCM-SIV
#[pin_project::pin_project]
pub struct AesGcmSivStorage<DS, EDS, ES>
where
   DS: AsyncSink,
   EDS: 'static + Serializer<Entity = EncryptedData>,
   ES: 'static + Serializer<Entity = Entries>,
{
   #[pin]
   data_source: Arc<AsyncRwLock<DS>>,
   password_hasher: Arc<dyn PasswordHasher>,
   key: GenericArray<u8, U32>,
   phc_str: String,
   nonce: GenericArray<u8, U12>,
   encr_data_serde: PhantomData<EDS>,
   entries_serde: PhantomData<ES>,
}

impl<DS, EDS, ES> AesGcmSivStorage<DS, EDS, ES>
where
   DS: AsyncSink,
   EDS: 'static + Serializer<Entity = EncryptedData>,
   ES: 'static + Serializer<Entity = Entries>,
{
   pub fn new(
      data_source: Arc<AsyncRwLock<DS>>,
      password_hasher: Arc<dyn PasswordHasher>,
      key: &GenericArray<u8, U32>,
      phc_str: String,
      nonce: &GenericArray<u8, U12>,
   ) -> Self {
      Self {
         data_source,
         password_hasher,
         key: *key,
         phc_str,
         nonce: *nonce,
         encr_data_serde: PhantomData::<EDS>,
         entries_serde: PhantomData::<ES>,
      }
   }

   async fn async_store(encrypted_entries: Vec<u8>, data_source: Arc<AsyncRwLock<DS>>) -> Result<(), Error> {
      let mut data_source = data_source.write().await;

      data_source
         .seek(SeekFrom::Start(0))
         .map_err(|e| Error(e.to_string()))
         .await?;

      data_source
         .write_all(&encrypted_entries)
         .map_err(|e| Error(e.to_string()))
         .await?;

      data_source.flush().map_err(|e| Error(e.to_string())).await
   }
}

impl<DS, EDS, ES> EncryptEntries for AesGcmSivStorage<DS, EDS, ES>
where
   DS: AsyncSink,
   EDS: 'static + Serializer<Entity = EncryptedData>,
   ES: 'static + Serializer<Entity = Entries>,
{
   fn encrypt(&self, entries: &Entries) -> Result<Vec<u8>, encrypted::Error> {
      let mut serialized_entries = ES::serialize(entries).map_err(encrypted::Error)?;

      Aes256GcmSiv::new(&self.key)
         .encrypt_in_place(&self.nonce, use_cases::unlocking::AAD, &mut serialized_entries)
         .map_err(|e| encrypted::Error(e.to_string()))?;

      let encrypted_data = EncryptedData {
         header: EncryptionHeader {
            phc_str: self.phc_str.clone(),
            nonce: self.nonce.to_vec(),
         },
         payload: serialized_entries,
      };

      let data_to_write = EDS::serialize(&encrypted_data).map_err(encrypted::Error)?;

      Ok(data_to_write)
   }
}

impl<DS, EDS, ES> storage::Encrypted for AesGcmSivStorage<DS, EDS, ES>
where
   DS: AsyncSink,
   EDS: 'static + Serializer<Entity = EncryptedData>,
   ES: 'static + Serializer<Entity = Entries>,
{
   fn store(&mut self, entries: &Entries) -> BoxFuture<'static, Result<(), Error>> {
      let encrypted = self.encrypt(entries).map_err(|e| Error(e.0));

      if let Err(e) = encrypted {
         return async move { Err(Error(e.0)) }.boxed();
      }

      let data_source = Arc::clone(&self.data_source);

      let result = Self::async_store(encrypted.unwrap(), data_source);

      result.boxed()
   }

   fn update_encryption(
      &mut self,
      new_plain_password: String,
      entries: Entries,
   ) -> BoxFuture<'static, Result<(), Error>> {
      let new_encryption_header = EncryptionHeader::new();

      let new_password = self
         .password_hasher
         .hash_password(new_encryption_header.phc_str.as_str(), new_plain_password.as_bytes())
         .map_err(|e| Error(e.to_string()));

      if let Err(e) = new_password {
         return async move { Err(e) }.boxed();
      }

      self.key = *GenericArray::from_slice(new_password.unwrap().as_ref());
      self.phc_str = new_encryption_header.phc_str;
      self.nonce = *GenericArray::from_slice(new_encryption_header.nonce.as_slice());

      let encrypted = self.encrypt(&entries).map_err(|e| Error(e.0));

      if let Err(e) = encrypted {
         return async move { Err(e) }.boxed();
      }

      let data_source = Arc::clone(&self.data_source);

      let result = Self::async_store(encrypted.unwrap(), data_source);

      result.boxed()
   }
}

impl<DS, EDS, ES> Debug for AesGcmSivStorage<DS, EDS, ES>
where
   DS: AsyncSink,
   EDS: 'static + Serializer<Entity = EncryptedData>,
   ES: 'static + Serializer<Entity = Entries>,
{
   fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
      f.debug_struct("AesGcmSivDS")
         .field("data_source", &"<omitted>")
         .field("serializer", &"<omitted>")
         .field("password", &"<hidden>")
         .field("nonce", &"<hidden>")
         .finish()
   }
}

#[cfg(test)]
mod tests {
   use std::{
      io::{Result as IoResult, SeekFrom},
      pin::Pin,
      sync::Arc,
      task::{Context, Poll},
   };

   use aes_gcm_siv::{aead::AeadInPlace, Aes256GcmSiv, KeyInit};
   use async_lock::RwLock as AsyncRwLock;
   use futures::{AsyncSeek, AsyncWrite};
   use generic_array::GenericArray;

   use crate::{
      entities::{create_test_entry, encrypted::EncryptedData, entry::Entries, Deserializer, Serializer},
      repository::storage::{Encrypted, Error},
      scaffold_serialize,
      use_cases,
      use_cases::unlocking::{aes_gcm_siv_storage::AesGcmSivStorage, PasswordHasher, StubPasswordHasher},
   };

   const PASSWORD: [u8; 32] = [0; 32];
   /// Should be the actual Argon2d presets plus a test salt
   const PHC_STR: &str = "$argon2d$v=19$m=524288,t=3,p=3$AQIDBAUGBwgJCgsMDQ4PEA";
   const NONCE: [u8; 12] = [1u8, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

   #[pin_project::pin_project]
   #[derive(Debug)]
   struct TestDataSource {
      written: Vec<u8>,
   }

   impl TestDataSource {
      fn new(written: Vec<u8>) -> Self {
         TestDataSource { written }
      }

      /// `written` is held locked by [async_lock::RwLock] until released.
      /// Some tests might do consecutive .read() and .write() operations which don't get dropped
      /// until the end of the test. This causes a deadlock: written isn't released until the
      /// end of the test, but the test doesn't end until it has executed all the lines.
      fn written_copy(&self) -> Vec<u8> {
         self.written.clone()
      }
   }

   impl AsyncSeek for TestDataSource {
      fn poll_seek(self: Pin<&mut Self>, _: &mut Context<'_>, _: SeekFrom) -> Poll<IoResult<u64>> {
         let this = self.project();
         this.written.clear();

         Poll::Ready(Ok(0))
      }
   }

   impl AsyncWrite for TestDataSource {
      fn poll_write(self: Pin<&mut Self>, _: &mut Context<'_>, buf: &[u8]) -> Poll<IoResult<usize>> {
         let this = self.project();
         this.written.extend_from_slice(buf);

         Poll::Ready(Ok(buf.len()))
      }

      fn poll_flush(self: Pin<&mut Self>, _: &mut Context<'_>) -> Poll<IoResult<()>> {
         Poll::Ready(Ok(()))
      }

      fn poll_close(self: Pin<&mut Self>, _: &mut Context<'_>) -> Poll<IoResult<()>> {
         Poll::Ready(Ok(()))
      }
   }

   scaffold_serialize!(WorkingEntriesSer, Entries, entries, {
      Ok(format!("entries-{}", entries.values.len()).as_bytes().to_vec())
   });

   scaffold_serialize!(FailingEntriesSer, Entries, _entries, { Err("Error".to_owned()) });

   scaffold_serialize!(EncDataWorks, EncryptedData, enc_data, {
      let mut buffer: Vec<u8> = Vec::new();

      buffer.push(enc_data.header.phc_str.as_bytes().len() as u8);
      buffer.push(enc_data.header.nonce.len() as u8);
      buffer.push(enc_data.payload.len() as u8);

      buffer.extend_from_slice(enc_data.header.phc_str.as_bytes());
      buffer.extend_from_slice(enc_data.header.nonce.as_slice());
      buffer.extend_from_slice(enc_data.payload.as_slice());

      Ok(buffer)
   });

   struct WrittenEncryptedData {
      phc_str: Vec<u8>,
      nonce: Vec<u8>,
      payload: Vec<u8>,
   }

   impl WrittenEncryptedData {
      pub fn new(buf: &[u8]) -> Self {
         let pos_phc_str = (3 + buf[0]) as usize;
         let pos_nonce = pos_phc_str + buf[1] as usize;

         WrittenEncryptedData {
            phc_str: buf[3..pos_phc_str].to_vec(),
            nonce: buf[pos_phc_str..pos_nonce].to_vec(),
            payload: buf[pos_nonce..].to_vec(),
         }
      }
   }

   scaffold_serialize!(EncDataFails, EncryptedData, _enc_data, { Err("Error".to_owned()) });

   #[test]
   fn fails_if_entries_serialization_fails() {
      async_std::task::block_on(async {
         let key = GenericArray::from_slice(&PASSWORD);
         let nonce = GenericArray::from_slice(&NONCE);

         let data_source = Arc::new(AsyncRwLock::new(TestDataSource::new(Vec::new())));
         let mut encrypted_ds = AesGcmSivStorage::<TestDataSource, EncDataWorks, FailingEntriesSer>::new(
            Arc::clone(&data_source),
            Arc::new(StubPasswordHasher),
            key,
            "ignored".to_owned(),
            nonce,
         );

         let entries = Entries {
            values: Default::default(),
         };

         let result = encrypted_ds.store(&entries).await;

         assert!(matches!(result, Err(Error(_))));
         assert_eq!(data_source.read().await.written.len(), 0);
      });
   }

   #[test]
   fn fails_if_encrypted_data_serialization_fails() {
      async_std::task::block_on(async {
         let key = GenericArray::from_slice(&PASSWORD);
         let nonce = GenericArray::from_slice(&NONCE);

         let data_source = Arc::new(AsyncRwLock::new(TestDataSource::new(Vec::new())));
         let mut encrypted_ds = AesGcmSivStorage::<TestDataSource, EncDataFails, WorkingEntriesSer>::new(
            Arc::clone(&data_source),
            Arc::new(StubPasswordHasher),
            key,
            "ignored".to_owned(),
            nonce,
         );

         let entries = Entries {
            values: Default::default(),
         };

         let result = encrypted_ds.store(&entries).await;

         assert!(matches!(result, Err(Error(_))));
         assert_eq!(data_source.read().await.written.len(), 0);
      });
   }

   #[test]
   fn writes_entries_serialized_and_encrypted_along_with_cryptographic_parameters() {
      async_std::task::block_on(async {
         let key = GenericArray::from_slice(&PASSWORD);
         let phc_str = PHC_STR.to_owned();
         let nonce = GenericArray::from_slice(&NONCE);

         let data_source = Arc::new(AsyncRwLock::new(TestDataSource::new(Vec::new())));

         let mut encrypted_ds = AesGcmSivStorage::<TestDataSource, EncDataWorks, WorkingEntriesSer>::new(
            Arc::clone(&data_source),
            Arc::new(StubPasswordHasher),
            key,
            phc_str,
            nonce,
         );

         let entries = Entries {
            values: Default::default(),
         };

         let result = encrypted_ds.store(&entries).await;

         //Encrypted using all zeros 256bit password
         const ENCRYPTED_SERIALIZED_DATA: [u8; 93] = [
            53_u8, 12, 25, // sizes
            36, 97, 114, 103, 111, 110, 50, 100, 36, 118, 61, 49, 57, 36, 109, 61, 53, 50, 52, 50, 56, 56, 44,
            116, 61, 51, 44, 112, 61, 51, 36, 65, 81, 73, 68, 66, 65, 85, 71, 66, 119, 103, 74, 67, 103, 115, 77,
            68, 81, 52, 80, 69, 65, // phc_str
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, // nonce
            //payload: result from encrypted_serialized_entries() with empty entries serialized as "entries-0"
            126, 120, 158, 215, 63, 245, 135, 128, 21, 229, 25, 53, 113, 19, 130, 103, 130, 107, 109, 73, 238, 154,
            42, 41, 50,
         ];

         assert!(result.is_ok(), "{:?}", result);
         assert_eq!(data_source.read().await.written.as_slice(), ENCRYPTED_SERIALIZED_DATA);
      });
   }

   #[test]
   fn update_encryption_reencrypts_entries_serialized_and_with_new_cryptographic_parameters() {
      async_std::task::block_on(async {
         let data_source = Arc::new(AsyncRwLock::new(TestDataSource::new(Vec::new())));

         let mut encrypted_ds = AesGcmSivStorage::<TestDataSource, EncDataWorks, WorkingEntriesSer>::new(
            Arc::clone(&data_source),
            Arc::new(StubPasswordHasher),
            GenericArray::from_slice(&PASSWORD),
            PHC_STR.to_owned(),
            GenericArray::from_slice(&NONCE),
         );

         let entries = Entries {
            values: Default::default(),
         };

         // Initial write to the data store
         let result = encrypted_ds.store(&entries).await;
         assert!(result.is_ok(), "{:?}", result);

         let new_password = "abcdefg123";
         let result = encrypted_ds.update_encryption(new_password.to_owned(), entries).await;

         assert!(result.is_ok(), "{:?}", result);

         let written = data_source.read().await.written_copy();
         let new_written_encdata = WrittenEncryptedData::new(written.as_slice());

         assert_ne!(new_written_encdata.phc_str, PHC_STR.as_bytes());
         assert_ne!(new_written_encdata.nonce.as_slice(), &NONCE);

         let phc_str = String::from_utf8_lossy(new_written_encdata.phc_str.as_slice());

         assert!(phc_str.as_ref().starts_with("$argon2d$v=19$m=524288,t=3,p=3$"));

         let password = StubPasswordHasher
            .hash_password(phc_str.as_ref(), new_password.as_bytes())
            .unwrap();

         let cipher = Aes256GcmSiv::new(GenericArray::from_slice(password.as_slice()));

         let new_nonce = GenericArray::from_slice(new_written_encdata.nonce.as_slice());

         let mut buffer: Vec<u8> = new_written_encdata.payload;

         let result = cipher.decrypt_in_place(new_nonce, use_cases::unlocking::AAD, &mut buffer);

         assert!(result.is_ok(), "{:?}", result);

         assert_eq!(buffer.as_slice(), b"entries-0");
      });
   }

   #[test]
   fn update_encryption_reencrypts_with_new_cryptographic_parameters_after_adding_new_entry() {
      async_std::task::block_on(async {
         let data_source = Arc::new(AsyncRwLock::new(TestDataSource::new(Vec::new())));

         let mut encrypted_ds = AesGcmSivStorage::<TestDataSource, EncDataWorks, WorkingEntriesSer>::new(
            Arc::clone(&data_source),
            Arc::new(StubPasswordHasher),
            GenericArray::from_slice(&PASSWORD),
            "1234567890123456".to_owned(),
            GenericArray::from_slice(&NONCE),
         );

         // First encryption with no entries
         let mut entries = Entries {
            values: Default::default(),
         };

         let result = encrypted_ds.store(&entries).await;
         assert!(result.is_ok(), "{:?}", result);

         // Update encryption parameters and add new entry
         let new_password = "abcdefg123";
         let result = encrypted_ds
            .update_encryption(new_password.to_owned(), entries.clone())
            .await;

         assert!(result.is_ok(), "{:?}", result);

         entries.values.insert(create_test_entry(0));

         let result = encrypted_ds.store(&entries).await;
         assert!(result.is_ok(), "{:?}", result);

         // Check new entry is encrypted as expected and with new parameters
         let written = data_source.read().await.written_copy();
         let new_written_encdata = WrittenEncryptedData::new(written.as_slice());

         assert_ne!(new_written_encdata.phc_str, PHC_STR.as_bytes());
         assert_ne!(new_written_encdata.nonce.as_slice(), &NONCE);

         let phc_str = String::from_utf8_lossy(new_written_encdata.phc_str.as_slice());

         assert!(phc_str.as_ref().starts_with("$argon2d$v=19$m=524288,t=3,p=3$"));

         let password = StubPasswordHasher
            .hash_password(phc_str.as_ref(), new_password.as_bytes())
            .unwrap();

         let cipher = Aes256GcmSiv::new(GenericArray::from_slice(password.as_slice()));

         let new_nonce = GenericArray::from_slice(new_written_encdata.nonce.as_slice());

         let mut buffer: Vec<u8> = new_written_encdata.payload;

         let result = cipher.decrypt_in_place(new_nonce, use_cases::unlocking::AAD, &mut buffer);

         assert!(result.is_ok(), "{:?}", result);

         assert_eq!(buffer.as_slice(), b"entries-1");
      });
   }

   #[test]
   #[ignore]
   #[allow(clippy::print_stdout, clippy::use_debug)]
   fn encrypted_serialized_entries() {
      use aes_gcm_siv::{
         aead::{generic_array::GenericArray, AeadInPlace},
         Aes256GcmSiv,
      };

      const PHC_STR_BYTES: &[u8] = PHC_STR.as_bytes();
      println!(
         "Test PHC string = {:?}, length = {}",
         PHC_STR_BYTES,
         PHC_STR_BYTES.len()
      );

      let mut encrypted: Vec<u8> = b"entries-1".to_vec();
      let key = GenericArray::from_slice(&PASSWORD);
      let nonce = GenericArray::from_slice(&NONCE);

      const TEST_AAD: &[u8; 20] = b"fastotp-verification";

      Aes256GcmSiv::new(key)
         .encrypt_in_place(nonce, TEST_AAD, &mut encrypted)
         .expect("encryption failure");

      println!("Encrypted buffer = {:?}, length = {}", encrypted, encrypted.len());
   }
}
