/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! Logic that allows generating an [Entry] as a QR code to easily share it between other applications

use core::time::Duration;

use qrcode::{
   render::{svg, Pixel},
   types::QrError,
   QrCode,
};

use crate::entities::entry::Entry;

/// For failures when generating a QR code
#[derive(Clone, Debug, thiserror::Error, Eq, PartialEq)]
pub enum Error {
   /// If the TOTP URL is too long to encode in a QR code.
   #[error("TOTP URL length is exceeds QR encoding length limit")]
   DataTooLong,

   /// If there's an uncontrolled error while generating the QR code.
   #[error("Unexpected error while generate QR code: {0}")]
   UnexpectedQRError(String),
}

/// Used to encode an [Entry] as an URL and store it in a QR code. This allows to easily share added
/// entries.
pub struct QrGeneration;

impl QrGeneration {
   pub fn new() -> Self {
      Self
   }

   /// Generate a QR code of the [Entry] encoded as an URL
   ///
   /// # Errors
   /// Any kind of error while trying to generate the QR code will return an [Error]
   pub fn generate(&self, entry: &Entry) -> Result<Vec<u8>, Error> {
      use percent_encoding::{AsciiSet, CONTROLS};

      /// <https://url.spec.whatwg.org/#query-percent-encode-set>
      const QUERY_ENCODE_SET: &AsciiSet = &CONTROLS.add(b' ').add(b'"').add(b'#').add(b'<').add(b'>');

      /// <https://url.spec.whatwg.org/#path-percent-encode-set>
      const PATH_ENCODE_SET: &AsciiSet = &QUERY_ENCODE_SET.add(b'?').add(b'`').add(b'{').add(b'}');

      let description_encoded =
         percent_encoding::utf8_percent_encode(&entry.description, PATH_ENCODE_SET).to_string();

      let issuer_encoded = percent_encoding::utf8_percent_encode(&entry.issuer, QUERY_ENCODE_SET).to_string();

      let time_step: Duration = entry.time_step.into();

      let url = format!(
         "otpauth://totp/{description}?secret={secret}&issuer={issuer}&algorithm=SHA1&digits=6&period={period}",
         description = description_encoded,
         secret = entry.secret.as_ref(),
         issuer = issuer_encoded,
         period = time_step.as_secs(),
      );

      Self::generate_svg(url.as_bytes())
   }

   fn generate_svg(data: &[u8]) -> Result<Vec<u8>, Error> {
      let code = QrCode::with_version(data, qrcode::Version::Normal(14), qrcode::EcLevel::L)?;

      Ok(code
         .render()
         .min_dimensions(200, 200)
         .dark_color(svg::Color::default_color(qrcode::Color::Dark))
         .light_color(svg::Color::default_color(qrcode::Color::Light))
         .build()
         .into_bytes())
   }
}

impl From<QrError> for Error {
   fn from(error: QrError) -> Self {
      match error {
         QrError::DataTooLong => Self::DataTooLong,
         QrError::InvalidVersion
         | QrError::InvalidCharacter
         | QrError::InvalidEciDesignator
         | QrError::UnsupportedCharacterSet => Self::UnexpectedQRError(format!("{error}")),
      }
   }
}

impl Default for QrGeneration {
   fn default() -> Self {
      Self::new()
   }
}

#[cfg(test)]
mod tests {
   use std::{convert::TryInto, time::Duration};

   use tiny_skia_path::Transform;
   use usvg::{tiny_skia_path, TreeParsing};

   use crate::{
      entities::create_test_entry,
      use_cases::qr_generation::{Error, QrGeneration},
   };

   #[test]
   fn entry_is_qr_encoded_as_valid_url() {
      let qr_generation = QrGeneration::new();
      let mut entry = create_test_entry(0);
      entry.description = r#"{`<#"My Description">?`}"#.to_owned();
      entry.issuer = r#"{`<#"My Issuer">?`}"#.to_owned();
      entry.time_step = Duration::from_secs(30).try_into().unwrap();

      let qr_code = qr_generation.generate(&entry);
      assert!(qr_code.is_ok(), "{:?}", qr_code);

      let qr_code = qr_code.unwrap();

      let rendered_qr = render_svg_as_png(&qr_code);
      let data = decode_qr_code(&rendered_qr);

      assert_eq!(data.len(), 1);
      assert_eq!(
         data[0],
         "otpauth://totp/%7B%60%3C%23%22My%20Description%22%3E%3F%60%7D?secret=ONSWG4TFOQQDA&issuer={`%3C%23%\
          22My%20Issuer%22%3E?`}&algorithm=SHA1&digits=6&period=30"
      )
   }

   #[test]
   fn fails_if_url_is_too_long() {
      let qr_generation = QrGeneration::new();
      let mut entry = create_test_entry(0);
      entry.time_step = Duration::from_secs(30).try_into().unwrap();

      // Enough number of bits to trigger a too long exception error even with QR code version 40 with error correction L
      entry.issuer = String::with_capacity(36000 / 8);
      for _ in 0..entry.issuer.capacity() {
         entry.issuer.push('A');
      }

      let qr_code = qr_generation.generate(&entry);

      let error = qr_code.unwrap_err();

      assert_eq!(error, Error::DataTooLong);
   }

   fn render_svg_as_png(qr_code_img: &[u8]) -> Vec<u8> {
      let opt = usvg::Options::default();

      let tree = usvg::Tree::from_data(qr_code_img, &opt).unwrap();
      let rtree = resvg::Tree::from_usvg(&tree);

      let pixmap_size = rtree.size.to_int_size();

      let mut pixmap = resvg::tiny_skia::Pixmap::new(pixmap_size.width(), pixmap_size.height()).unwrap();

      rtree.render(Transform::identity(), &mut pixmap.as_mut());

      let mut png_image = Vec::new();
      // Encode as PNG
      {
         let w = std::io::BufWriter::new(&mut png_image);
         let mut encoder = png::Encoder::new(w, pixmap_size.width(), pixmap_size.height());
         encoder.set_color(png::ColorType::Rgba);
         encoder.set_depth(png::BitDepth::Eight);

         let mut writer = encoder.write_header().unwrap();
         writer
            .write_image_data(pixmap.data_mut())
            .expect("Failed to write image data");
      }

      png_image
   }

   fn decode_qr_code(qr_code_image: &[u8]) -> Vec<String> {
      let img = image::load_from_memory(qr_code_image).unwrap();
      let decoder = bardecoder::default_decoder();
      let results = decoder.decode(&img);

      results.into_iter().map(|r| r.unwrap()).collect()
   }
}
