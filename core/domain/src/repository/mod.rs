/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! Related logic of how to store and retrieve [Entries]

pub mod storage;

use core::fmt::{Debug, Formatter};
use std::{collections::HashMap, sync::Arc};

use async_lock::RwLock as AsyncRwLock;
use futures::{future::BoxFuture, stream::BoxStream, FutureExt};
use futures_signals::signal::{Mutable, SignalExt};

use crate::{
   entities::entry::{Entries, Entry, Id},
   repository::storage::Encrypted,
};

/// Errors generated during repository operations.
#[derive(Clone, Debug, thiserror::Error, Eq, PartialEq)]
pub enum Error {
   /// For failed storage operations
   #[error("couldn't write to the data store: {0}")]
   Storage(String),

   /// For nonexistent [Entry] in the repository
   #[error("the entry doesn't exist")]
   EntryDoesntExist,
}

/// Represents the operations that can be made to save, delete and load [Entry]
pub trait EntriesRepository: Send + Sync + Debug {
   /// Saves an [Entry], or updates an existing one, in the repository and returns the result of the operation
   ///
   /// # Errors
   /// Any kind of error while trying to save will return an [Error]
   fn save_or_update(&mut self, entry: Entry) -> BoxFuture<'static, Result<(), Error>>;

   /// Finds an [Entry] given an [Id] in the repository and returns the result of the operation
   ///
   /// # Errors
   /// Any kind of error while performing the operation will result in an [Error]
   fn find(&self, id: &Id) -> BoxFuture<'static, Result<Option<Entry>, Error>>;

   /// Deletes an [Entry] from the repository and returns the result of the operation
   ///
   /// # Errors
   /// Any kind of error while trying to delete the entry will return an [Error]
   fn delete(&mut self, id: &Id) -> BoxFuture<'static, Result<(), Error>>;

   /// Fetches a snapshot of the [Entries] currently available
   ///
   /// # Errors
   /// Any kind of error during processing will return an [Error]
   fn all(&self) -> BoxFuture<'static, Result<Entries, Error>>;

   /// Continuous stream of new states of the database after write changes to the database
   fn changes(&self) -> BoxStream<'static, Entries>;
}

/// The most basic implementation of a repository. Used internally during testing
#[derive(Debug, Default)]
pub struct InMemory {
   entries: HashMap<Id, Entry>,
   changes: Mutable<Entries>,
}

impl InMemory {
   pub fn new() -> Self {
      Self::default()
   }

   #[inline]
   fn publish_change(&self) {
      let entries: Entries = (&self.entries).into();

      self.changes.set(entries);
   }
}

impl EntriesRepository for InMemory {
   fn save_or_update(&mut self, entry: Entry) -> BoxFuture<'static, Result<(), Error>> {
      self.entries.insert(entry.id, entry);

      self.publish_change();

      async move { Ok(()) }.boxed()
   }

   fn find(&self, id: &Id) -> BoxFuture<'static, Result<Option<Entry>, Error>> {
      let tmp = self.entries.get(id).cloned();

      async move { Ok(tmp) }.boxed()
   }

   fn delete(&mut self, id: &Id) -> BoxFuture<'static, Result<(), Error>> {
      if self.entries.remove(id).is_some() {
         self.publish_change();

         async move { Ok(()) }.boxed()
      }
      else {
         async move { Err(Error::EntryDoesntExist) }.boxed()
      }
   }

   fn all(&self) -> BoxFuture<'static, Result<Entries, Error>> {
      let tmp: Entries = (&self.entries).into();

      async move { Ok(tmp) }.boxed()
   }

   fn changes(&self) -> BoxStream<'static, Entries> {
      Box::pin(self.changes.signal_cloned().to_stream())
   }
}

/// Writes and reads [Entries] from an arbitrary data source
pub struct DurableRepository<Storage>
where
   Storage: 'static + Encrypted + Unpin,
{
   storage: Arc<AsyncRwLock<Storage>>,
   cache: Arc<AsyncRwLock<HashMap<Id, Entry>>>,
   changes: Mutable<Entries>,
}

impl<Storage> DurableRepository<Storage>
where
   Storage: 'static + Encrypted + Unpin,
{
   /// Create a new repository which will persist the information in a durable fashion
   pub fn new(storage: Arc<AsyncRwLock<Storage>>, entries: Entries) -> Self {
      let mut entries_map: HashMap<Id, Entry> = HashMap::new();

      for entry in &entries.values {
         entries_map.insert(entry.id, entry.clone());
      }

      Self {
         storage,
         cache: Arc::new(AsyncRwLock::new(entries_map)),
         changes: Mutable::new(entries),
      }
   }
}

impl<Storage> EntriesRepository for DurableRepository<Storage>
where
   Storage: 'static + Encrypted + Unpin,
{
   fn save_or_update(&mut self, entry: Entry) -> BoxFuture<'static, Result<(), Error>> {
      let key = entry.id;

      let storage = Arc::clone(&self.storage);
      let cache = Arc::clone(&self.cache);
      let changes = self.changes.clone();

      async move {
         let mut cache = cache.write().await;
         let old_value = cache.insert(entry.id, entry);

         let entries = Entries {
            values: cache.values().cloned().collect(),
         };

         let write_result = storage
            .write()
            .await
            .store(&entries)
            .await
            .map_err(|e| Error::Storage(e.to_string()));

         if write_result.is_err() {
            // Revert in-memory state if writing fails
            if let Some(old) = old_value {
               cache.insert(key, old);
            }
            else {
               cache.remove(&key);
            }

            drop(cache);
         }
         else {
            changes.set(entries);
         }

         write_result
      }
      .boxed()
   }

   fn find(&self, id: &Id) -> BoxFuture<'static, Result<Option<Entry>, Error>> {
      let cache = Arc::clone(&self.cache);
      let id = *id;

      async move {
         let tmp = cache.read().await.get(&id).cloned();

         Ok(tmp)
      }
      .boxed()
   }

   fn delete(&mut self, id: &Id) -> BoxFuture<'static, Result<(), Error>> {
      let storage = Arc::clone(&self.storage);
      let cache = Arc::clone(&self.cache);
      let changes = self.changes.clone();
      let id = *id;

      async move {
         let mut cache = cache.write().await;

         let old_value = cache.remove(&id);
         if let Some(old) = old_value {
            let entries = Entries {
               values: cache.values().cloned().collect(),
            };

            let write_result = storage
               .write()
               .await
               .store(&entries)
               .await
               .map_err(|e| Error::Storage(e.to_string()));

            if write_result.is_err() {
               cache.insert(old.id, old);
               drop(cache);
               return write_result;
            }

            changes.set(entries);

            write_result
         }
         else {
            Err(Error::EntryDoesntExist)
         }
      }
      .boxed()
   }

   fn all(&self) -> BoxFuture<'static, Result<Entries, Error>> {
      let cache = Arc::clone(&self.cache);

      async move {
         let tmp = cache.read().await.values().cloned().collect();

         Ok(Entries { values: tmp })
      }
      .boxed()
   }

   fn changes(&self) -> BoxStream<'static, Entries> {
      Box::pin(self.changes.signal_cloned().to_stream())
   }
}

impl<Storage> Debug for DurableRepository<Storage>
where
   Storage: 'static + Encrypted + Unpin,
{
   fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
      f.debug_struct("BinaryFile")
         .field("data_source", &"<omitted>")
         .field("cache", &"<omitted>")
         .field("changes", &self.changes)
         .finish()
   }
}

#[cfg(test)]
mod tests {
   use std::{
      collections::HashSet,
      convert::TryInto,
      sync::{Arc, RwLock},
      time::Duration,
   };

   use async_lock::RwLock as AsyncRwLock;
   use futures::{future::BoxFuture, FutureExt, StreamExt};

   use crate::{
      entities::{
         create_test_entry,
         encrypted,
         encrypted::EncryptEntries,
         entry::{Entries, Entry, Id},
      },
      repository::{
         storage,
         storage::{Error as StorageError, StubEncryptedStorage},
         DurableRepository,
         EntriesRepository,
         Error,
      },
   };

   #[derive(Debug)]
   struct TestStorage {
      should_succeed: Arc<RwLock<bool>>,
      entries: Arc<RwLock<Entries>>,
   }

   impl EncryptEntries for TestStorage {
      fn encrypt(&self, _entries: &Entries) -> Result<Vec<u8>, encrypted::Error> {
         unimplemented!()
      }
   }

   impl storage::Encrypted for TestStorage {
      fn store(&mut self, entries: &Entries) -> BoxFuture<'static, Result<(), StorageError>> {
         *self.entries.try_write().unwrap() = entries.clone();

         if *self.should_succeed.read().unwrap() {
            return async { Ok(()) }.boxed();
         }

         async { Err(StorageError("error".to_owned())) }.boxed()
      }

      fn update_encryption(&mut self, _: String, _: Entries) -> BoxFuture<'static, Result<(), StorageError>> {
         async move { Ok(()) }.boxed()
      }
   }

   #[test]
   fn find_and_new_work() {
      async_std::task::block_on(async {
         let storage = Arc::new(AsyncRwLock::new(StubEncryptedStorage));
         let repository = DurableRepository::new(Arc::clone(&storage), Entries::default());

         let entry_id = Id::generate();

         let result = repository.find(&entry_id).await;

         assert_eq!(result, Ok(None));

         let mut entries_values = HashSet::new();

         for _ in 0..8 {
            let entry_id = Id::generate();
            let entry = Entry {
               id: entry_id,
               issuer: "issuer".to_owned(),
               description: "description".to_owned(),
               secret: "secret".try_into().unwrap(),
               time_step: Duration::from_secs(1).try_into().unwrap(),
            };
            let expected = entry.clone();

            entries_values.insert(entry);

            let repository = DurableRepository::new(
               Arc::clone(&storage),
               Entries {
                  values: entries_values.clone(),
               },
            );

            let result = repository.find(&entry_id).await;

            assert_eq!(result, Ok(Some(expected)));
         }
      });
   }

   #[test]
   fn save_or_replace_returns_ok_if_entries_are_stored() {
      async_std::task::block_on(async {
         let entries = Arc::new(RwLock::new(Entries::default()));

         let storage = TestStorage {
            should_succeed: Arc::new(RwLock::new(true)),
            entries: Arc::clone(&entries),
         };
         let storage = Arc::new(AsyncRwLock::new(storage));

         let mut repository = DurableRepository::new(storage, Entries::default());

         let entry = create_test_entry(0);

         let result = repository.save_or_update(entry.clone()).await;

         assert_eq!(result, Ok(()));

         let mut entries_values = HashSet::new();
         entries_values.insert(entry);

         let entries = entries.read().unwrap();

         assert_eq!(entries.values, entries_values);
      });
   }

   #[test]
   fn save_or_replace_reverts_state_if_entries_arent_stored() {
      async_std::task::block_on(async {
         let should_succeed = Arc::new(RwLock::new(true));

         let storage = TestStorage {
            should_succeed: Arc::clone(&should_succeed),
            entries: Arc::new(RwLock::new(Entries::default())),
         };
         let storage = Arc::new(AsyncRwLock::new(storage));

         let mut repository = DurableRepository::new(storage, Entries::default());

         // First insert successfully
         let entry_saved = create_test_entry(0);

         let result = repository.save_or_update(entry_saved.clone()).await;

         assert_eq!(result, Ok(()));

         let mut expected = HashSet::new();
         expected.insert(entry_saved);

         let expected_entries = Entries {
            values: expected.clone(),
         };
         assert_eq!(repository.changes().next().await.unwrap(), expected_entries);

         // Second insert has to fail so that it attempts to revert
         *should_succeed.try_write().unwrap() = false;

         let entry_not_saved = create_test_entry(1);

         let result = repository.save_or_update(entry_not_saved).await;

         assert!(matches!(result, Err(Error::Storage(_))));
         assert_eq!(repository.changes().next().await.unwrap(), expected_entries);

         // third insert should succeed and not show second insert when querying changes
         *should_succeed.try_write().unwrap() = true;

         let entry_saved_2 = create_test_entry(2);

         expected.insert(entry_saved_2.clone());
         let expected_entries = Entries { values: expected };

         let result = repository.save_or_update(entry_saved_2.clone()).await;

         assert_eq!(result, Ok(()));
         assert_eq!(repository.changes().next().await.unwrap(), expected_entries);

         // Update has to fail so it attempts to revert and changes emit unchanged results
         let mut entry_not_updated = create_test_entry(3);
         entry_not_updated.id = entry_saved_2.id;

         *should_succeed.try_write().unwrap() = false;

         let result = repository.save_or_update(entry_not_updated).await;

         assert!(matches!(result, Err(Error::Storage(_))));
         assert_eq!(repository.changes().next().await.unwrap(), expected_entries);
      });
   }

   #[test]
   fn delete_returns_ok_if_entry_is_deleted() {
      async_std::task::block_on(async {
         let entries = Arc::new(RwLock::new(Entries::default()));

         let storage = TestStorage {
            should_succeed: Arc::new(RwLock::new(true)),
            entries: Arc::clone(&entries),
         };
         let storage = Arc::new(AsyncRwLock::new(storage));

         let entry_deleted = create_test_entry(1);
         let id = entry_deleted.id;

         let mut values = HashSet::new();
         values.insert(create_test_entry(0));
         values.insert(entry_deleted);

         let mut repository = DurableRepository::new(storage, Entries { values });

         let result = repository.delete(&id).await;

         assert_eq!(result, Ok(()));

         let mut expected = HashSet::new();
         expected.insert(create_test_entry(0));

         let entries = entries.read().unwrap();

         assert_eq!(entries.values, expected);
      });
   }

   #[test]
   fn all_returns_all_entries_stored() {
      async_std::task::block_on(async {
         let storage = TestStorage {
            should_succeed: Arc::new(RwLock::new(true)),
            entries: Arc::new(RwLock::new(Entries::default())),
         };
         let storage = Arc::new(AsyncRwLock::new(storage));

         let mut repository = DurableRepository::new(storage, Entries::default());

         let result = repository.save_or_update(create_test_entry(0)).await;
         assert_eq!(result, Ok(()));
         let result = repository.save_or_update(create_test_entry(1)).await;
         assert_eq!(result, Ok(()));
         let result = repository.save_or_update(create_test_entry(2)).await;
         assert_eq!(result, Ok(()));

         let result = repository.all().await;

         let mut expected = HashSet::new();
         expected.insert(create_test_entry(0));
         expected.insert(create_test_entry(1));
         expected.insert(create_test_entry(2));

         assert_eq!(result.unwrap().values, expected);
      });
   }
}
