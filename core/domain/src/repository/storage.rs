/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! Related logic of how to store [Entries]

use core::{
   fmt::Debug,
   pin::Pin,
   task::{Context, Poll},
};
use std::io::{Result as IoResult, SeekFrom};

use futures::{future::BoxFuture, AsyncRead, AsyncSeek, AsyncWrite, FutureExt};

use crate::entities::{encrypted, encrypted::EncryptEntries, entry::Entries};

/// Errors generated during storage operations.
#[derive(Clone, Debug, thiserror::Error, Eq, PartialEq)]
#[error("couldn't write to the storage: {0}")]
pub struct Error(pub String);

pub trait AsyncSink: 'static + AsyncWrite + AsyncSeek + Send + Sync + Unpin {}

impl<T> AsyncSink for T where T: 'static + AsyncWrite + AsyncSeek + Send + Sync + Unpin {}

/// To allow writing [Entries] into a durable storage, usually a file.
pub trait Encrypted: EncryptEntries {
   /// Writes [Entries] into the storage
   ///
   /// # Errors
   /// Any kind of error while trying to write will return an [Error]
   fn store(&mut self, entries: &Entries) -> BoxFuture<'static, Result<(), Error>>;

   /// Updates the encryption parameters and re-encrypts the stored data with the new encryption password
   ///
   /// # Errors
   /// Any kind of error while processing will return an [Error]
   fn update_encryption(
      &mut self,
      new_plain_password: String,
      entries: Entries,
   ) -> BoxFuture<'static, Result<(), Error>>;
}

#[derive(Debug)]
pub struct StubEncryptedStorage;

impl AsyncSeek for StubEncryptedStorage {
   fn poll_seek(self: Pin<&mut Self>, _: &mut Context<'_>, _: SeekFrom) -> Poll<IoResult<u64>> {
      Poll::Ready(Ok(0))
   }
}

impl AsyncRead for StubEncryptedStorage {
   fn poll_read(self: Pin<&mut Self>, _: &mut Context<'_>, _: &mut [u8]) -> Poll<IoResult<usize>> {
      Poll::Ready(Ok(0))
   }
}

impl EncryptEntries for StubEncryptedStorage {
   fn encrypt(&self, _: &Entries) -> Result<Vec<u8>, encrypted::Error> {
      Ok(vec![])
   }
}

impl Encrypted for StubEncryptedStorage {
   fn store(&mut self, _: &Entries) -> BoxFuture<'static, Result<(), Error>> {
      async move { Ok(()) }.boxed()
   }

   fn update_encryption(&mut self, _: String, _: Entries) -> BoxFuture<'static, Result<(), Error>> {
      async move { Ok(()) }.boxed()
   }
}

impl AsyncWrite for StubEncryptedStorage {
   fn poll_write(self: Pin<&mut Self>, _: &mut Context<'_>, _: &[u8]) -> Poll<IoResult<usize>> {
      Poll::Ready(Ok(0))
   }

   fn poll_flush(self: Pin<&mut Self>, _: &mut Context<'_>) -> Poll<IoResult<()>> {
      Poll::Ready(Ok(()))
   }

   fn poll_close(self: Pin<&mut Self>, _: &mut Context<'_>) -> Poll<IoResult<()>> {
      Poll::Ready(Ok(()))
   }
}
