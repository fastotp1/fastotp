/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! Contains the logic required to save, update and generate TOTP codes.\
//! User data is protected using strong encryption.
//! The password may be changed resulting in an immediate re-encryption to disk of all user data.

#![allow(
   clippy::doc_markdown,
   clippy::items_after_statements,
   clippy::missing_fields_in_debug,
   clippy::missing_panics_doc,
   clippy::must_use_candidate,
   clippy::suspicious_else_formatting
)]
#![deny(
   array_into_iter,
   clippy::rest_pat_in_fully_bound_structs,
   clippy::separated_literal_suffix,
   non_camel_case_types,
   private_in_public,
   rust_2018_idioms,
   trivial_casts,
   trivial_numeric_casts,
   type_alias_bounds,
   unknown_lints,
   unsafe_code,
   unused,
   unused_import_braces,
   while_true
)]

pub mod entities;
pub mod repository;
pub mod use_cases;
