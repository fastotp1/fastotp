/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! Main entities used by the application. They represent either the information required to
//! generate an TOTP code ([Entry]) or the total of entries along with the required cryptographic
//! parameters to decrypt them once read from storage.

pub mod encrypted;
pub mod entry;

use core::{
   convert::{TryFrom, TryInto},
   time::Duration,
};

use ulid::Ulid;

use crate::entities::entry::{Entry, Id};

/// Process of serializing an entity into a buffer of bytes
pub trait Serializer: Sync + Send {
   /// Entity that will be serialized
   type Entity;

   /// Serializes the given entity into a bytes buffer
   ///
   /// # Errors
   ///
   /// If any error arises during the process an error message is returned
   fn serialize(_: &Self::Entity) -> Result<Vec<u8>, String>;
}

/// Process of deserializing an entity from a buffer of bytes
pub trait Deserializer: Sync + Send {
   /// Entity that will be deserialized
   type Entity;

   /// Deserializes a bytes buffer into an entity
   ///
   /// # Errors
   ///
   /// If any error arises during the process an error message is returned
   fn deserialize(buf: &[u8]) -> Result<Self::Entity, String>;
}

/// Represents the process of de/serializing an entity into/from a buffer of bytes
pub trait Serdelizer<E>: Deserializer<Entity = E> + Serializer<Entity = E> {}

impl<E, T: Deserializer<Entity = E> + Serializer<Entity = E>> Serdelizer<E> for T {}

/// These macros enable easier testing of functionlity that requires serialization or deserialization.
pub mod serdelizer_macros {
   /// Define an implementation of [Serializer](super::Serializer) using the provided body for serialization.
   #[macro_export]
   macro_rules! scaffold_serialize {
      ( $struct_name:ident, $entity:ident, $_entity:ident, $fn_body:block ) => {
         struct $struct_name;
         impl Serializer for $struct_name {
            type Entity = $entity;

            fn serialize($_entity: &Self::Entity) -> Result<Vec<u8>, String>
            $fn_body
         }

         impl Deserializer for $struct_name {
            type Entity = $entity;

            fn deserialize(_: &[u8]) -> Result<Self::Entity, String>
            {
               panic!("deserialize shouldn't be invoked");
            }
         }
      };
   }

   /// Define an implementation of [Deserializer](super::Deserializer) using the provided body for deserialization.
   #[macro_export]
   macro_rules! scaffold_deserialize {
      ( $struct_name:ident, $entity:ident, $buf:ident, $fn_body:block ) => {
         struct $struct_name;
         impl Deserializer for $struct_name {
            type Entity = $entity;

            fn deserialize($buf: &[u8]) -> Result<Self::Entity, String>
            $fn_body
         }

         impl Serializer for $struct_name {
            type Entity = $entity;

            fn serialize(_: &Self::Entity) -> Result<Vec<u8>, String>
            {
               panic!("serialize shouldn't be invoked");
            }
         }
      };
   }

   /// Define an implementation of [Serializer](super::Serializer) and [Deserializer](super::Deserializer) using the provided bodies
   #[macro_export]
   macro_rules! scaffold_serdelizer {
      ( $struct_name:ident, $entity:ident, $input:ident, $ser_body:block, $des_body:block ) => {
         struct $struct_name;
         impl Serializer for $struct_name {
            type Entity = $entity;

            fn serialize($input: &Self::Entity) -> Result<Vec<u8>, String>
            $ser_body
         }
         impl Deserializer for $struct_name {
            type Entity = $entity;

            fn deserialize($input: &[u8]) -> Result<Self::Entity, String>
            $des_body
         }
      };
   }
}

/// Creates a valid test [Entry] in a deterministic way, that is: for the same `i` value, two invocations return
/// the exact same [Entry].
pub fn create_test_entry(i: u64) -> Entry {
   let ulid: Ulid = (0u64, i).into();

   let not_encoded_secret = format!("secret {i}");

   let secret = base32::encode(
      base32::Alphabet::RFC4648 { padding: false },
      not_encoded_secret.as_bytes(),
   );

   Entry {
      id: Id::try_from(ulid.to_string().as_str()).unwrap(),
      issuer: format!("Amafacebooglesoft {i}"),
      description: format!("A test entry {i}"),
      secret: secret.try_into().unwrap(),
      time_step: Duration::from_secs(i + 1).try_into().unwrap(),
   }
}
