/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! Entities with data required for TOTP functionality.

use core::{cmp::Ordering, convert::TryFrom, hash::Hash, time::Duration};
use std::collections::{hash_map::RandomState, HashMap, HashSet};

/// Uniquely identifies an [Entry]. Required to identify an entry when updating its information
#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct Id(ulid::Ulid);

impl Id {
   #[inline]
   pub fn generate() -> Self {
      Self(ulid::Ulid::new())
   }
}

impl Default for Id {
   fn default() -> Self {
      Self::generate()
   }
}

impl ToString for Id {
   fn to_string(&self) -> String {
      self.0.to_string()
   }
}

impl TryFrom<&str> for Id {
   type Error = String;

   fn try_from(value: &str) -> Result<Self, Self::Error> {
      ulid::Ulid::from_string(value)
         .map(Self)
         .map_err(|e| format!("Invalid ULID: {e}"))
   }
}

/// Base32 encoded and cleaned up secret used to generate TOTP values.
///
/// Ensures that once created always represents a valid secret used in [RFC6238](https://tools.ietf.org/html/rfc6238).
#[derive(Clone, Debug, Hash, PartialEq, Eq)]
pub struct Secret(String);

impl Secret {
   /// Creates a valid [Secret]
   ///
   /// # Errors
   ///
   /// If the passed raw secret is empty or not Base32 encoded.
   pub fn new<RawSecret: Into<String>>(secret: RawSecret) -> Result<Self, SecretError> {
      let s = secret.into();
      if s.is_empty() {
         return Err(SecretError::Empty);
      }

      let cleaned_up_secret = s.replace(' ', "").to_uppercase();

      let is_badly_encoded =
         base32::decode(base32::Alphabet::RFC4648 { padding: false }, &cleaned_up_secret).is_none();

      if is_badly_encoded {
         return Err(SecretError::InvalidBase32Encoded);
      }

      Ok(Self(cleaned_up_secret))
   }
}

/// Error thrown when failing to parse a String as a [Secret]
#[derive(Clone, Debug, thiserror::Error, Eq, PartialEq)]
pub enum SecretError {
   /// The passed string is empty
   #[error("the secret mustn't be empty")]
   Empty,

   /// The string isn't correctly Base32 encoded
   #[error("invalid base32 encoded secret provided")]
   InvalidBase32Encoded,
}

impl TryFrom<&str> for Secret {
   type Error = SecretError;

   fn try_from(value: &str) -> Result<Self, Self::Error> {
      Self::new(value)
   }
}

impl TryFrom<String> for Secret {
   type Error = SecretError;

   fn try_from(value: String) -> Result<Self, Self::Error> {
      Self::new(value)
   }
}

impl AsRef<str> for Secret {
   fn as_ref(&self) -> &str {
      &self.0
   }
}

impl From<Secret> for String {
   fn from(secret: Secret) -> Self {
      secret.0
   }
}

impl From<&Secret> for String {
   fn from(secret: &Secret) -> Self {
      secret.0.clone()
   }
}

/// Non-zero [Duration] representing a time-step to calculate an TOTP.
#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct TimeStep(Duration);

impl TimeStep {
   /// Creates a valid [TimeStep]
   ///
   /// # Errors
   ///
   /// If the duration is less than 1 second since [RFC6238](https://tools.ietf.org/html/rfc6238) requires time steps
   /// in seconds greater than 0.
   pub const fn new(time_step: Duration) -> Result<Self, TimeStepError> {
      if time_step.as_secs() < 1 {
         return Err(TimeStepError);
      }

      Ok(Self(time_step))
   }
}

/// Error thrown when the passed duration is less than 1 second.
#[derive(Clone, Copy, Debug, thiserror::Error, Eq, PartialEq)]
#[error("time-step must be greater than zero")]
pub struct TimeStepError;

impl TryFrom<Duration> for TimeStep {
   type Error = TimeStepError;

   fn try_from(value: Duration) -> Result<Self, Self::Error> {
      Self::new(value)
   }
}

impl From<TimeStep> for Duration {
   fn from(time_step: TimeStep) -> Self {
      time_step.0
   }
}

/// Data needed to represent an TOTP record in the system. This is used to calculate an TOTP code.
#[derive(Clone, Debug, Hash, PartialEq, Eq)]
pub struct Entry {
   pub id: Id,

   /// Name of the issuer of the secret (AWS, Google, Twitch, ...)
   pub issuer: String,

   /// Additional description used ("Personal e-mail", "Business e-mail", "First", ...)
   pub description: String,

   /// [Secret] used to calculate the TOTP value
   pub secret: Secret,

   /// Time step to use with the secret. 30 seconds by default.
   pub time_step: TimeStep,
}

impl PartialOrd for Entry {
   fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
      self.issuer.partial_cmp(&other.issuer).and_then(|ord| {
         match ord {
            Ordering::Equal => self.description.partial_cmp(&other.description),
            Ordering::Less | Ordering::Greater => Some(ord),
         }
      })
   }
}

impl Ord for Entry {
   fn cmp(&self, other: &Self) -> Ordering {
      let o = self.issuer.cmp(&other.issuer);
      match o {
         Ordering::Equal => self.description.cmp(&other.description),
         Ordering::Less | Ordering::Greater => o,
      }
   }
}

/// Collection of [Entry] that constitutes the totality of entries created in the application
#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct Entries {
   pub values: HashSet<Entry>,
}

impl<K> From<&HashMap<K, Entry>> for Entries {
   fn from(map: &HashMap<K, Entry, RandomState>) -> Self {
      Self {
         values: map.values().cloned().collect(),
      }
   }
}

#[cfg(test)]
mod tests {
   mod secret {
      use std::convert::TryInto;

      use crate::entities::entry::{Secret, SecretError};

      #[test]
      fn fails_to_parse_with_empty_error_if_value_is_empty() {
         let actual: Result<Secret, SecretError> = "".try_into();
         assert_eq!(actual, Err(SecretError::Empty));

         let actual: Result<Secret, SecretError> = String::new().try_into();
         assert_eq!(actual, Err(SecretError::Empty));

         let actual: Result<Secret, SecretError> = Secret::new("");
         assert_eq!(actual, Err(SecretError::Empty));

         let actual: Result<Secret, SecretError> = Secret::new(String::new());
         assert_eq!(actual, Err(SecretError::Empty));
      }

      #[test]
      fn fails_to_parse_with_invalid_encoding_error_if_value_not_encoded_in_base32() {
         let actual: Result<Secret, SecretError> = "$%&/#$&/".try_into();
         assert_eq!(actual, Err(SecretError::InvalidBase32Encoded));

         let actual: Result<Secret, SecretError> = "$%&/#$&/".to_owned().try_into();
         assert_eq!(actual, Err(SecretError::InvalidBase32Encoded));

         let actual: Result<Secret, SecretError> = Secret::new("$%&/#$&/");
         assert_eq!(actual, Err(SecretError::InvalidBase32Encoded));

         let actual: Result<Secret, SecretError> = Secret::new("$%&/#$&/".to_owned());
         assert_eq!(actual, Err(SecretError::InvalidBase32Encoded));
      }

      #[test]
      fn succeeds_if_value_not_empty_and_properly_base32_encoded() {
         let encoded_secret = base32::encode(base32::Alphabet::RFC4648 { padding: false }, "hunter2".as_bytes());

         let actual: Result<String, SecretError> = encoded_secret.as_str().try_into().map(|s: Secret| s.into());
         assert!(matches!(actual, Ok(s) if s == encoded_secret));

         let actual: Result<String, SecretError> = encoded_secret.clone().try_into().map(|s: Secret| s.into());
         assert!(matches!(actual, Ok(s) if s == encoded_secret));

         let actual: Result<String, SecretError> = Secret::new(encoded_secret.as_str()).map(|s| s.into());
         assert!(matches!(actual, Ok(s) if s == encoded_secret));

         let actual: Result<String, SecretError> = Secret::new(encoded_secret.clone()).map(|s| s.into());
         assert!(matches!(actual, Ok(s) if s == encoded_secret));
      }
   }

   mod time_step {
      use std::{convert::TryInto, time::Duration};

      use crate::entities::entry::{TimeStep, TimeStepError};

      #[test]
      fn fails_to_parse_if_duration_is_less_than_1_second() {
         let test_duration = Duration::ZERO;

         let actual: Result<TimeStep, TimeStepError> = test_duration.try_into();
         assert_eq!(actual, Err(TimeStepError));

         let actual: Result<TimeStep, TimeStepError> = TimeStep::new(test_duration);
         assert_eq!(actual, Err(TimeStepError));

         let test_duration = Duration::from_millis(900);

         let actual: Result<TimeStep, TimeStepError> = test_duration.try_into();
         assert_eq!(actual, Err(TimeStepError));

         let actual: Result<TimeStep, TimeStepError> = TimeStep::new(test_duration);
         assert_eq!(actual, Err(TimeStepError));
      }

      #[test]
      fn succeeds_if_duration_is_greater_than_zero() {
         let test_duration = Duration::from_secs(30);

         let actual: Result<Duration, TimeStepError> = test_duration.try_into().map(|ts: TimeStep| ts.into());
         assert!(matches!(actual, Ok(td) if td == test_duration));

         let actual: Result<Duration, TimeStepError> = TimeStep::new(test_duration).map(|ts| ts.into());
         assert!(matches!(actual, Ok(td) if td == test_duration));
      }
   }
}
