/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! Contains the definition used to represent the complete data of the application to be persisted.
//! Password is hashed, we require a PHC string for Argon2, and for encryption a nonce.

use core::fmt::Debug;

use crate::entities::entry::Entries;

/// Argon2d parameters
pub const ARGON2_PHC: &str = "$argon2d$v=19$m=524288,t=3,p=3$";

/// Size of nonce to use for encryption
const NONCE_SIZE: usize = 12;

/// Encryption parameters used by the application to read and store the user information
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct EncryptionHeader {
   /// [String for the Password Hashing Competition (PHC).](https://github.com/P-H-C/phc-string-format/blob/master/phc-sf-spec.md#specification)
   pub phc_str: String,
   pub nonce: Vec<u8>,
}

impl EncryptionHeader {
   /// Initializes a new header with valid, and securely generated, values.
   pub fn new() -> Self {
      use argon2::password_hash::SaltString;
      use rand::RngCore;

      let mut rnd = rand::thread_rng();

      let phc_str = ARGON2_PHC.to_owned() + SaltString::generate(&mut rnd).as_str();

      let nonce = &mut [0u8; NONCE_SIZE];
      rnd.fill_bytes(nonce);

      Self {
         phc_str,
         nonce: nonce.to_vec(),
      }
   }
}

impl Default for EncryptionHeader {
   fn default() -> Self {
      Self::new()
   }
}

/// Encrypted [Entries] alongside with the parameters used to encrypt it: PHC string and nonce.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct EncryptedData {
   pub header: EncryptionHeader,
   pub payload: Vec<u8>,
}

impl EncryptedData {
   /// Creates a new instance with valid, secure, and randomly generated, PHC string and nonce.
   pub fn new() -> Self {
      Self {
         header: EncryptionHeader::new(),
         payload: vec![],
      }
   }
}

impl Default for EncryptedData {
   fn default() -> Self {
      Self::new()
   }
}

/// Generic error for errors generated during encryption.
#[derive(Clone, Debug, thiserror::Error, Eq, PartialEq)]
#[error("couldn't write to the storage: {0}")]
pub struct Error(pub String);

/// Knows how to encrypt [Entries] and output the result as a byte buffer.
/// This includes any encryption parameters required for deserialization, for example a nonce.
pub trait EncryptEntries: Send + Sync + Debug {
   /// Encrypts [Entries]
   ///
   /// # Errors
   /// Any kind of error while trying to write will return an [Error]
   fn encrypt(&self, entries: &Entries) -> Result<Vec<u8>, Error>;
}

#[cfg(test)]
mod tests {
   mod encryption_header {
      use password_hash::PasswordHash;

      use crate::entities::encrypted::EncryptionHeader;

      #[test]
      fn phc_str_is_valid() {
         let header = EncryptionHeader::new();

         let password_hash = PasswordHash::new(header.phc_str.as_str());

         assert!(password_hash.is_ok());

         let password_hash = password_hash.unwrap();

         assert!(password_hash.version.is_some());
         assert_eq!(password_hash.params.as_str(), "m=524288,t=3,p=3");
         assert!(password_hash.salt.is_some());
         assert!(password_hash.hash.is_none());
      }

      #[test]
      fn nonce_12_bytes_long() {
         let header = EncryptionHeader::new();

         assert_eq!(header.nonce.len(), 12);
      }
   }

   mod encrypted_data {
      use crate::entities::encrypted::EncryptedData;

      #[test]
      fn payload_is_empty_if_new() {
         let data = EncryptedData::new();

         assert!(data.payload.is_empty());
      }
   }
}
