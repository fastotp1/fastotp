# `fastotp_core`
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

Exposes the entire application as a library rendering it independent of a specific user interface implementation.

The architecture is based on, with some variations, "Clean Architecture" as described by Robert C. Martin. 

## Structure

### `domain`

Business logic of the application. Contains the different entities used to represent the data to manipulate entries of TOTPs, do CRUD operations over them, how to generate the TOTP codes, persist them, and unlocking the application.

The use cases are the operations carried out in order to fulfill the functionality of the application. They operate over the entities and maintain business invariants. 

### `persistence`

Contains serialization & deserialization code to encode the user data in binary format for fast and efficient data storage,
as well as the logic to do crud operations over the stored data. 

### `presentation`

Everything related to UX and application flow is contained here. This includes user facing strings, screen transitions, user data validation at UI level, etc.

### `stream_utils`

Collection of helper functions designed to provide some of the functionality found in [Rx](http://reactivex.io/).

## Build

```shell script
cargo build --release --package fastotp_core
```

## Acknowledgements

[github.com/TimDumol/rust-otp](https://github.com/TimDumol/rust-otp/blob/master/src/lib.rs): Initial version of TOTP generation was based on this implementation.

## License

[AGPL-3.0-or-later](LICENSE.AGPL)
