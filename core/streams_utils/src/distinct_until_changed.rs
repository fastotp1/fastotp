/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! See [DistinctUntilChangeExt]

use core::pin::Pin;

use futures::{
   stream::{Fuse, FusedStream, Stream},
   task::{Context, Poll},
   StreamExt,
};
use pin_project::pin_project;

pub trait ClonablePartialEq = Clone + PartialEq;

impl<S, I> DistinctUntilChangeExt for S
where
   I: ClonablePartialEq,
   S: Sized + Stream<Item = I>,
{
}

/// Emulates the distinct Rx operator: <http://reactivex.io/documentation/operators/distinct.html>
pub trait DistinctUntilChangeExt: Stream {
   fn distinct_until_changed<I>(self) -> DistinctUntilChanged<Self, I>
   where
      I: ClonablePartialEq,
      Self: Stream<Item = I> + Sized,
   {
      DistinctUntilChanged {
         stream: self.fuse(),
         last_emitted: None,
      }
   }
}

#[pin_project]
#[must_use = "streams do nothing unless polled"]
pub struct DistinctUntilChanged<S, I>
where
   I: ClonablePartialEq,
   S: Stream<Item = I>,
{
   #[pin]
   stream: Fuse<S>,
   last_emitted: Option<I>,
}

impl<S, I> FusedStream for DistinctUntilChanged<S, I>
where
   I: ClonablePartialEq,
   S: Stream<Item = I>,
{
   fn is_terminated(&self) -> bool {
      self.stream.is_terminated()
   }
}

impl<S, I> Stream for DistinctUntilChanged<S, I>
where
   I: ClonablePartialEq,
   S: Stream<Item = I>,
{
   type Item = I;

   fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
      let self_pinned = self.project();
      let poll = self_pinned.stream.poll_next(cx);

      let result = match poll {
         Poll::Pending => return Poll::Pending,
         Poll::Ready(None) => None,
         Poll::Ready(Some(item)) => {
            match *(self_pinned.last_emitted) {
               None => {
                  *self_pinned.last_emitted = Some(item.clone());
                  Some(item)
               }
               Some(ref last_distinct_item) => {
                  if *last_distinct_item == item {
                     return Poll::Pending;
                  }

                  *self_pinned.last_emitted = Some(item.clone());
                  Some(item)
               }
            }
         }
      };

      Poll::Ready(result)
   }
}

#[cfg(test)]
mod tests {
   use std::time::Duration;

   use async_std::task;
   use futures::{select_biased, FutureExt, StreamExt};
   use futures_signals::signal::{Mutable, SignalExt};
   use futures_timer::Delay;

   use crate::distinct_until_changed::DistinctUntilChangeExt;

   #[test]
   fn only_emits_new_distinct_values() {
      task::block_on(async {
         let mut item_to_emit = 0;

         let mutable = Mutable::new(item_to_emit);
         let mut distinct_stream = mutable.signal_cloned().to_stream().distinct_until_changed();

         assert_eq!(distinct_stream.next().await, Some(item_to_emit));

         mutable.set(item_to_emit);
         let mut delay = Delay::new(Duration::from_millis(30)).fuse();
         select_biased! {
             _ = delay => (),
             _ = distinct_stream.next() => panic!("No item should've been emitted"),
         };

         mutable.set(item_to_emit);
         delay = Delay::new(Duration::from_millis(30)).fuse();
         select_biased! {
             _ = delay => (),
             _ = distinct_stream.next() => panic!("No item should've been emitted"),
         };

         item_to_emit = 1;
         mutable.set(item_to_emit);
         assert_eq!(distinct_stream.next().await, Some(item_to_emit));

         item_to_emit = 2;
         mutable.set(item_to_emit);
         assert_eq!(distinct_stream.next().await, Some(item_to_emit));
      });
   }
}
