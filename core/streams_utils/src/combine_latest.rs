/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! See [combine_latest()]

use core::pin::Pin;

use futures::{
   stream::{Fuse, FusedStream, Stream},
   task::{Context, Poll},
   StreamExt,
};
use pin_project::pin_project;

/// Emulates the Rx operator with the same name: <http://reactivex.io/documentation/operators/combinelatest.html>
pub fn combine_latest<St1, T1, St2, T2>(stream1: St1, stream2: St2) -> CombineLatest<St1, T1, St2, T2>
where
   T1: Clone,
   St1: Stream<Item = T1>,
   T2: Clone,
   St2: Stream<Item = T2>,
{
   CombineLatest {
      stream1: stream1.fuse(),
      last_emitted1: None,
      stream2: stream2.fuse(),
      last_emitted2: None,
   }
}

#[pin_project]
#[must_use = "streams do nothing unless polled"]
pub struct CombineLatest<St1, T1, St2, T2>
where
   T1: Clone,
   St1: Stream<Item = T1>,
   T2: Clone,
   St2: Stream<Item = T2>,
{
   #[pin]
   stream1: Fuse<St1>,
   last_emitted1: Option<T1>,
   #[pin]
   stream2: Fuse<St2>,
   last_emitted2: Option<T2>,
}

impl<St1, T1, St2, T2> FusedStream for CombineLatest<St1, T1, St2, T2>
where
   T1: Clone,
   St1: Stream<Item = T1>,
   T2: Clone,
   St2: Stream<Item = T2>,
{
   fn is_terminated(&self) -> bool {
      self.stream1.is_terminated() || self.stream2.is_terminated()
   }
}

impl<St1, St2, T1, T2> Stream for CombineLatest<St1, T1, St2, T2>
where
   T1: Clone,
   St1: Stream<Item = T1>,
   T2: Clone,
   St2: Stream<Item = T2>,
{
   type Item = (T1, T2);

   fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
      let self_pinned = self.project();
      let poll1 = self_pinned.stream1.poll_next(cx);
      let poll2 = self_pinned.stream2.poll_next(cx);

      let result = match (poll1, poll2) {
         (Poll::Pending, Poll::Pending) => return Poll::Pending,
         (Poll::Ready(None), _) | (_, Poll::Ready(None)) => None,
         (Poll::Ready(Some(item1)), Poll::Ready(Some(item2))) => {
            *self_pinned.last_emitted1 = Some(item1.clone());
            *self_pinned.last_emitted2 = Some(item2.clone());

            Some((item1, item2))
         }
         (Poll::Ready(Some(item1)), Poll::Pending) => {
            *self_pinned.last_emitted1 = Some(item1.clone());

            match *(self_pinned.last_emitted2) {
               None => return Poll::Pending,
               Some(ref item2) => Some((item1, item2.clone())),
            }
         }
         (Poll::Pending, Poll::Ready(Some(item2))) => {
            *self_pinned.last_emitted2 = Some(item2.clone());

            match *(self_pinned.last_emitted1) {
               None => return Poll::Pending,
               Some(ref item1) => Some((item1.clone(), item2)),
            }
         }
      };

      Poll::Ready(result)
   }
}

#[cfg(test)]
mod tests {
   use futures::StreamExt;
   use futures_signals::signal::{Mutable, SignalExt};

   use crate::combine_latest::combine_latest;

   #[test]
   fn emits_last_emited_events_combined_whenever_any_stream_emits_an_event() {
      async_std::task::block_on(async {
         let first_event = Mutable::new(0);
         let first_stream = first_event.signal_cloned().to_stream();

         let second_event = Mutable::new(0);
         let second_stream = second_event.signal_cloned().to_stream();

         let mut combined_stream = combine_latest(first_stream, second_stream);

         assert_eq!(combined_stream.next().await, Some((0, 0)));

         first_event.set(1);
         assert_eq!(combined_stream.next().await, Some((1, 0)));

         first_event.set(2);
         assert_eq!(combined_stream.next().await, Some((2, 0)));

         first_event.set(3);
         assert_eq!(combined_stream.next().await, Some((3, 0)));

         second_event.set(1);
         assert_eq!(combined_stream.next().await, Some((3, 1)));

         second_event.set(2);
         assert_eq!(combined_stream.next().await, Some((3, 2)));
      });
   }
}
