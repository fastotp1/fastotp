/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! UX logic for the main screen of the application.\
//! Here the user will see the generated TOTP codes and navigate to the screen that allows doing CRUD
//! operations over the stored entries, as well as managing different aspects of the application such
//! as changing password.

use std::{
   hash::{Hash, Hasher},
   ops::RangeInclusive,
   time::Instant,
};

use clipboard::{ClipboardContext, ClipboardProvider};
use domain::{
   entities::entry::Id,
   use_cases::otp_generation::{OtpCode, OtpGeneration, Result as OtpGenerationResult},
};
use futures::{future::BoxFuture, stream::BoxStream, StreamExt};
use futures_signals::signal::{Mutable, SignalExt};

use crate::{Navigator, Screen};

#[derive(Clone, PartialEq, Eq, Debug)]
pub enum Action {
   FilterBy(String),
   DisplayResults(OtpGenerationResult),
   CleanFilter,
   TryCopyOtpAt { position: usize, requires_single_otp: bool },
   EditEntryAt { position: usize, requires_single_otp: bool },
   EditEntry(Id),
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Command {
   pub action: Option<Action>,
   pub go_to: Option<Screen>,
}

impl Navigator for Command {
   fn is_navigation(&self) -> Option<Screen> {
      self.go_to
   }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct OtpData {
   pub issuer: String,
   pub description: String,
   pub otp: String,
   pub entry_id: Id,
   otp_code: Option<OtpCode>,
}

/// Identifier for an TOTP entry to be displayed
#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq)]
pub struct OtpDataId(u64);

impl OtpData {
   pub fn id(&self) -> OtpDataId {
      use std::collections::hash_map::DefaultHasher;

      let mut s = DefaultHasher::new();
      self.issuer.hash(&mut s);
      self.description.hash(&mut s);

      OtpDataId(s.finish())
   }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Labels {
   pub search: String,
   pub no_results: String,
   pub edit: String,
   pub system_time_backwards: String,
   pub informative_messages: String,
}

#[derive(Clone, Debug, PartialEq)]
pub enum OtpsViewModel {
   NoResults,
   SystemTimeBefore1970,
   Results {
      time_range: RangeInclusive<f32>,
      time_left: f32,
      time_left_str: String,
      otps: Vec<OtpData>,
      last_copied_otp: Option<(Instant, OtpData)>,
   },
}

#[derive(Clone, PartialEq, Debug)]
pub struct ViewModel {
   pub labels: Labels,
   pub filter: String,
   pub otps: OtpsViewModel,
}

pub struct OtpDisplay {
   use_case: OtpGeneration,
   filter_query: Mutable<String>,
   view_model: ViewModel,
}

impl OtpDisplay {
   pub fn new(use_case: OtpGeneration) -> Self {
      let labels = Labels {
         search: "Search... or press Ctrl+N to create a new entry".to_owned(),
         no_results: "No Entries Found".to_owned(),
         edit: "Edit".to_owned(),
         system_time_backwards: "Can't generate codes.\n System time is set before 1970-01-01".to_owned(),
         informative_messages: "Press Enter to copy TOTP\nPress Ctrl+Enter to edit".to_owned(),
      };

      Self {
         use_case,
         filter_query: Mutable::new(String::new()),
         view_model: ViewModel {
            labels,
            filter: String::new(),
            otps: OtpsViewModel::NoResults,
         },
      }
   }

   #[inline]
   pub fn view_model(&self) -> ViewModel {
      self.view_model.clone()
   }

   pub fn update_events<'future>(&self) -> BoxStream<'future, Command> {
      let filter_signal = self.filter_query.signal_cloned().to_stream().boxed();
      let generated_otps_stream = self.use_case.generate_otps(filter_signal);

      generated_otps_stream
         .map(|otps| {
            Command {
               action: Some(Action::DisplayResults(otps)),
               go_to: None,
            }
         })
         .boxed()
   }

   pub fn interpret(&mut self, cmd: Command) -> Option<BoxFuture<'static, Command>> {
      let Command { action, .. } = cmd;

      match action? {
         Action::FilterBy(filter_query) => {
            self.view_model.filter = filter_query.clone();
            self.filter_query.set(filter_query);
            None
         }
         Action::CleanFilter => {
            self.view_model.filter = String::new();
            self.filter_query.set(String::new());
            None
         }
         Action::DisplayResults(generated_otps_res) => {
            match generated_otps_res {
               Ok(generated_otps) => {
                  if generated_otps.otps.is_empty() {
                     self.view_model.otps = OtpsViewModel::NoResults;
                  }
                  else {
                     let time_left = generated_otps.expires_in.as_millis() as f32;
                     let max_time_left = generated_otps.max_time_step.as_millis() as f32;

                     let was_selected: Option<(Instant, OtpData)> = if let OtpsViewModel::Results {
                        ref last_copied_otp,
                        ..
                     } = self.view_model.otps
                     {
                        last_copied_otp.clone()
                     }
                     else {
                        None
                     };

                     self.view_model.otps = OtpsViewModel::Results {
                        time_range: 0.0..=max_time_left,
                        time_left,
                        time_left_str: format!("{}s", generated_otps.expires_in.as_secs()),
                        otps: generated_otps
                           .otps
                           .into_iter()
                           .map(|otp| {
                              OtpData {
                                 issuer: otp.issuer,
                                 description: if otp.description.is_empty() {
                                    "-".to_owned()
                                 }
                                 else {
                                    otp.description
                                 },
                                 otp: otp.otp.as_ref().map_or_else(
                                    |_| "Error calculating code".to_owned(),
                                    |code| {
                                       format!("{code:0width$}", width = 6)
                                          .chars()
                                          .enumerate()
                                          .flat_map(|(i, c)| {
                                             // Characters in groups of 3
                                             (i != 0 && i % 3 == 0)
                                                .then_some(' ')
                                                .into_iter()
                                                .chain(std::iter::once(c))
                                          })
                                          .collect::<String>()
                                    },
                                 ),
                                 otp_code: otp.otp.ok(),
                                 entry_id: otp.entry_id,
                              }
                           })
                           .collect(),
                        last_copied_otp: was_selected,
                     };
                  }
               }
               Err(_) => self.view_model.otps = OtpsViewModel::SystemTimeBefore1970,
            }

            None
         }
         Action::TryCopyOtpAt {
            position,
            requires_single_otp,
         } => {
            if let OtpsViewModel::Results {
               ref otps,
               ref mut last_copied_otp,
               ..
            } = self.view_model.otps
            {
               if requires_single_otp && otps.len() > 1 {
                  return None;
               }

               if let Some(otp) = otps.get(position) {
                  if let Some(raw_otp) = otp.otp_code {
                     let mut ctx: ClipboardContext =
                        ClipboardProvider::new().expect("Failed to get clipboard provider");

                     // TODO: Add error management
                     let _ = ctx.set_contents(format!("{raw_otp:0width$}", width = 6));

                     *last_copied_otp = Some((Instant::now(), otp.clone()));
                  }
               }
            }

            None
         }
         Action::EditEntryAt {
            position,
            requires_single_otp,
         } => {
            if let OtpsViewModel::Results { ref otps, .. } = self.view_model.otps {
               if requires_single_otp && otps.len() > 1 {
                  return None;
               }

               if let Some(otp) = otps.get(position) {
                  use futures::FutureExt;

                  let entry_id = otp.entry_id;
                  let f = async move { entry_id }.map(|id| {
                     Command {
                        action: Some(Action::EditEntry(id)),
                        go_to: Some(Screen::CreateEntry),
                     }
                  });

                  return Some(Box::pin(f));
               }
            }

            None
         }
         Action::EditEntry(_) => None,
      }
   }
}

#[cfg(test)]
mod tests {
   use std::{sync::Arc, time::Duration};

   use async_lock::RwLock as AsyncRwLock;
   use domain::{
      entities::entry::Id,
      repository::InMemory,
      use_cases::otp_generation::{Error, GeneratedOtp, GeneratedOtps, OtpGeneration, DEFAULT_TIME_STEP},
   };

   use crate::otp_display::{Action, Command, Labels, OtpData, OtpDisplay, OtpsViewModel, ViewModel};

   fn default_labels() -> Labels {
      Labels {
         search: "Search... or press Ctrl+N to create a new entry".to_owned(),
         no_results: "No Entries Found".to_owned(),
         edit: "Edit".to_owned(),
         system_time_backwards: "Can't generate codes.\n System time is set before 1970-01-01".to_owned(),
         informative_messages: "Press Enter to copy TOTP\nPress Ctrl+Enter to edit".to_owned(),
      }
   }

   #[test]
   fn intial_state_is_empty_filter_and_no_results() {
      let repository = Arc::new(AsyncRwLock::new(InMemory::new()));
      let presenter = OtpDisplay::new(OtpGeneration::new(repository));

      let expected_view_model = ViewModel {
         labels: default_labels(),
         filter: String::new(),
         otps: OtpsViewModel::NoResults,
      };

      assert_eq!(presenter.view_model(), expected_view_model);
   }

   #[test]
   fn cleaning_the_filter_works() {
      let repository = Arc::new(AsyncRwLock::new(InMemory::new()));
      let mut presenter = OtpDisplay::new(OtpGeneration::new(repository));

      let actual = presenter.interpret(Command {
         action: Some(Action::FilterBy("abcdefg".to_owned())),
         go_to: None,
      });

      let expected_view_model = ViewModel {
         labels: default_labels(),
         filter: "abcdefg".to_owned(),
         otps: OtpsViewModel::NoResults,
      };
      assert!(actual.is_none());
      assert_eq!(presenter.view_model(), expected_view_model);

      let actual = presenter.interpret(Command {
         action: Some(Action::CleanFilter),
         go_to: None,
      });

      let expected_view_model = ViewModel {
         labels: default_labels(),
         filter: String::new(),
         otps: OtpsViewModel::NoResults,
      };

      assert!(actual.is_none());
      assert_eq!(presenter.view_model(), expected_view_model);
   }

   #[test]
   fn no_otps_generated_then_no_deferred_command_is_created_and_view_model_is_no_results() {
      let repository = Arc::new(AsyncRwLock::new(InMemory::new()));
      let mut presenter = OtpDisplay::new(OtpGeneration::new(repository));

      let cmd = Command {
         action: Some(Action::DisplayResults(Ok(GeneratedOtps {
            max_time_step: Default::default(),
            expires_in: Default::default(),
            otps: vec![],
         }))),
         go_to: None,
      };

      let actual = presenter.interpret(cmd);

      let expected_view_model = ViewModel {
         labels: default_labels(),
         filter: String::new(),
         otps: OtpsViewModel::NoResults,
      };

      assert!(actual.is_none());
      assert_eq!(presenter.view_model(), expected_view_model);
   }

   #[test]
   fn otps_generated_then_no_deferred_command_is_created_and_otps_are_mapped_to_view_model() {
      let repository = Arc::new(AsyncRwLock::new(InMemory::new()));
      let mut presenter = OtpDisplay::new(OtpGeneration::new(repository));

      const EXPIRES_IN: Duration = Duration::from_secs(5);
      let entries_ids = [Id::generate(), Id::generate(), Id::generate(), Id::generate()];
      let cmd = Command {
         action: Some(Action::DisplayResults(Ok(GeneratedOtps {
            max_time_step: Duration::from_secs(DEFAULT_TIME_STEP),
            expires_in: EXPIRES_IN,
            otps: vec![
               GeneratedOtp {
                  entry_id: entries_ids[0].clone(),
                  issuer: "Issuer A".to_owned(),
                  description: "Description A".to_owned(),
                  otp: Ok(123456),
               },
               GeneratedOtp {
                  entry_id: entries_ids[1].clone(),
                  issuer: "Issuer B".to_owned(),
                  description: String::new(),
                  otp: Ok(3456),
               },
               GeneratedOtp {
                  entry_id: entries_ids[2].clone(),
                  issuer: "Issuer C".to_owned(),
                  description: String::new(),
                  otp: Err(Error::CalculatingOtp("calculation error".to_owned())),
               },
               GeneratedOtp {
                  entry_id: entries_ids[3].clone(),
                  issuer: "Issuer D".to_owned(),
                  description: String::new(),
                  otp: Err(Error::CurrentTimeBeforeUnixEpoch),
               },
            ],
         }))),
         go_to: None,
      };

      let actual = presenter.interpret(cmd);

      let expected_view_model = ViewModel {
         labels: default_labels(),
         filter: String::new(),
         otps: OtpsViewModel::Results {
            time_range: 0.0..=Duration::from_secs(DEFAULT_TIME_STEP).as_millis() as f32,
            time_left: EXPIRES_IN.as_millis() as f32,
            time_left_str: "5s".to_owned(),
            otps: vec![
               OtpData {
                  issuer: "Issuer A".to_owned(),
                  description: "Description A".to_owned(),
                  otp: "123 456".to_owned(),
                  otp_code: Some(123456),
                  entry_id: entries_ids[0].clone(),
               },
               OtpData {
                  issuer: "Issuer B".to_owned(),
                  description: "-".to_owned(),
                  otp: "003 456".to_owned(),
                  otp_code: Some(3456),
                  entry_id: entries_ids[1].clone(),
               },
               OtpData {
                  issuer: "Issuer C".to_owned(),
                  description: "-".to_owned(),
                  otp: "Error calculating code".to_owned(),
                  otp_code: None,
                  entry_id: entries_ids[2].clone(),
               },
               OtpData {
                  issuer: "Issuer D".to_owned(),
                  description: "-".to_owned(),
                  otp: "Error calculating code".to_owned(),
                  otp_code: None,
                  entry_id: entries_ids[3].clone(),
               },
            ],
            last_copied_otp: None,
         },
      };

      assert!(actual.is_none());
      assert_eq!(presenter.view_model(), expected_view_model);
   }
}
