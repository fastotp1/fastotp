/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! UX logic for changing the password of the application. Verifies that the passwords entered match.\
//! There are no requirements to how secure the password should be, is up to the user.

use domain::use_cases::change_password::{ChangePassword as UseCase, Result as UseCaseResult};
use futures::{future::BoxFuture, FutureExt};

use crate::{Navigator, Screen};

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Command {
   SetPassword(String),
   SetPasswordRepetition(String),
   Cancel,
   ChangePassword,
   PasswordChanged(UseCaseResult),
   GoTo(Screen),
}

impl Navigator for Command {
   fn is_navigation(&self) -> Option<Screen> {
      if let Self::GoTo(screen) = *self {
         return Some(screen);
      }

      None
   }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum State {
   ValidInputs,
   InvalidInputs,
   ChangingPassword,
   PasswordChangeFailed(String),
}

impl State {
   #[inline]
   pub fn isnt_changing_password(&self) -> bool {
      *self != Self::ChangingPassword
   }

   #[inline]
   pub const fn can_unlock(&self) -> bool {
      matches!(*self, Self::ValidInputs | Self::PasswordChangeFailed(_))
   }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Labels {
   pub title: String,
   pub password: String,
   pub password_repetition: String,
   pub cancel: String,
   pub action: String,
   pub error_message: String,
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct ViewModel {
   pub labels: Labels,
   pub password: String,
   pub password_repetition: String,
   pub state: State,
}

pub struct ChangePassword {
   use_case: UseCase,
   view_model: ViewModel,
}

impl ChangePassword {
   pub fn new(use_case: UseCase) -> Self {
      let view_model = Self::default_view_model();

      Self { use_case, view_model }
   }

   fn default_view_model() -> ViewModel {
      ViewModel {
         labels: Labels {
            title: "Password Change".to_owned(),
            password: "New Password".to_owned(),
            password_repetition: "Repeat New Password".to_owned(),
            cancel: "Cancel".to_owned(),
            action: "Change Password".to_owned(),
            error_message: String::new(),
         },
         password: String::new(),
         password_repetition: String::new(),
         state: State::ValidInputs,
      }
   }

   #[inline]
   pub fn view_model(&self) -> ViewModel {
      self.view_model.clone()
   }

   #[inline]
   fn update_state(&mut self) {
      self.view_model.state = if self.view_model.password == self.view_model.password_repetition {
         State::ValidInputs
      }
      else {
         State::InvalidInputs
      }
   }

   fn reset(&mut self) {
      self.view_model = Self::default_view_model();
   }

   pub fn interpret(&mut self, cmd: Command) -> Option<BoxFuture<'static, Command>> {
      match cmd {
         Command::SetPassword(password) => {
            if self.view_model.state.isnt_changing_password() {
               self.view_model.password = password;
               self.update_state();
            }
            None
         }
         Command::SetPasswordRepetition(password_repetition) => {
            if self.view_model.state.isnt_changing_password() {
               self.view_model.password_repetition = password_repetition;
               self.update_state();
            }
            None
         }
         Command::Cancel => {
            self.view_model.state.isnt_changing_password().then(|| {
               self.reset();

               let out_cmd: BoxFuture<'static, Command> = Box::pin(async { Command::GoTo(Screen::OtpDisplay) });

               out_cmd
            })
         }
         Command::ChangePassword => {
            self.view_model.state.can_unlock().then(|| {
               self.view_model.state = State::ChangingPassword;

               let result = self
                  .use_case
                  .change_password(self.view_model.password.clone())
                  .map(Command::PasswordChanged);

               let out_cmd: BoxFuture<'static, Command> = Box::pin(result);

               out_cmd
            })
         }
         Command::PasswordChanged(result) => {
            if result.is_ok() {
               self.reset();

               Some(Box::pin(async { Command::GoTo(Screen::OtpDisplay) }))
            }
            else {
               self.view_model.state = State::PasswordChangeFailed("Password change failed!".to_owned());

               None
            }
         }
         Command::GoTo(_) => None,
      }
   }
}

#[cfg(test)]
mod tests {
   use std::sync::Arc;

   use async_lock::RwLock as AsyncRwLock;
   use domain::{
      entities::{encrypted::EncryptedData, entry::Entries, Deserializer, Serializer},
      repository::{
         storage::{Error as StorageError, StubEncryptedStorage},
         InMemory,
      },
      scaffold_deserialize,
      use_cases::{change_password, change_password::Error},
   };

   use crate::{
      change_password::{ChangePassword, Command, Labels, State, ViewModel},
      Screen,
   };

   #[derive(Debug, thiserror::Error)]
   #[error("Error")]
   struct TestError;

   scaffold_deserialize!(EncDataDummySerde, EncryptedData, _buf, { Ok(EncryptedData::new()) });
   scaffold_deserialize!(EntriesDummySerde, Entries, _buf, { Ok(Entries::default()) });

   #[test]
   fn initial_state_is_no_error_message_and_empty_password_fields() {
      let presenter = ChangePassword::new(change_password::ChangePassword::new(
         Arc::new(AsyncRwLock::new(InMemory::new())),
         Arc::new(AsyncRwLock::new(StubEncryptedStorage)),
      ));

      let expected_view_model = ViewModel {
         labels: Labels {
            title: "Password Change".to_owned(),
            password: "New Password".to_owned(),
            password_repetition: "Repeat New Password".to_owned(),
            cancel: "Cancel".to_owned(),
            action: "Change Password".to_owned(),
            error_message: String::new(),
         },
         password: String::new(),
         password_repetition: String::new(),
         state: State::ValidInputs,
      };

      assert_eq!(presenter.view_model(), expected_view_model);
   }

   #[test]
   fn state_is_valid_inputs_only_if_both_passwords_match() {
      let mut presenter = ChangePassword::new(change_password::ChangePassword::new(
         Arc::new(AsyncRwLock::new(InMemory::new())),
         Arc::new(AsyncRwLock::new(StubEncryptedStorage)),
      ));

      let expected_view_model = ViewModel {
         labels: Labels {
            title: "Password Change".to_owned(),
            password: "New Password".to_owned(),
            password_repetition: "Repeat New Password".to_owned(),
            cancel: "Cancel".to_owned(),
            action: "Change Password".to_owned(),
            error_message: String::new(),
         },
         password: String::new(),
         password_repetition: String::new(),
         state: State::ValidInputs,
      };

      // Both empty is a valid password
      assert_eq!(presenter.view_model(), expected_view_model);

      presenter.interpret(Command::SetPassword("a".to_owned()));
      let expected_view_model = ViewModel {
         password: "a".to_owned(),
         state: State::InvalidInputs,
         ..expected_view_model
      };
      assert_eq!(presenter.view_model(), expected_view_model);

      presenter.interpret(Command::SetPasswordRepetition("a".to_owned()));
      let expected_view_model = ViewModel {
         password_repetition: "a".to_owned(),
         state: State::ValidInputs,
         ..expected_view_model
      };
      assert_eq!(presenter.view_model(), expected_view_model);

      presenter.interpret(Command::SetPasswordRepetition("ab".to_owned()));
      let expected_view_model = ViewModel {
         password_repetition: "ab".to_owned(),
         state: State::InvalidInputs,
         ..expected_view_model
      };
      assert_eq!(presenter.view_model(), expected_view_model);

      presenter.interpret(Command::SetPassword("ab".to_owned()));
      let expected_view_model = ViewModel {
         password: "ab".to_owned(),
         state: State::ValidInputs,
         ..expected_view_model
      };
      assert_eq!(presenter.view_model(), expected_view_model);
   }

   #[test]
   fn changes_password_only_if_has_valid_inputs_or_a_password_change_attempt_failed() {
      let mut presenter = ChangePassword::new(change_password::ChangePassword::new(
         Arc::new(AsyncRwLock::new(InMemory::new())),
         Arc::new(AsyncRwLock::new(StubEncryptedStorage)),
      ));

      presenter.interpret(Command::SetPassword("a".to_owned()));

      let cmd = presenter.interpret(Command::ChangePassword);

      assert!(cmd.is_none());

      // Reset to hve valid inputs again
      presenter.interpret(Command::SetPassword(String::new()));

      let cmd = presenter.interpret(Command::PasswordChanged(Err(Error::Storage(StorageError(
         "storage error".to_owned(),
      )))));

      assert!(cmd.is_none());

      let expected_view_model = ViewModel {
         labels: Labels {
            title: "Password Change".to_owned(),
            password: "New Password".to_owned(),
            password_repetition: "Repeat New Password".to_owned(),
            cancel: "Cancel".to_owned(),
            action: "Change Password".to_owned(),
            error_message: String::new(),
         },
         password: String::new(),
         password_repetition: String::new(),
         state: State::PasswordChangeFailed("Password change failed!".to_owned()),
      };

      assert_eq!(presenter.view_model(), expected_view_model);

      let cmd = presenter.interpret(Command::ChangePassword);

      assert!(cmd.is_some());
   }

   #[test]
   fn cancelling_resets_the_view_state_and_goes_to_otp_display() {
      let mut presenter = ChangePassword::new(change_password::ChangePassword::new(
         Arc::new(AsyncRwLock::new(InMemory::new())),
         Arc::new(AsyncRwLock::new(StubEncryptedStorage)),
      ));

      presenter.interpret(Command::SetPassword("not empty".to_owned()));
      presenter.interpret(Command::SetPasswordRepetition("not empty".to_owned()));

      let cmd = presenter.interpret(Command::Cancel);

      assert!(cmd.is_some());

      async_std::task::block_on(async move {
         let command: Command = cmd.unwrap().await;
         assert_eq!(command, Command::GoTo(Screen::OtpDisplay))
      });

      let expected_view_model = ViewModel {
         labels: Labels {
            title: "Password Change".to_owned(),
            password: "New Password".to_owned(),
            password_repetition: "Repeat New Password".to_owned(),
            cancel: "Cancel".to_owned(),
            action: "Change Password".to_owned(),
            error_message: String::new(),
         },
         password: String::new(),
         password_repetition: String::new(),
         state: State::ValidInputs,
      };

      assert_eq!(presenter.view_model(), expected_view_model);
   }

   #[test]
   fn creating_resets_the_view_state_and_goes_to_otp_display() {
      let mut presenter = ChangePassword::new(change_password::ChangePassword::new(
         Arc::new(AsyncRwLock::new(InMemory::new())),
         Arc::new(AsyncRwLock::new(StubEncryptedStorage)),
      ));

      presenter.interpret(Command::SetPassword("not empty".to_owned()));
      presenter.interpret(Command::SetPasswordRepetition("not empty".to_owned()));

      let cmd = presenter.interpret(Command::PasswordChanged(Ok(())));

      assert!(cmd.is_some());

      async_std::task::block_on(async move {
         let command: Command = cmd.unwrap().await;
         assert_eq!(command, Command::GoTo(Screen::OtpDisplay))
      });

      let expected_view_model = ViewModel {
         labels: Labels {
            title: "Password Change".to_owned(),
            password: "New Password".to_owned(),
            password_repetition: "Repeat New Password".to_owned(),
            cancel: "Cancel".to_owned(),
            action: "Change Password".to_owned(),
            error_message: String::new(),
         },
         password: String::new(),
         password_repetition: String::new(),
         state: State::ValidInputs,
      };

      assert_eq!(presenter.view_model(), expected_view_model);
   }
}
