/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! Contains the UX logic to represent the flow between functionalities, user data input
//! validation, data in the format displayed to the user, and error management.

#![allow(
   clippy::doc_markdown,
   clippy::items_after_statements,
   clippy::missing_fields_in_debug,
   clippy::missing_panics_doc,
   clippy::must_use_candidate,
   clippy::suspicious_else_formatting
)]
#![deny(
   array_into_iter,
   clippy::rest_pat_in_fully_bound_structs,
   clippy::separated_literal_suffix,
   non_camel_case_types,
   private_in_public,
   rust_2018_idioms,
   trivial_casts,
   trivial_numeric_casts,
   type_alias_bounds,
   unknown_lints,
   unsafe_code,
   unused,
   unused_import_braces,
   while_true
)]

pub mod change_password;
pub mod create_entry;
pub mod otp_display;
pub mod unlocking;

/// Screens of the application which usually correspond to an use case
#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub enum Screen {
   Unlocking,
   CreateEntry,
   OtpDisplay,
   ChangePassword,
}

/// If implemented designates `Self` as a navigation instruction
pub trait Navigator: Sized {
   fn is_navigation(&self) -> Option<Screen>;
}
