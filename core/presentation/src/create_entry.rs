/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! UX logic for creating, editing or deleting an entry from the application.

use std::{convert::TryInto, time::Duration};

use domain::{
   entities::entry::{Entry, Id, SecretError},
   use_cases::{
      create_entry::{EntryCreation as EntryCreationUseCase, EntryFindResult, Error as EntryCreationError},
      delete_entry::{EntryDeletion, Result as DeleteEntryResult},
      otp_generation::DEFAULT_TIME_STEP,
      qr_generation::QrGeneration,
   },
};
use futures::{future::BoxFuture, FutureExt};

use crate::{Navigator, Screen};

/// Occurs when trying to create an [Entry]
#[derive(Clone, Debug, thiserror::Error, Eq, PartialEq)]
pub enum Error {
   /// When creating or updating an [Entry] and the [Secret](crate::domain::entities::entry::Secret) is invalid
   #[error("invalid secret: {0}")]
   InvalidSecret(SecretError),

   /// When creating or updating an [Entry] and the operation fails
   #[error("entry save failed: {0}")]
   CreateUpdateFailure(EntryCreationError),
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub enum Command {
   SetIssuer(String),
   SetDescription(String),
   SetSecret(String),
   CreateOrUpdate,
   CreatedOrUpdated(Result<Entry, Error>),
   Cancel,
   Load(Id),
   Edit(EntryFindResult),
   AwaitDeleteConfirmation,
   Delete,
   Deleted(DeleteEntryResult),
   GoTo(Screen),
}

impl Navigator for Command {
   fn is_navigation(&self) -> Option<Screen> {
      if let Self::GoTo(screen) = *self {
         return Some(screen);
      }

      None
   }
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub enum State {
   Processing,
   ProcessingFailed(String),
   InvalidInputs,
   ValidInputs,
   AwaitingDeleteConfirmation,
}

impl State {
   #[inline]
   pub const fn can_create(&self) -> bool {
      matches!(*self, Self::ValidInputs | Self::ProcessingFailed(_))
   }

   #[inline]
   pub fn can_delete(&self) -> bool {
      *self == Self::AwaitingDeleteConfirmation
   }

   #[inline]
   pub fn can_cancel(&self) -> bool {
      *self == Self::AwaitingDeleteConfirmation || self.isnt_processing()
   }

   #[inline]
   pub fn isnt_processing(&self) -> bool {
      *self != Self::Processing
   }
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct DeleteLabels {
   pub delete: String,
   pub confirm_delete: String,
   pub ask_delete: String,
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Labels {
   pub title: String,
   pub issuer: String,
   pub description: String,
   pub secret: String,
   pub cancel: String,
   pub create_edit: String,
   pub delete: Option<DeleteLabels>,
   pub shortcuts: String,
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct ViewModel {
   pub labels: Labels,
   pub issuer: String,
   pub description: String,
   pub secret: String,
   pub qr_code: Vec<u8>,
   pub state: State,
}

impl ViewModel {
   pub fn is_new(&self) -> bool {
      self.issuer.is_empty()
         && self.description.is_empty()
         && self.secret.is_empty()
         && self.state == State::InvalidInputs
   }
}

pub struct EntryCreation {
   create_entry_use_case: EntryCreationUseCase,
   delete_entry_use_case: EntryDeletion,
   qr_generation_use_case: QrGeneration,
   view_model: ViewModel,
   pre_edit_entry_id: Option<Id>,
}

impl EntryCreation {
   pub fn new(
      entry_creation: EntryCreationUseCase,
      entry_deletion: EntryDeletion,
      qr_generation: QrGeneration,
   ) -> Self {
      Self {
         create_entry_use_case: entry_creation,
         delete_entry_use_case: entry_deletion,
         qr_generation_use_case: qr_generation,
         view_model: Self::default_view_model(),
         pre_edit_entry_id: None,
      }
   }

   fn default_view_model() -> ViewModel {
      let labels = Labels {
         title: "New Entry".to_owned(),
         issuer: "Issuer: Google, AWS, LastPass, etc.".to_owned(),
         description: "Description: Personal email, work account, secondary account".to_owned(),
         secret: "Secret: Provided by the issuer".to_owned(),
         cancel: "Cancel".to_owned(),
         create_edit: "Create".to_owned(),
         delete: None,
         shortcuts: "Press Esc to go back".to_owned(),
      };

      ViewModel {
         labels,
         issuer: String::new(),
         description: String::new(),
         secret: String::new(),
         qr_code: vec![],
         state: State::InvalidInputs,
      }
   }

   #[inline]
   fn update_state(&mut self) {
      let (state, shortcuts) = if !self.view_model.issuer.is_empty() && !self.view_model.secret.is_empty() {
         (State::ValidInputs, "Press Enter to save changes\nPress Esc to go back")
      }
      else {
         (State::InvalidInputs, "Press Esc to go back")
      };

      self.view_model.state = state;
      self.view_model.labels.shortcuts = if self.is_edit() {
         "Press Ctrl+D to delete\n".to_owned() + shortcuts
      }
      else {
         shortcuts.to_owned()
      }
   }

   #[inline]
   const fn is_edit(&self) -> bool {
      self.pre_edit_entry_id.is_some()
   }

   fn reset(&mut self) {
      self.view_model = Self::default_view_model();
      self.pre_edit_entry_id = None;
   }

   #[inline]
   pub fn view_model(&self) -> ViewModel {
      self.view_model.clone()
   }

   pub fn interpret(&mut self, cmd: Command) -> Option<BoxFuture<'static, Command>> {
      match cmd {
         Command::SetIssuer(new_issuer) => {
            if self.view_model.state.isnt_processing() {
               self.view_model.issuer = new_issuer;
               self.update_state();
            }
            None
         }
         Command::SetDescription(new_description) => {
            if self.view_model.state.isnt_processing() {
               self.view_model.description = new_description;
               self.update_state();
            }
            None
         }
         Command::SetSecret(new_secret) => {
            if self.view_model.state.isnt_processing() {
               self.view_model.secret = new_secret;
               self.update_state();
            }
            None
         }
         Command::CreateOrUpdate => {
            self.view_model.state.can_create().then(|| {
               self.view_model.state = State::Processing;

               let secret = self.view_model.secret.as_str().try_into();
               if let Err(e) = secret {
                  let out_cmd: BoxFuture<'static, Command> =
                     Box::pin(async { Command::CreatedOrUpdated(Err(Error::InvalidSecret(e))) });

                  return out_cmd;
               }

               let result = self
                  .create_entry_use_case
                  .create_entry(Entry {
                     id: self.pre_edit_entry_id.unwrap_or_else(Id::generate),
                     issuer: self.view_model.issuer.clone(),
                     description: self.view_model.description.clone(),
                     secret: secret.unwrap(),
                     time_step: Duration::from_secs(DEFAULT_TIME_STEP).try_into().unwrap(),
                  })
                  .map(|r| r.map_err(Error::CreateUpdateFailure))
                  .map(Command::CreatedOrUpdated);

               let out_cmd: BoxFuture<'static, Command> = Box::pin(result);

               out_cmd
            })
         }
         Command::CreatedOrUpdated(result) => {
            match result {
               Ok(_) => {
                  self.reset();

                  Some(Box::pin(async { Command::GoTo(Screen::OtpDisplay) }))
               }
               Err(e) => {
                  let msg = match e {
                     Error::InvalidSecret(_) => "Secret has invalid format",
                     Error::CreateUpdateFailure(_) => "Something happened, entry creation wasn't successful!",
                  };

                  self.view_model.state = State::ProcessingFailed(msg.to_owned());

                  None
               }
            }
         }
         Command::Cancel => {
            if self.view_model.state.can_cancel() {
               if self.view_model.state == State::AwaitingDeleteConfirmation {
                  self.update_state();
                  None
               }
               else {
                  self.reset();
                  Some(Box::pin(async { Command::GoTo(Screen::OtpDisplay) }))
               }
            }
            else {
               None
            }
         }
         Command::Load(entry_id) => {
            self.view_model.state.isnt_processing().then(|| {
               self.view_model.state = State::Processing;

               let result = self.create_entry_use_case.get_entry(&entry_id).map(Command::Edit);

               let out_cmd: BoxFuture<'static, Command> = Box::pin(result);

               out_cmd
            })
         }
         Command::Edit(result) => {
            match result {
               Ok(load_result) => {
                  match load_result {
                     Some(entry) => {
                        self.view_model.qr_code =
                           self.qr_generation_use_case.generate(&entry).unwrap_or_else(|_| vec![]);

                        self.view_model.issuer = entry.issuer.clone();
                        self.view_model.description = entry.description.clone();
                        self.view_model.secret = entry.secret.into();
                        self.pre_edit_entry_id = Some(entry.id);
                        self.update_state();

                        self.view_model.labels.title = "Edit Entry".to_owned();
                        self.view_model.labels.create_edit = "Edit".to_owned();
                        self.view_model.labels.delete = Some(DeleteLabels {
                           delete: "Delete".to_owned(),
                           confirm_delete: "Delete entry?".to_owned(),
                           ask_delete: "Yes, delete it".to_owned(),
                        });
                     }
                     None => {
                        self.view_model.state = State::ProcessingFailed("Failed to find the entry!".to_owned());
                     }
                  }
               }
               Err(_) => {
                  self.view_model.state = State::ProcessingFailed("Failed to find the entry!".to_owned());
               }
            }

            None
         }
         Command::AwaitDeleteConfirmation => {
            if self.is_edit() && self.view_model.state.isnt_processing() {
               self.view_model.state = State::AwaitingDeleteConfirmation;
               self.view_model.labels.shortcuts =
                  "Press Ctrl+Enter to confirm deletion\nPress Esc to cancel".to_owned();
            }

            None
         }
         Command::Delete => {
            self.view_model.state.can_delete().then(|| {
               self.view_model.state = State::Processing;

               let result = self
                  .delete_entry_use_case
                  .delete_entry(&self.pre_edit_entry_id.unwrap())
                  .map(Command::Deleted);

               let out_cmd: BoxFuture<'static, Command> = Box::pin(result);

               out_cmd
            })
         }
         Command::Deleted(result) => {
            if result.is_ok() {
               self.reset();

               Some(Box::pin(async { Command::GoTo(Screen::OtpDisplay) }))
            }
            else {
               self.view_model.state =
                  State::ProcessingFailed("Something happened, entry deletion wasn't successful!".to_owned());

               None
            }
         }
         Command::GoTo(_) => None,
      }
   }
}

#[cfg(test)]
mod tests {
   use std::{convert::TryInto, sync::Arc, time::Duration};

   use async_lock::RwLock as AsyncRwLock;
   use domain::{
      entities::{
         create_test_entry,
         entry::{self, Entry},
      },
      repository::InMemory,
      use_cases::{create_entry, delete_entry::EntryDeletion, qr_generation::QrGeneration, SharedRepository},
   };

   use crate::{
      create_entry::{Command, DeleteLabels, EntryCreation, Labels, State, ViewModel},
      Screen,
   };

   fn default_labels() -> Labels {
      Labels {
         title: "New Entry".to_owned(),
         issuer: "Issuer: Google, AWS, LastPass, etc.".to_owned(),
         description: "Description: Personal email, work account, secondary account".to_owned(),
         secret: "Secret: Provided by the issuer".to_owned(),
         cancel: "Cancel".to_owned(),
         create_edit: "Create".to_owned(),
         delete: None,
         shortcuts: "Press Esc to go back".to_owned(),
      }
   }

   fn edit_labels() -> Labels {
      Labels {
         title: "Edit Entry".to_owned(),
         create_edit: "Edit".to_owned(),
         delete: Some(DeleteLabels {
            delete: "Delete".to_owned(),
            confirm_delete: "Delete entry?".to_owned(),
            ask_delete: "Yes, delete it".to_owned(),
         }),
         shortcuts: "Press Ctrl+D to delete\nPress Enter to save changes\nPress Esc to go back".to_owned(),
         ..default_labels()
      }
   }

   #[test]
   fn initial_state_is_empty_and_is_invalid_inputs() {
      let repository: SharedRepository = Arc::new(AsyncRwLock::new(InMemory::new()));
      let presenter = EntryCreation::new(
         create_entry::EntryCreation::new(Arc::clone(&repository)),
         EntryDeletion::new(repository),
         QrGeneration::new(),
      );

      let expected_view_model = ViewModel {
         labels: default_labels(),
         issuer: String::new(),
         description: String::new(),
         secret: String::new(),
         qr_code: vec![],
         state: State::InvalidInputs,
      };

      assert_eq!(presenter.view_model(), expected_view_model);
   }

   #[test]
   fn when_the_issuer_and_the_secret_are_not_empty_the_state_is_valid_inputs() {
      let repository: SharedRepository = Arc::new(AsyncRwLock::new(InMemory::new()));
      let mut presenter = EntryCreation::new(
         create_entry::EntryCreation::new(Arc::clone(&repository)),
         EntryDeletion::new(repository),
         QrGeneration::new(),
      );

      let expected_view_model = ViewModel {
         labels: default_labels(),
         issuer: String::new(),
         description: String::new(),
         secret: String::new(),
         qr_code: vec![],
         state: State::InvalidInputs,
      };
      assert_eq!(presenter.view_model(), expected_view_model);

      presenter.interpret(Command::SetIssuer("issuer".to_owned()));
      let expected_view_model = ViewModel {
         issuer: "issuer".to_owned(),
         ..expected_view_model
      };
      assert_eq!(presenter.view_model(), expected_view_model);

      presenter.interpret(Command::SetSecret("secret".to_owned()));
      let expected_view_model = ViewModel {
         labels: Labels {
            shortcuts: "Press Enter to save changes\nPress Esc to go back".to_owned(),
            ..expected_view_model.labels
         },
         secret: "secret".to_owned(),
         state: State::ValidInputs,
         ..expected_view_model
      };
      assert_eq!(presenter.view_model(), expected_view_model);

      presenter.interpret(Command::SetIssuer(String::new()));
      let expected_view_model = ViewModel {
         labels: Labels {
            shortcuts: "Press Esc to go back".to_owned(),
            ..expected_view_model.labels
         },
         issuer: String::new(),
         state: State::InvalidInputs,
         ..expected_view_model
      };
      assert_eq!(presenter.view_model(), expected_view_model);
   }

   #[test]
   fn only_on_valid_inputs_can_create() {
      let repository: SharedRepository = Arc::new(AsyncRwLock::new(InMemory::new()));
      let mut presenter = EntryCreation::new(
         create_entry::EntryCreation::new(Arc::clone(&repository)),
         EntryDeletion::new(repository),
         QrGeneration::new(),
      );

      let cmd = presenter.interpret(Command::CreateOrUpdate);
      assert!(cmd.is_none());
      let expected_view_model = ViewModel {
         labels: default_labels(),
         issuer: String::new(),
         description: String::new(),
         secret: String::new(),
         qr_code: vec![],
         state: State::InvalidInputs,
      };
      assert_eq!(presenter.view_model(), expected_view_model);

      presenter.interpret(Command::SetIssuer("a".to_owned()));
      let cmd = presenter.interpret(Command::CreateOrUpdate);
      assert!(cmd.is_none());
      let expected_view_model = ViewModel {
         issuer: "a".to_owned(),
         ..expected_view_model
      };
      assert_eq!(presenter.view_model(), expected_view_model);

      presenter.interpret(Command::SetSecret("b".to_owned()));
      let cmd = presenter.interpret(Command::CreateOrUpdate);
      assert!(cmd.is_some());
      let expected_view_model = ViewModel {
         labels: Labels {
            shortcuts: "Press Enter to save changes\nPress Esc to go back".to_owned(),
            ..expected_view_model.labels
         },
         issuer: "a".to_owned(),
         secret: "b".to_owned(),
         state: State::Processing,
         ..expected_view_model
      };
      assert_eq!(presenter.view_model(), expected_view_model);
   }

   #[test]
   fn creating_resets_the_view_state_and_goes_to_otp_display() {
      let repository: SharedRepository = Arc::new(AsyncRwLock::new(InMemory::new()));
      let mut presenter = EntryCreation::new(
         create_entry::EntryCreation::new(Arc::clone(&repository)),
         EntryDeletion::new(repository),
         QrGeneration::new(),
      );

      presenter.interpret(Command::SetIssuer("issuer".to_owned()));
      presenter.interpret(Command::SetDescription("description".to_owned()));
      presenter.interpret(Command::SetSecret("secret".to_owned()));

      let entry_created = Entry {
         id: entry::Id::generate(),
         issuer: "issuer".to_owned(),
         description: "description".to_owned(),
         secret: "secret".try_into().unwrap(),
         time_step: Duration::from_secs(1).try_into().unwrap(),
      };

      let cmd = presenter.interpret(Command::CreatedOrUpdated(Ok(entry_created)));

      assert!(cmd.is_some());

      async_std::task::block_on(async move {
         let command: Command = cmd.unwrap().await;
         assert_eq!(command, Command::GoTo(Screen::OtpDisplay))
      });

      let expected_view_model = ViewModel {
         labels: default_labels(),
         issuer: String::new(),
         description: String::new(),
         secret: String::new(),
         qr_code: vec![],
         state: State::InvalidInputs,
      };
      assert_eq!(presenter.view_model(), expected_view_model);
   }

   #[test]
   fn cancelling_resets_the_view_state_and_goes_to_otp_display() {
      let repository: SharedRepository = Arc::new(AsyncRwLock::new(InMemory::new()));
      let mut presenter = EntryCreation::new(
         create_entry::EntryCreation::new(Arc::clone(&repository)),
         EntryDeletion::new(repository),
         QrGeneration::new(),
      );

      presenter.interpret(Command::SetIssuer("issuer".to_owned()));
      presenter.interpret(Command::SetDescription("description".to_owned()));
      presenter.interpret(Command::SetSecret("secret".to_owned()));

      let cmd = presenter.interpret(Command::Cancel);

      assert!(cmd.is_some());

      async_std::task::block_on(async move {
         let command: Command = cmd.unwrap().await;
         assert_eq!(command, Command::GoTo(Screen::OtpDisplay))
      });

      let expected_view_model = ViewModel {
         labels: default_labels(),
         issuer: String::new(),
         description: String::new(),
         secret: String::new(),
         qr_code: vec![],
         state: State::InvalidInputs,
      };

      assert_eq!(presenter.view_model(), expected_view_model);
   }

   #[test]
   fn loading_an_entry_works() {
      let repository: SharedRepository = Arc::new(AsyncRwLock::new(InMemory::new()));

      let mut entry_creation_use_case = create_entry::EntryCreation::new(Arc::clone(&repository));

      let saved_entry = create_test_entry(0);
      let result = entry_creation_use_case.create_entry(saved_entry.clone());
      async_std::task::block_on(async move { assert!(result.await.is_ok()) });

      let mut presenter = EntryCreation::new(
         entry_creation_use_case,
         EntryDeletion::new(repository),
         QrGeneration::new(),
      );

      let cmd = presenter.interpret(Command::Load(saved_entry.id.clone()));

      assert!(cmd.is_some());
      async_std::task::block_on(async move {
         assert_eq!(cmd.unwrap().await, Command::Edit(Ok(Some(saved_entry.clone()))))
      });

      let expected_view_model = ViewModel {
         labels: default_labels(),
         issuer: String::new(),
         description: String::new(),
         secret: String::new(),
         qr_code: vec![],
         state: State::Processing,
      };

      assert_eq!(presenter.view_model(), expected_view_model);
   }

   #[test]
   fn editing_an_entry_updates_view_model() {
      let repository: SharedRepository = Arc::new(AsyncRwLock::new(InMemory::new()));
      let mut presenter = EntryCreation::new(
         create_entry::EntryCreation::new(Arc::clone(&repository)),
         EntryDeletion::new(repository),
         QrGeneration::new(),
      );

      let loaded_entry = create_test_entry(0);

      let cmd = presenter.interpret(Command::Edit(Ok(Some(loaded_entry.clone()))));

      assert!(cmd.is_none());

      let generated_qr = QrGeneration::new().generate(&loaded_entry);
      assert!(generated_qr.is_ok(), "{:?}", generated_qr);

      let expected_view_model = ViewModel {
         labels: edit_labels(),
         issuer: loaded_entry.issuer,
         description: loaded_entry.description,
         secret: loaded_entry.secret.into(),
         qr_code: generated_qr.unwrap(),
         state: State::ValidInputs,
      };
      assert_eq!(presenter.view_model(), expected_view_model);

      presenter.interpret(Command::SetIssuer(String::new()));
      let expected_view_model = ViewModel {
         labels: Labels {
            shortcuts: "Press Ctrl+D to delete\nPress Esc to go back".to_owned(),
            ..expected_view_model.labels
         },
         issuer: String::new(),
         state: State::InvalidInputs,
         ..expected_view_model
      };
      assert_eq!(presenter.view_model(), expected_view_model);
   }

   #[test]
   fn an_entry_can_be_deleted_only_if_loaded() {
      let repository: SharedRepository = Arc::new(AsyncRwLock::new(InMemory::new()));

      let mut entry_creation_use_case = create_entry::EntryCreation::new(Arc::clone(&repository));

      let loaded_entry = create_test_entry(0);
      let result = entry_creation_use_case.create_entry(loaded_entry.clone());
      async_std::task::block_on(async move { assert!(result.await.is_ok()) });

      let mut presenter = EntryCreation::new(
         entry_creation_use_case,
         EntryDeletion::new(Arc::clone(&repository)),
         QrGeneration::new(),
      );

      let cmd = presenter.interpret(Command::Delete);

      assert!(cmd.is_none());

      async_std::task::block_on(async {
         assert!(
            repository
               .read()
               .await
               .all()
               .await
               .unwrap()
               .values
               .contains(&loaded_entry)
         );
      });
   }

   #[test]
   fn an_entry_can_be_deleted_only_if_awaiting_deletion_confirmation() {
      let repository: SharedRepository = Arc::new(AsyncRwLock::new(InMemory::new()));

      let mut entry_creation_use_case = create_entry::EntryCreation::new(Arc::clone(&repository));

      let loaded_entry = create_test_entry(0);
      let result = entry_creation_use_case.create_entry(loaded_entry.clone());
      async_std::task::block_on(async move { assert!(result.await.is_ok()) });

      let mut presenter = EntryCreation::new(
         entry_creation_use_case,
         EntryDeletion::new(Arc::clone(&repository)),
         QrGeneration::new(),
      );

      presenter.interpret(Command::Edit(Ok(Some(loaded_entry.clone()))));

      let cmd = presenter.interpret(Command::Delete);
      assert!(cmd.is_none());

      async_std::task::block_on(async {
         assert!(
            repository
               .read()
               .await
               .all()
               .await
               .unwrap()
               .values
               .contains(&loaded_entry)
         );
      });

      let cmd = presenter.interpret(Command::AwaitDeleteConfirmation);
      assert!(cmd.is_none());

      let cmd = presenter.interpret(Command::Delete);
      assert!(cmd.is_some());

      async_std::task::block_on(async move { cmd.unwrap().await });

      async_std::task::block_on(async {
         let contains_value = repository
            .read()
            .await
            .all()
            .await
            .unwrap()
            .values
            .contains(&loaded_entry);

         assert!(!contains_value);
      });
   }

   #[test]
   fn awaiting_deletion_confirmation_updates_view_model() {
      let repository: SharedRepository = Arc::new(AsyncRwLock::new(InMemory::new()));

      let mut entry_creation_use_case = create_entry::EntryCreation::new(Arc::clone(&repository));

      let loaded_entry = create_test_entry(0);
      let result = entry_creation_use_case.create_entry(loaded_entry.clone());
      async_std::task::block_on(async move { assert!(result.await.is_ok()) });

      let mut presenter = EntryCreation::new(
         entry_creation_use_case,
         EntryDeletion::new(Arc::clone(&repository)),
         QrGeneration::new(),
      );

      presenter.interpret(Command::Edit(Ok(Some(loaded_entry.clone()))));

      let generated_qr = QrGeneration::new().generate(&loaded_entry);
      assert!(generated_qr.is_ok(), "{:?}", generated_qr);

      let cmd = presenter.interpret(Command::AwaitDeleteConfirmation);
      assert!(cmd.is_none());

      let expected_view_model = ViewModel {
         labels: Labels {
            shortcuts: "Press Ctrl+Enter to confirm deletion\nPress Esc to cancel".to_owned(),
            ..edit_labels()
         },
         issuer: loaded_entry.issuer,
         description: loaded_entry.description,
         secret: loaded_entry.secret.into(),
         qr_code: generated_qr.unwrap(),
         state: State::AwaitingDeleteConfirmation,
      };
      assert_eq!(presenter.view_model(), expected_view_model);
   }

   #[test]
   fn cancelling_a_deletion_resets_view_state_to_previous_state() {
      let repository: SharedRepository = Arc::new(AsyncRwLock::new(InMemory::new()));
      let mut presenter = EntryCreation::new(
         create_entry::EntryCreation::new(Arc::clone(&repository)),
         EntryDeletion::new(repository),
         QrGeneration::new(),
      );

      let loaded_entry = create_test_entry(0);
      presenter.interpret(Command::Edit(Ok(Some(loaded_entry.clone()))));

      let generated_qr = QrGeneration::new().generate(&loaded_entry);
      assert!(generated_qr.is_ok(), "{:?}", generated_qr);

      let cmd = presenter.interpret(Command::AwaitDeleteConfirmation);
      assert!(cmd.is_none());

      let cmd = presenter.interpret(Command::Cancel);
      assert!(cmd.is_none());

      let expected_view_model = ViewModel {
         labels: edit_labels(),
         issuer: loaded_entry.issuer,
         description: loaded_entry.description,
         secret: loaded_entry.secret.into(),
         qr_code: generated_qr.unwrap(),
         state: State::ValidInputs,
      };

      assert_eq!(presenter.view_model(), expected_view_model);
   }
}
