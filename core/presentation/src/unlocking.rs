/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! UX logic for unlocking the application.\
//! The first time it is initialized, lets the user define a new password, which can be an empty one.
//! This password is used to encrypt and decrypt the data to and from the backing storage.

use domain::{
   entities::{encrypted::EncryptedData, entry::Entries, Serdelizer},
   use_cases::{
      unlocking::{
         unlocking::Unlocking as UseCase,
         AsyncDataSource,
         Error,
         PasswordHasher,
         Result as UnlockingResult,
      },
      SharedRepository,
      SharedStorage,
   },
};
use futures::{future::BoxFuture, FutureExt};

use crate::{Navigator, Screen};

#[derive(Clone, Debug)]
pub enum Action {
   SetPassword(String),
   SetPasswordRepetition(String),
   Unlock,
   Unlocked(UnlockingResult),
   RepositoryStorage((SharedRepository, SharedStorage)),
   GoTo(Screen),
}

impl PartialEq for Action {
   fn eq(&self, other: &Self) -> bool {
      match *self {
         Self::SetPassword(ref s1) => {
            if let Self::SetPassword(ref s2) = *other {
               return s1 == s2;
            }

            false
         }
         Self::SetPasswordRepetition(ref s1) => {
            if let Self::SetPasswordRepetition(ref s2) = *other {
               return s1 == s2;
            }

            false
         }
         Self::Unlock => {
            if Self::Unlock == *other {
               return true;
            }

            false
         }
         Self::Unlocked(_) => {
            if let Self::Unlocked(_) = *other {
               return true;
            }

            false
         }
         Self::RepositoryStorage(_) => {
            if let Self::RepositoryStorage(_) = *other {
               return true;
            }

            false
         }
         Self::GoTo(ref s1) => {
            if let Self::GoTo(ref s2) = *other {
               return s1 == s2;
            }

            false
         }
      }
   }
}

impl Eq for Action {}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Command {
   pub action: Option<Action>,
   pub go_to: Option<Screen>,
}

impl Navigator for Command {
   fn is_navigation(&self) -> Option<Screen> {
      self.go_to
   }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum State {
   ValidInputs,
   InvalidInputs,
   Unlocking,
   UnlockingFailed(String),
}

impl State {
   #[inline]
   fn isnt_unlocking(&self) -> bool {
      *self != Self::Unlocking
   }

   #[inline]
   pub const fn can_unlock(&self) -> bool {
      matches!(*self, Self::ValidInputs | Self::UnlockingFailed(_))
   }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Labels {
   pub title: String,
   pub password: String,
   pub password_repetition: Option<String>,
   pub action: String,
   pub error_message: String,
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct ViewModel {
   pub labels: Labels,
   pub password: String,
   pub password_repetition: Option<String>,
   pub state: State,
}

pub struct Unlocking<ADS, PH>
where
   ADS: AsyncDataSource,
   PH: 'static + PasswordHasher,
{
   use_case: UseCase<ADS, PH>,
   view_model: ViewModel,
   first_execution: bool,
}

impl<ADS, PH> Unlocking<ADS, PH>
where
   ADS: AsyncDataSource,
   PH: 'static + PasswordHasher,
{
   pub fn new(use_case: UseCase<ADS, PH>, first_execution: bool) -> Self {
      let view_model = if first_execution {
         ViewModel {
            labels: Labels {
               title: "Secure Your TOTP Entries - Password Setup".to_owned(),
               password: "Password".to_owned(),
               password_repetition: Some("Repeat Password".to_owned()),
               action: "Create Password".to_owned(),
               error_message: String::new(),
            },
            password: String::new(),
            password_repetition: Some(String::new()),
            state: State::ValidInputs,
         }
      }
      else {
         ViewModel {
            labels: Labels {
               title: "Enter Password to Unlock".to_owned(),
               password: "Password".to_owned(),
               password_repetition: None,
               action: "Unlock".to_owned(),
               error_message: String::new(),
            },
            password: String::new(),
            password_repetition: None,
            state: State::ValidInputs,
         }
      };

      Self {
         use_case,
         view_model,
         first_execution,
      }
   }

   #[inline]
   pub fn view_model(&self) -> ViewModel {
      self.view_model.clone()
   }

   #[inline]
   fn update_state(&mut self) {
      self.view_model.state = if !self.first_execution
         || &self.view_model.password == self.view_model.password_repetition.as_ref().unwrap()
      {
         State::ValidInputs
      }
      else {
         State::InvalidInputs
      }
   }

   pub fn interpret<EDS, ES>(&mut self, cmd: Command) -> Option<BoxFuture<'static, Command>>
   where
      EDS: 'static + Serdelizer<EncryptedData>,
      ES: 'static + Serdelizer<Entries>,
   {
      let Command { action, .. } = cmd;

      match action? {
         Action::SetPassword(password) => {
            if self.view_model.state.isnt_unlocking() {
               self.view_model.password = password;
               self.update_state();
            }
            None
         }
         Action::SetPasswordRepetition(password_repetition) => {
            if self.view_model.state.isnt_unlocking() && self.first_execution {
               self.view_model.password_repetition = Some(password_repetition);
               self.update_state();
            }
            None
         }
         Action::Unlock => {
            self.view_model.state.can_unlock().then(|| {
               self.view_model.state = State::Unlocking;

               let result = self
                  .use_case
                  .unlock::<EDS, ES>(self.view_model.password.clone())
                  .map(|result| {
                     Command {
                        action: Some(Action::Unlocked(result)),
                        go_to: None,
                     }
                  });

               let out_cmd: BoxFuture<'static, Command> = Box::pin(result);

               out_cmd
            })
         }
         Action::Unlocked(result) => {
            match result {
               Ok(repository_storage) => {
                  Some(Box::pin(async {
                     Command {
                        action: Some(Action::RepositoryStorage(repository_storage)),
                        go_to: Some(Screen::OtpDisplay),
                     }
                  }))
               }
               Err(e) => {
                  let msg = match e {
                     Error::ReadingDataSource(_)
                     | Error::RawDataDeserialization(_)
                     | Error::DecryptedEntriesDeserialization(_) => "Error reading entries file!",
                     Error::Decrypting => "Wrong password!",
                  };

                  self.view_model.state = State::UnlockingFailed(msg.to_owned());

                  None
               }
            }
         }
         Action::RepositoryStorage(_) | Action::GoTo(_) => None,
      }
   }
}

#[cfg(test)]
mod tests {
   use domain::{
      entities::{encrypted::EncryptedData, entry::Entries, Deserializer, Serializer},
      repository::storage::StubEncryptedStorage,
      scaffold_deserialize,
      use_cases::unlocking::{unlocking::Unlocking as UseCase, StubPasswordHasher},
   };

   use crate::unlocking::{Action, Command, Labels, State, Unlocking, ViewModel};

   #[derive(Debug, thiserror::Error)]
   #[error("Error")]
   struct TestError;

   scaffold_deserialize!(EncDataDummySerde, EncryptedData, _buf, { Ok(EncryptedData::new()) });
   scaffold_deserialize!(EntriesDummySerde, Entries, _buf, { Ok(Entries::default()) });

   #[test]
   fn initial_state_is_right_for_first_execution() {
      let presenter = Unlocking::new(
         UseCase::<StubEncryptedStorage, StubPasswordHasher>::new(StubEncryptedStorage, StubPasswordHasher),
         true,
      );

      let expected_view_model = ViewModel {
         labels: Labels {
            title: "Secure Your TOTP Entries - Password Setup".to_owned(),
            password: "Password".to_owned(),
            password_repetition: Some("Repeat Password".to_owned()),
            action: "Create Password".to_owned(),
            error_message: String::new(),
         },
         password: String::new(),
         password_repetition: Some(String::new()),
         state: State::ValidInputs,
      };
      assert_eq!(presenter.view_model(), expected_view_model);
   }

   #[test]
   fn initial_state_is_right_for_non_first_execution() {
      let presenter = Unlocking::new(
         UseCase::<StubEncryptedStorage, StubPasswordHasher>::new(StubEncryptedStorage, StubPasswordHasher),
         false,
      );

      let expected_view_model = ViewModel {
         labels: Labels {
            title: "Enter Password to Unlock".to_owned(),
            password: "Password".to_owned(),
            password_repetition: None,
            action: "Unlock".to_owned(),
            error_message: String::new(),
         },
         password: String::new(),
         password_repetition: None,
         state: State::ValidInputs,
      };
      assert_eq!(presenter.view_model(), expected_view_model);
   }

   #[test]
   fn if_executing_first_time_state_is_valid_inputs_only_if_both_passwords_match() {
      let mut presenter = Unlocking::new(
         UseCase::<StubEncryptedStorage, StubPasswordHasher>::new(StubEncryptedStorage, StubPasswordHasher),
         true,
      );

      let expected_view_model = ViewModel {
         labels: Labels {
            title: "Secure Your TOTP Entries - Password Setup".to_owned(),
            password: "Password".to_owned(),
            password_repetition: Some("Repeat Password".to_owned()),
            action: "Create Password".to_owned(),
            error_message: String::new(),
         },
         password: String::new(),
         password_repetition: Some(String::new()),
         state: State::ValidInputs,
      };
      // Empty password is a valid password. This equals both fields are empty.
      assert_eq!(presenter.view_model(), expected_view_model);

      presenter.interpret::<EncDataDummySerde, EntriesDummySerde>(Command {
         action: Some(Action::SetPassword("a".to_owned())),
         go_to: None,
      });
      let expected_view_model = ViewModel {
         password: "a".to_owned(),
         state: State::InvalidInputs,
         ..expected_view_model
      };
      assert_eq!(presenter.view_model(), expected_view_model);

      presenter.interpret::<EncDataDummySerde, EntriesDummySerde>(Command {
         action: Some(Action::SetPasswordRepetition("a".to_owned())),
         go_to: None,
      });
      let expected_view_model = ViewModel {
         password_repetition: Some("a".to_owned()),
         state: State::ValidInputs,
         ..expected_view_model
      };
      assert_eq!(presenter.view_model(), expected_view_model);

      presenter.interpret::<EncDataDummySerde, EntriesDummySerde>(Command {
         action: Some(Action::SetPasswordRepetition("ab".to_owned())),
         go_to: None,
      });
      let expected_view_model = ViewModel {
         password_repetition: Some("ab".to_owned()),
         state: State::InvalidInputs,
         ..expected_view_model
      };
      assert_eq!(presenter.view_model(), expected_view_model);

      presenter.interpret::<EncDataDummySerde, EntriesDummySerde>(Command {
         action: Some(Action::SetPassword("ab".to_owned())),
         go_to: None,
      });
      let expected_view_model = ViewModel {
         password: "ab".to_owned(),
         state: State::ValidInputs,
         ..expected_view_model
      };
      assert_eq!(presenter.view_model(), expected_view_model);
   }

   #[test]
   fn if_not_first_time_then_updates_view_state() {
      let mut presenter = Unlocking::new(
         UseCase::<StubEncryptedStorage, StubPasswordHasher>::new(StubEncryptedStorage, StubPasswordHasher),
         false,
      );

      presenter.interpret::<EncDataDummySerde, EntriesDummySerde>(Command {
         action: Some(Action::SetPassword("hunter2".to_owned())),
         go_to: None,
      });

      let expected_view_model = ViewModel {
         labels: Labels {
            title: "Enter Password to Unlock".to_owned(),
            password: "Password".to_owned(),
            password_repetition: None,
            action: "Unlock".to_owned(),
            error_message: String::new(),
         },
         password: "hunter2".to_owned(),
         password_repetition: None,
         state: State::ValidInputs,
      };
      assert_eq!(presenter.view_model(), expected_view_model);
   }

   #[test]
   fn if_unlocking_fails_then_doesnt_unlock_and_updates_view_state() {
      use domain::use_cases::unlocking::Error::Decrypting;

      let mut presenter = Unlocking::new(
         UseCase::<StubEncryptedStorage, StubPasswordHasher>::new(StubEncryptedStorage, StubPasswordHasher),
         false,
      );

      presenter.interpret::<EncDataDummySerde, EntriesDummySerde>(Command {
         action: Some(Action::SetPassword("hunter2".to_owned())),
         go_to: None,
      });

      let cmd = presenter.interpret::<EncDataDummySerde, EntriesDummySerde>(Command {
         action: Some(Action::Unlocked(Err(Decrypting))),
         go_to: None,
      });
      assert!(cmd.is_none());

      let expected_view_model = ViewModel {
         labels: Labels {
            title: "Enter Password to Unlock".to_owned(),
            password: "Password".to_owned(),
            password_repetition: None,
            action: "Unlock".to_owned(),
            error_message: String::new(),
         },
         password: "hunter2".to_owned(),
         password_repetition: None,
         state: State::UnlockingFailed("Wrong password!".to_owned()),
      };
      assert_eq!(presenter.view_model(), expected_view_model);
   }

   #[test]
   fn unlocks_only_if_has_valid_inputs() {
      let mut presenter = Unlocking::new(
         UseCase::<StubEncryptedStorage, StubPasswordHasher>::new(StubEncryptedStorage, StubPasswordHasher),
         false,
      );

      presenter.interpret::<EncDataDummySerde, EntriesDummySerde>(Command {
         action: Some(Action::SetPassword("hunter2".to_owned())),
         go_to: None,
      });
      let cmd = presenter.interpret::<EncDataDummySerde, EntriesDummySerde>(Command {
         action: Some(Action::Unlock),
         go_to: None,
      });
      assert!(cmd.is_some());
   }
}
