/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! Will re-generate the Cap'n Proto models whenever they change

#![allow(
   clippy::doc_markdown,
   clippy::items_after_statements,
   clippy::missing_fields_in_debug,
   clippy::missing_panics_doc,
   clippy::must_use_candidate,
   clippy::suspicious_else_formatting
)]
#![warn(rust_2018_idioms)]
#![deny(
   array_into_iter,
   clippy::rest_pat_in_fully_bound_structs,
   clippy::separated_literal_suffix,
   non_camel_case_types,
   private_in_public,
   rust_2018_idioms,
   trivial_casts,
   trivial_numeric_casts,
   type_alias_bounds,
   unknown_lints,
   unsafe_code,
   unused,
   unused_import_braces,
   while_true
)]

use capnpc::CompilerCommand;

#[allow(clippy::print_stdout)]
fn main() {
   println!("cargo:rustc-cfg=nightly_error_messages");

   const ENTRY_FILE_PATH: &str = r"src/schemas/entry.capnp";

   println!("cargo:rerun-if-changed={ENTRY_FILE_PATH}");

   let mut compiler_cmd = CompilerCommand::new();

   #[cfg(target_family = "windows")]
   {
      let exec_file_path = {
         let mut file_path = std::env::current_dir().unwrap();
         file_path.push("capnp.exe");
         file_path
      };

      let capnp_exec_exists = exec_file_path.exists();

      assert!(capnp_exec_exists, "Please download the Cap'n Proto executable (capnp.exe) and place it in '{}'.\n\
         Unless the URL has changed you can find it here: https://capnproto.org/install.html#installation-windows", exec_file_path.display());

      compiler_cmd.capnp_executable(exec_file_path);
   }
   #[cfg(not(target_family = "windows"))]
   {
      compiler_cmd.capnp_executable("capnp");
   }

   compiler_cmd
      .file(ENTRY_FILE_PATH)
      .src_prefix("src/schemas")
      .output_path("src/generated/")
      .default_parent_module(vec!["generated".to_owned()])
      .run()
      .expect("compiling schema");
}
