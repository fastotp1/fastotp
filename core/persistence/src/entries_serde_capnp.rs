/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! Allows de/serializing [Entries] into binary format

use std::{
   collections::HashSet,
   convert::{TryFrom, TryInto},
   time::Duration,
};

use capnp::{
   message::{Builder, ReaderOptions},
   serialize_packed,
};
use domain::entities::{
   entry::{Entries, Entry, Id, Secret},
   Deserializer,
   Serializer,
};

use crate::generated::entry_capnp::capnp_entries;

/// Allows de/serializing using Cap'n Proto schemas
pub struct EntriesCapnp;

impl Serializer for EntriesCapnp {
   type Entity = Entries;

   /// Serializes an [Entries] instance into a byte slice.
   ///
   /// # Errors
   /// Any kind of error while trying to serialize will return a `String` with the error description
   /// of the [Capnp Error](capnp::Error).
   fn serialize(entries: &Self::Entity) -> Result<Vec<u8>, String> {
      let mut message = Builder::new_default();

      let mut entries_builder = message
         .init_root::<'_, capnp_entries::Builder<'_>>()
         .init_entries(entries.values.len() as u32);

      for (i, entry) in entries.values.iter().enumerate() {
         let time_step: Duration = entry.time_step.into();
         let mut entry_builder = entries_builder.reborrow().get(i as u32);
         entry_builder.set_id(entry.id.to_string().as_str());
         entry_builder.set_issuer(entry.issuer.as_str());
         entry_builder.set_description(entry.description.as_str());
         entry_builder.set_secret(entry.secret.as_ref());
         entry_builder.set_time_step(time_step.as_secs());
      }

      let mut bytes: Vec<u8> = vec![];

      serialize_packed::write_message(&mut bytes, &message).map_err(|e| format!("{e}"))?;

      Ok(bytes)
   }
}

impl Deserializer for EntriesCapnp {
   type Entity = Entries;

   /// Deserializes a byte slice into an [Entries] struct.
   /// If the buffer is empty,
   ///
   /// # Errors
   /// It fails if it can't deserialize the entries or if after deserializing any among them
   /// are missing the issuer or the secret. Returns a `String` with the error description
   /// of the [Capnp Error](capnp::Error).
   fn deserialize(buf: &[u8]) -> Result<Self::Entity, String> {
      if buf.is_empty() {
         return Ok(Entries::default());
      }

      let message_reader =
         serialize_packed::read_message(buf, ReaderOptions::new()).map_err(|e| format!("{e}"))?;

      let entries_reader = message_reader
         .get_root::<'_, capnp_entries::Reader<'_>>()
         .map_err(|e| format!("{e}"))?;

      let entries = entries_reader
         .get_entries()
         .map_err(|e| format!("{e}"))?
         .iter()
         .map(|reader| {
            let id = reader
               .get_id()
               .map_err(|_| "no id found for entry".to_owned())
               .and_then(Id::try_from)
               .map_err(|e| format!("there's an invalid entry ({e})"))?;

            let issuer = reader
               .get_issuer()
               .map_err(|_| "no issuer found for entry".to_owned())?;

            let secret = reader
               .get_secret()
               .map_err(|_| "no secret found for entry".to_owned())
               .and_then(|s| Secret::try_from(s).map_err(|e| e.to_string()))
               .map_err(|e| format!("there's an invalid entry ({e})"))?;

            Ok(Entry {
               id,
               issuer: issuer.to_owned(),
               description: reader
                  .get_description()
                  .map_or_else(|_| String::new(), ToOwned::to_owned),
               secret,
               time_step: Duration::from_secs(reader.get_time_step()).try_into().unwrap(),
            })
         })
         .collect::<Result<HashSet<Entry>, String>>();

      entries.map(|values| Entries { values })
   }
}

#[cfg(test)]
mod tests {
   use std::collections::HashSet;

   use domain::{
      entities,
      entities::{entry::Entries, Deserializer, Serializer},
   };

   use crate::entries_serde_capnp::EntriesCapnp;

   #[test]
   fn serialization_and_deserialization_work() {
      let mut values = HashSet::new();
      values.insert(entities::create_test_entry(1));
      values.insert(entities::create_test_entry(2));
      values.insert(entities::create_test_entry(3));
      values.insert(entities::create_test_entry(4));
      values.insert(entities::create_test_entry(5));

      let entries = Entries { values };

      let serialized = &EntriesCapnp::serialize(&entries).unwrap();
      let deserialized = EntriesCapnp::deserialize(serialized);

      assert!(deserialized.is_ok());
      let actual = deserialized.unwrap();
      assert_eq!(actual, entries)
   }

   #[test]
   fn serialization_and_deserialization_work_with_zero_entries() {
      let entries = Entries { values: HashSet::new() };

      let serialized = &EntriesCapnp::serialize(&entries).unwrap();
      let deserialized = EntriesCapnp::deserialize(serialized);

      assert!(deserialized.is_ok());
      let actual = deserialized.unwrap();
      assert_eq!(actual, entries)
   }

   #[test]
   fn deserialization_returns_empty_entries_if_bytes_buffer_is_empty() {
      let deserialized = EntriesCapnp::deserialize(&[]);

      let actual = deserialized.expect("Shouldn't be an error");
      assert_eq!(actual, Entries::default())
   }

   #[test]
   fn deserialization_returns_an_error_if_the_bytes_arent_serialized() {
      let wrong_serialized: [u8; 8] = [1, 2, 3, 4, 5, 6, 7, 8];

      let deserialized = EntriesCapnp::deserialize(&wrong_serialized);

      assert!(matches!(deserialized, Err(_)));
   }
}
