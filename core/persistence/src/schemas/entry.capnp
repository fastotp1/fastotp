@0xe2c49696982d42cc;

struct CapnpEntry @0xb9e21a0c67d3d724 {
   id @0 :Text;
   issuer @1 :Text;
   description @2 :Text;
   secret @3 :Text;
   timeStep @4 :UInt64;
}

struct CapnpEntries @0xa37590debb91846a {
   entries @0 :List(CapnpEntry);
}

struct EncryptedData @0x8c15ed7c6305dccc {
   phcStr @0 :Data;
   nonce @1 :Data;
   payload @2 :Data;
}