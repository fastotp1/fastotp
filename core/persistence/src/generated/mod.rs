/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#[allow(
   clippy::all,
   clippy::pedantic,
   clippy::restriction,
   clippy::nursery,
   clippy::cargo,
   dead_code,
   unknown_lints,
   unused_variables,
   unreachable_code,
   unused_imports,
   unused_extern_crates
)]
pub mod entry_capnp;
