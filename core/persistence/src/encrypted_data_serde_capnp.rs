/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! Allows serializing and deserializing [Entries](super::super::domain::entities::entry::Entries) into binary format

use capnp::{
   message::{Builder, ReaderOptions},
   serialize_packed,
};
use domain::entities::{
   encrypted::{EncryptedData, EncryptionHeader},
   Deserializer,
   Serializer,
};

use crate::generated::entry_capnp::encrypted_data;

/// Allows de/serializing using Cap'n Proto schemas
pub struct EncryptedDataCapnp;

impl Serializer for EncryptedDataCapnp {
   type Entity = EncryptedData;

   /// Serializes an [EncryptedData] instance into a byte slice.
   ///
   /// # Errors
   /// Any kind of error while trying to serialize will return a `String` with the error description
   /// of the [Capnp Error](capnp::Error).
   fn serialize(encrypted_data: &Self::Entity) -> Result<Vec<u8>, String> {
      let mut message = Builder::new_default();

      let mut encrypted_data_builder = message.init_root::<'_, encrypted_data::Builder<'_>>();

      encrypted_data_builder.set_phc_str(encrypted_data.header.phc_str.as_bytes());
      encrypted_data_builder.set_nonce(encrypted_data.header.nonce.as_slice());
      encrypted_data_builder.set_payload(encrypted_data.payload.as_slice());

      let mut bytes: Vec<u8> = vec![];

      serialize_packed::write_message(&mut bytes, &message).map_err(|e| format!("{e}"))?;

      Ok(bytes)
   }
}

impl Deserializer for EncryptedDataCapnp {
   type Entity = EncryptedData;

   /// Deserializes a byte slice into an [EncryptedData] struct.
   /// If the buffer is empty,
   ///
   /// # Errors
   /// Any kind of error while trying to serialize will return a `String` with the error description
   /// of the [Capnp Error](capnp::Error).
   fn deserialize(buf: &[u8]) -> Result<Self::Entity, String> {
      if buf.is_empty() {
         return Ok(EncryptedData::new());
      }

      let message_reader =
         serialize_packed::read_message(buf, ReaderOptions::new()).map_err(|e| format!("{e}"))?;

      let encrypted_data_reader = message_reader
         .get_root::<'_, encrypted_data::Reader<'_>>()
         .map_err(|e| format!("{e}"))?;

      let phc_str = encrypted_data_reader
         .get_phc_str()
         .map_err(|e| format!("{e}"))
         .and_then(|bytes| String::from_utf8(bytes.to_vec()).map_err(|e| format!("{e}")))?;

      let header = EncryptionHeader {
         phc_str,
         nonce: encrypted_data_reader.get_nonce().map_err(|e| format!("{e}"))?.to_vec(),
      };

      Ok(EncryptedData {
         header,
         payload: encrypted_data_reader
            .get_payload()
            .map_err(|e| format!("{e}"))?
            .to_vec(),
      })
   }
}

#[cfg(test)]
mod tests {
   use domain::entities::{encrypted::EncryptedData, Deserializer, Serializer};

   use crate::encrypted_data_serde_capnp::EncryptedDataCapnp;

   #[test]
   fn serialization_and_deserialization_work() {
      let mut ed = EncryptedData::new();
      ed.payload = vec![1u8, 2, 3, 4, 5, 6, 7, 8, 9];

      let serialized = &EncryptedDataCapnp::serialize(&ed).unwrap();
      let deserialized = EncryptedDataCapnp::deserialize(serialized);

      assert!(deserialized.is_ok());
      let actual = deserialized.unwrap();
      assert_eq!(actual, ed)
   }

   #[test]
   fn deserialization_returns_empty_entries_if_bytes_buffer_is_empty() {
      let deserialized = EncryptedDataCapnp::deserialize(&[]);

      let actual = deserialized.expect("Shouldn't be an error");
      assert!(!actual.header.phc_str.is_empty());
      assert!(!actual.header.nonce.is_empty());
      assert!(actual.payload.is_empty());
   }

   #[test]
   fn deserialization_returns_an_error_if_the_bytes_arent_serialized() {
      let wrong_serialized: [u8; 8] = [1, 2, 3, 4, 5, 6, 7, 8];

      let deserialized = EncryptedDataCapnp::deserialize(&wrong_serialized);

      assert!(matches!(deserialized, Err(_)));
   }
}
