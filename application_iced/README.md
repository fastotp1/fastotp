# `fastotp_iced_gui`
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

GUI implementation of the application making of use of [Iced](https://github.com/hecrj/iced) library.

![Unlock screen](readme_assets/unlock_screen.png "Unlock screen")
![Main screen](readme_assets/general_screen.png "Main screen")
![Filtering screen](readme_assets/filtered_screen.png "Filtering screen")
![Single entry filtered screen](readme_assets/single_issuer_screen.png "Single entry filtered screen")
![Entry creation / update screen](readme_assets/create_edit_screen.png "Entry creation / update screen")
![Password change screen](readme_assets/password_change.png "Password change screen")

## Structure

### `assets`

Comprised of:

* Application icon
* Text fonts
* Icon fonts

### `logos`

Assortment of possible issuers' logos.

### `readme_assets`

The showcase images shown in this README file.

### `src`

Each directory corresponds to an application screen, composed of a `view`, a possible `subscription` to manage IO events, and a `module`. \
A `module` is the combination of `view`, `subscription`, and domain logic from [`core`](../core) that pertains the screen.

## Build

If you are on a Debian system make sure to install the following libxcb packages:

```shell script
sudo apt install libxcb-render0-dev libxcb-shape0-dev libxcb-xfixes0-dev
```

To build using the Iced frontend you can use the following [cargo alias](../.cargo/config.toml)

```shell script
cargo release_iced
```

## Acknowledgements

[github.com/andOTP/andOTP](https://github.com/andOTP/andOTP): The initial set of logos was borrowed from this project.
Most of them were left intact, although some had to be retrofitted to work as SVG instead of an Android Drawable.

## License

[AGPL-3.0-or-later](LICENSE.AGPL)
