/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! GUI rendering of the view for the main screen of the application, that is searching and displaying
//! TOTP generated codes.

use iced::{
   widget::{text_input, Button, Column, Row, Text},
   Alignment,
   Element,
   Length,
   Renderer,
};
use once_cell::sync::Lazy;
use presentation::change_password::{Command, State, ViewModel};

use crate::{
   fonts,
   icons,
   settings::change_password::{Message, ViewCommand},
   theme,
   theme::FastOtpTheme,
   widget_styling_utils,
   widget_styling_utils::{And, ButtonExt},
};

pub static PASSWORD_ID: Lazy<text_input::Id> = Lazy::new(text_input::Id::unique);
static PASSWORD_REPETITION_ID: Lazy<text_input::Id> = Lazy::new(text_input::Id::unique);

/// Visual state of the module exclusively related to Iced implementation
#[derive(Debug, Clone)]
pub(super) struct View {
   passwords_visible: bool,
}

impl View {
   pub fn new() -> Self {
      Self {
         passwords_visible: false,
      }
   }

   pub fn update(&mut self, view_cmd: ViewCommand) -> iced::Command<Message> {
      self.passwords_visible = matches!(view_cmd, ViewCommand::ShowPasswords);

      iced::Command::none()
   }

   pub fn render(&self, view_model: ViewModel) -> Element<'_, Message, Renderer<FastOtpTheme>> {
      let title = widget_styling_utils::title_text(view_model.labels.title);

      let show_passwords = self.passwords_visible;

      let password = widget_styling_utils::input_field(view_model.labels.password.as_str(), &view_model.password)
         .id(PASSWORD_ID.clone())
         .and(|mut txt| {
            if !show_passwords {
               txt = txt.password();
            }
            txt
         })
         .on_input(|text| Message::Model(Command::SetPassword(text)));

      let password_repetition = widget_styling_utils::input_field(
         view_model.labels.password_repetition.as_str(),
         view_model.password_repetition.as_str(),
      )
      .id(PASSWORD_REPETITION_ID.clone())
      .and(|mut txt| {
         if !show_passwords {
            txt = txt.password();
         }
         txt
      })
      .on_input(|text| Message::Model(Command::SetPasswordRepetition(text)));

      let fields = Column::new()
         .align_items(Alignment::Center)
         .spacing(15)
         .push(password)
         .push(password_repetition);

      let (visibility_icon, visibility_cmd) = if show_passwords {
         (icons::visible(), ViewCommand::HidePasswords)
      }
      else {
         (icons::hidden(), ViewCommand::ShowPasswords)
      };

      let fields_with_hiding = Row::new()
         .align_items(Alignment::Center)
         .spacing(10)
         .push(fields.width(Length::FillPortion(28)))
         .push(
            Button::new(visibility_icon.size(widget_styling_utils::BUTTONS_TEXT_SIZE))
               .padding(0)
               .width(Length::Fixed(f32::from(widget_styling_utils::BUTTONS_TEXT_SIZE)))
               .on_press(Message::View(visibility_cmd))
               .style(theme::Buttons::PasswordVisibility)
               .width(Length::FillPortion(1)),
         );

      let cancel_button = Button::new(
         Text::new(view_model.labels.cancel)
            .font(fonts::MULI_SEMIBOLD)
            .size(widget_styling_utils::BUTTONS_TEXT_SIZE),
      )
      .enable(
         view_model.state.isnt_changing_password(),
         Message::Model(Command::Cancel),
      )
      .style(theme::Buttons::Cancel)
      .padding(10);

      let action_button = Button::new(
         Text::new(view_model.labels.action)
            .font(fonts::MULI_SEMIBOLD)
            .size(widget_styling_utils::BUTTONS_TEXT_SIZE),
      )
      .enable(view_model.state.can_unlock(), Message::Model(Command::ChangePassword))
      .style(theme::Buttons::ChangePassword)
      .padding(10);

      let mut col = Column::new()
         .align_items(Alignment::Center)
         .spacing(15)
         .push(title)
         .push(fields_with_hiding)
         .push(
            Row::new()
               .spacing(20)
               .align_items(Alignment::Center)
               .push(cancel_button)
               .push(action_button),
         );

      if let State::PasswordChangeFailed(error_msg) = view_model.state {
         col = col.push(widget_styling_utils::info_message_text(error_msg));
      }

      col.into()
   }
}
