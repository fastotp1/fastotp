/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! A self contained module for the entire password change functionality, including GUI rendering using
//! [Iced](https://github.com/hecrj/iced)

mod subscription;
mod view;

use iced::{widget, Element, Renderer, Subscription};
use presentation::{
   change_password::{ChangePassword, Command},
   Screen,
};
pub use view::PASSWORD_ID as FIRST_FOCUS_ID;

use crate::{settings::change_password::view::View, theme::FastOtpTheme};

/// Commands solely for the GUI to interpret. They do not alter the application state, only view state
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum ViewCommand {
   HidePasswords,
   ShowPasswords,
   PreviousField,
   NextField,
}

/// Permits differentiate between commands that modify GUI-only state vs underlying application state
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum Message {
   Model(Command),
   View(ViewCommand),
}

pub enum UpdateResult<T> {
   Model(iced::Command<Message>),
   Widget(iced::Command<T>),
}

/// Represents the complete functionality for changing the password in the application
pub struct Module {
   presenter: ChangePassword,
   view: View,
}

impl Module {
   pub fn new(presenter: ChangePassword) -> Self {
      Self {
         presenter,
         view: View::new(),
      }
   }

   pub fn update<T: 'static>(&mut self, view_message: Message) -> UpdateResult<T> {
      match view_message {
         Message::Model(cmd) => {
            let reset_hide_password = match cmd {
               Command::Cancel => iced::Command::perform(async move { ViewCommand::HidePasswords }, Message::View),
               Command::SetPassword(_)
               | Command::SetPasswordRepetition(_)
               | Command::ChangePassword
               | Command::PasswordChanged(_)
               | Command::GoTo(_) => iced::Command::none(),
            };

            let presenter = &mut self.presenter;

            let command = presenter.interpret(cmd).map_or_else(iced::Command::none, |future| {
               iced::Command::perform(future, Message::Model)
            });

            UpdateResult::Model(iced::Command::batch([reset_hide_password, command]))
         }
         Message::View(view_cmd) => {
            match view_cmd {
               ViewCommand::PreviousField => UpdateResult::Widget(widget::focus_previous()),
               ViewCommand::NextField => UpdateResult::Widget(widget::focus_next()),
               ViewCommand::HidePasswords | ViewCommand::ShowPasswords => {
                  UpdateResult::Model(self.view.update(view_cmd))
               }
            }
         }
      }
   }

   pub fn view(&self) -> Element<'_, super::Messages, Renderer<FastOtpTheme>> {
      let presenter = &self.presenter;

      self
         .view
         .render(presenter.view_model())
         .map(super::Messages::ChangePassword)
   }

   pub fn subscription(&self, screen: Screen) -> Subscription<super::Messages> {
      Subscription::from_recipe(subscription::IoEvents {
         mute_io_events: screen != Screen::ChangePassword,
      })
      .map(super::Messages::ChangePassword)
   }
}
