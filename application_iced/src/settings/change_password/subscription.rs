/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! See [IoEvents]

use futures::{stream::BoxStream, StreamExt};
use iced::{event::Status, Event as InputEvent};
use iced_core::Hasher;
use iced_runtime::futures::subscription::Recipe;
use presentation::change_password::Command;

use crate::settings::change_password::{Message, ViewCommand};

/// Controls the consumption of IO events (mouse and keyboard) of the change password screen functionality
pub struct IoEvents {
   pub mute_io_events: bool,
}

impl Recipe for IoEvents {
   type Output = Message;

   fn hash(&self, state: &mut Hasher) {
      use std::hash::Hash;

      self.mute_io_events.hash(state);
      "ChangePassword.CommandsSubscription".hash(state);
   }

   fn stream(self: Box<Self>, input: BoxStream<'_, (InputEvent, Status)>) -> BoxStream<'_, Self::Output> {
      use iced_core::keyboard::{Event, KeyCode, Modifiers};

      if self.mute_io_events {
         return futures::stream::empty().boxed();
      }

      let input_as_commands = input.filter_map(move |(io_event, _)| {
         const NO_MODIFIER: Modifiers = Modifiers::empty();

         let cmd = if let InputEvent::Keyboard(Event::KeyReleased {
            key_code,
            modifiers: active_modifiers @ (NO_MODIFIER | Modifiers::SHIFT),
         }) = io_event
         {
            if key_code == KeyCode::Escape {
               Some(Message::Model(Command::Cancel))
            }
            else if key_code == KeyCode::Tab {
               let view_cmd = if active_modifiers.shift() {
                  ViewCommand::PreviousField
               }
               else {
                  ViewCommand::NextField
               };

               Some(Message::View(view_cmd))
            }
            else {
               None
            }
         }
         else {
            None
         };

         async move { cmd }
      });

      input_as_commands.boxed()
   }
}
