/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! The [Iced](https://github.com/hecrj/iced) version of the frontend for the view which groups
//! password change and potentially other user configuration. Makes use of its own [Messages]
//! to group the messages emitted by nested presenters.

//! A grouping module used to encapsulate functionalities related to the administration of the application.

mod change_password;
mod view;

pub use change_password::{Message as ChangePasswordMessage, Module as ChangePasswordModule, FIRST_FOCUS_ID};
use iced::{Command, Element, Renderer, Subscription};
use presentation::{Navigator, Screen};

use crate::{
   application,
   settings::{
      change_password::UpdateResult,
      view::{NestedViews, View},
   },
   theme::FastOtpTheme,
};

/// Grouping of messages for nested presenters.
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum Messages {
   ChangePassword(change_password::Message),
}

impl Navigator for Messages {
   fn is_navigation(&self) -> Option<Screen> {
      match *self {
         Self::ChangePassword(ref cmd) => {
            match *cmd {
               change_password::Message::Model(ref presentation_cmd) => presentation_cmd.is_navigation(),
               change_password::Message::View(_) => None,
            }
         }
      }
   }
}

/// Represents the complete functionality for managing the administrative settings of the application
pub struct Module {
   view: View,
   change_password: change_password::Module,
}

impl Module {
   pub fn new(change_password: change_password::Module) -> Self {
      Self {
         change_password,
         view: View::new(),
      }
   }

   pub fn update(&mut self, msg: Messages) -> Command<application::Message> {
      match msg {
         Messages::ChangePassword(subcmd) => {
            match self.change_password.update::<application::Message>(subcmd) {
               UpdateResult::Model(cmd) => cmd.map(Messages::ChangePassword).map(application::Message::Settings),
               UpdateResult::Widget(cmd) => cmd,
            }
         }
      }
   }

   pub fn view(&self) -> Element<'_, application::Message, Renderer<FastOtpTheme>> {
      let change_password = self.change_password.view().map(application::Message::Settings);

      self.view.render(NestedViews { change_password })
   }

   pub fn subscription(&self, screen: Screen) -> Subscription<application::Message> {
      self
         .change_password
         .subscription(screen)
         .map(application::Message::Settings)
   }
}
