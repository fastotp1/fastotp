/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! GUI rendering of the view for the create entry functionality

use iced::{
   widget::{Column, Container, Scrollable},
   Alignment,
   Element,
   Length,
   Renderer,
};

use crate::{application::Message, theme::FastOtpTheme};

/// Grouping of view states for nested views.
pub struct NestedViews<'state> {
   pub(super) change_password: Element<'state, Message, Renderer<FastOtpTheme>>,
}

/// Visual state for the settings module
#[derive(Debug, Clone)]
pub struct View;

impl View {
   pub fn new() -> Self {
      Self {}
   }

   pub fn render<'state>(
      &'state self,
      views: NestedViews<'state>,
   ) -> Element<'state, Message, Renderer<FastOtpTheme>> {
      let mut col = Column::new().align_items(Alignment::Start).spacing(30);

      col = col.push(views.change_password);

      Container::new(Scrollable::new(col))
         .width(Length::Fill)
         .height(Length::Shrink)
         .into()
   }
}
