/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! The [Iced](https://github.com/hecrj/iced) version of the frontend for the view associated
//! with the main unlocking screen.

mod subscription;
mod view;

use domain::{
   entities::{encrypted::EncryptedData, entry::Entries, Serdelizer},
   use_cases::unlocking::{AsyncDataSource, PasswordHasher},
};
use iced::{widget, Element, Renderer, Subscription};
use presentation::{
   unlocking::{Command, Unlocking as Presenter},
   Navigator,
   Screen,
};

use crate::{application, theme::FastOtpTheme, unlocking::view::View};

/// Commands solely for the GUI to interpret. They do not alter the application state, only view state
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum ViewCommand {
   HidePasswords,
   ShowPasswords,
   PreviousField,
   NextField,
}

/// Permits differentiate between commands that modify GUI-only state vs underlying application state
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum Message {
   Model(Command),
   View(ViewCommand),
}

pub use view::PASSWORD_ID as FIRST_FOCUS_ID;

impl Navigator for Message {
   fn is_navigation(&self) -> Option<Screen> {
      match *self {
         Self::Model(ref presentation_cmd) => presentation_cmd.is_navigation(),
         Self::View(_) => None,
      }
   }
}

/// Represents the complete functionality unlocking the TOTP entries of the application and loading
/// them into memory
pub struct Module<ADS, PH>
where
   ADS: AsyncDataSource,
   PH: 'static + PasswordHasher,
{
   presenter: Presenter<ADS, PH>,
   view: View,
}

impl<ADS, PH> Module<ADS, PH>
where
   ADS: AsyncDataSource,
   PH: 'static + PasswordHasher,
{
   pub fn new(presenter: Presenter<ADS, PH>) -> Self {
      Self {
         presenter,
         view: View::new(),
      }
   }

   pub fn update<EDS, ES>(&mut self, msg: Message) -> iced::Command<application::Message>
   where
      EDS: 'static + Serdelizer<EncryptedData>,
      ES: 'static + Serdelizer<Entries>,
   {
      match msg {
         Message::Model(cmd) => {
            let presenter = &mut self.presenter;

            presenter
               .interpret::<EDS, ES>(cmd)
               .map_or(iced::Command::none(), |future| {
                  iced::Command::perform(future, Message::Model)
               })
               .map(application::Message::Unlocking)
         }
         Message::View(view_cmd) => {
            match view_cmd {
               ViewCommand::PreviousField => widget::focus_previous(),
               ViewCommand::NextField => widget::focus_next(),
               ViewCommand::HidePasswords | ViewCommand::ShowPasswords => {
                  self.view.update(view_cmd).map(application::Message::Unlocking)
               }
            }
         }
      }
   }

   pub fn view(&self) -> Element<'_, application::Message, Renderer<FastOtpTheme>> {
      let presenter = &self.presenter;

      self
         .view
         .render(presenter.view_model())
         .map(application::Message::Unlocking)
   }

   pub fn subscription(&self, screen: Screen) -> Subscription<application::Message> {
      Subscription::from_recipe(subscription::IoEvents {
         mute_io_events: screen != Screen::Unlocking,
      })
      .map(application::Message::Unlocking)
   }
}
