/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

use iced::{alignment::Horizontal, widget::Text, Font, Length, Renderer};

use crate::theme::FastOtpTheme;

pub const ICONS_BYTES: &[u8] = include_bytes!("../assets/fastotp-operations.ttf").as_slice();

type ThemedText = Text<'static, Renderer<FastOtpTheme>>;

/// Instantiate a new icon component. Defaults to parent's style.
fn icon(unicode: char) -> ThemedText {
   Text::new(unicode.to_string())
      .font(Font::with_name("fastotp-operations"))
      .width(Length::Fixed(20.0))
      .horizontal_alignment(Horizontal::Center)
      .size(20)
}

#[inline]
pub fn edit() -> ThemedText {
   icon('\u{e905}')
}

#[inline]
pub fn copy() -> ThemedText {
   icon('\u{e92c}')
}

#[inline]
pub fn settings() -> ThemedText {
   icon('\u{e994}')
}

#[inline]
pub fn delete() -> ThemedText {
   icon('\u{e9ac}')
}

#[inline]
pub fn visible() -> ThemedText {
   icon('\u{e9ce}')
}

#[inline]
pub fn hidden() -> ThemedText {
   icon('\u{e9d1}')
}
