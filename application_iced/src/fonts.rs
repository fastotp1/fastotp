/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

use iced::Font;
use iced_core::font::{Family, Weight};

pub const MULI_REGULAR_BYTES: &[u8] = include_bytes!("../assets/Muli-Regular.ttf").as_slice();
pub const MULI_SEMIBOLD_BYTES: &[u8] = include_bytes!("../assets/Muli-SemiBold.ttf").as_slice();
pub const MULI_BOLD_BYTES: &[u8] = include_bytes!("../assets/Muli-Bold.ttf").as_slice();

pub const MULI_REGULAR: Font = Font {
   family: Family::Name("Muli"),
   weight: Weight::Normal,
   ..Font::DEFAULT
};

pub const MULI_SEMIBOLD: Font = Font {
   weight: Weight::Semibold,
   ..MULI_REGULAR
};

pub const MULI_BOLD: Font = Font {
   weight: Weight::Bold,
   ..MULI_REGULAR
};
