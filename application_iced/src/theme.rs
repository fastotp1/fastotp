/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

use iced::{
   application,
   widget::{button, container, scrollable, svg, text, text_input},
};
use iced_core::{Background, BorderRadius, Color, Vector};
use iced_widget::{progress_bar, text_input::Appearance};

use crate::{gray_text_color, widget_styling_utils::ColorHSV};

/// Default theme for the application
#[derive(Default, Debug, Clone, Copy)]
pub struct FastOtpTheme;

impl application::StyleSheet for FastOtpTheme {
   type Style = Self;

   fn appearance(&self, _style: &Self::Style) -> application::Appearance {
      application::Appearance {
         background_color: Color::WHITE,
         text_color: Color::BLACK,
      }
   }
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq)]
pub enum Buttons {
   #[default]
   Cancel,
   ChangePassword,
   Create,
   Delete,
   OtpCode,
   PasswordVisibility,
   Unlock,
}

impl button::StyleSheet for FastOtpTheme {
   type Style = Buttons;

   fn active(&self, style: &Self::Style) -> button::Appearance {
      let base = button::Appearance {
         background: Some(Background::Color(Color::TRANSPARENT)),
         text_color: Color::BLACK,
         ..button::Appearance::default()
      };

      let non_icon_button = button::Appearance {
         border_radius: BorderRadius::from(3.0),
         text_color: Color::WHITE,
         shadow_offset: Vector::new(1.0, 1.0),
         ..base
      };

      match style {
         Self::Style::Cancel | Self::Style::ChangePassword | Self::Style::Create | Self::Style::Delete => {
            button::Appearance {
               background: Some(Background::Color(match style {
                  Self::Style::Cancel => Color::from_rgb8(0xA8, 0xA8, 0xA8),
                  Self::Style::ChangePassword => Color::from_rgb8(0x21, 0x96, 0xF3),
                  Self::Style::Create => Color::from_rgb8(0x15, 0x65, 0xC0),
                  Self::Style::Delete => Color::from_rgb8(0xC6, 0x28, 0x28),
                  Self::Style::OtpCode | Self::Style::PasswordVisibility | Self::Style::Unlock => unreachable!(),
               })),
               ..non_icon_button
            }
         }
         Self::Style::OtpCode | Self::Style::PasswordVisibility => base,
         Self::Style::Unlock => {
            button::Appearance {
               background: Some(Background::Color(Color::from_rgb8(0x21, 0x96, 0xF3))),
               ..non_icon_button
            }
         }
      }
   }

   fn hovered(&self, style: &Self::Style) -> button::Appearance {
      let active = self.active(style);

      let non_icon_button = button::Appearance {
         shadow_offset: active.shadow_offset + Vector::new(0.0, 1.0),
         ..active
      };

      match style {
         Self::Style::Cancel | Self::Style::ChangePassword | Self::Style::Create | Self::Style::Delete => {
            button::Appearance {
               background: Some(Background::Color(match style {
                  Self::Style::Cancel => Color::from_rgb8(0x87, 0x87, 0x87),
                  Self::Style::ChangePassword | Self::Style::Create => Color::from_rgb8(0x15, 0x65, 0xC0),
                  Self::Style::Delete => Color::from_rgb8(0xC6, 0x28, 0x28),
                  Self::Style::OtpCode | Self::Style::PasswordVisibility | Self::Style::Unlock => unreachable!(),
               })),
               ..non_icon_button
            }
         }
         Self::Style::OtpCode | Self::Style::PasswordVisibility => {
            button::Appearance {
               text_color: Color::from_rgb8(0x1F, 0x8C, 0xE0),
               ..active
            }
         }
         Self::Style::Unlock => {
            button::Appearance {
               background: Some(Background::Color(Color::from_rgb8(0x15, 0x65, 0xC0))),
               ..non_icon_button
            }
         }
      }
   }

   fn pressed(&self, style: &Self::Style) -> button::Appearance {
      let hovered = self.hovered(style);

      match style {
         Self::Style::Cancel | Self::Style::ChangePassword | Self::Style::Create | Self::Style::Delete => {
            button::Appearance {
               background: Some(Background::Color(match style {
                  Self::Style::Cancel => Color::from_rgb8(0x7A, 0x7A, 0x7A),
                  Self::Style::ChangePassword | Self::Style::Create => Color::from_rgb8(0x0D, 0x47, 0xA1),
                  Self::Style::Delete => Color::from_rgb8(0xB7, 0x1C, 0x1C),
                  Self::Style::OtpCode | Self::Style::PasswordVisibility | Self::Style::Unlock => {
                     unreachable!()
                  }
               })),
               ..hovered
            }
         }
         Self::Style::OtpCode | Self::Style::PasswordVisibility => {
            button::Appearance {
               text_color: Color::from_rgb8(0x0D, 0x43, 0x93),
               ..hovered
            }
         }
         Self::Style::Unlock => {
            button::Appearance {
               background: Some(Background::Color(Color::from_rgb8(0x0D, 0x47, 0xA1))),
               ..hovered
            }
         }
      }
   }
}

#[derive(Debug, Default, Clone, Copy, PartialEq)]
pub struct ProgressBar {
   pub percentage_of_h: f32,
}

impl progress_bar::StyleSheet for FastOtpTheme {
   type Style = ProgressBar;

   fn appearance(&self, style: &Self::Style) -> progress_bar::Appearance {
      let mut bar_color_hsv: ColorHSV = Color::from_rgb8(0x77, 0xC6, 0x0F).into();
      bar_color_hsv.h *= style.percentage_of_h;

      progress_bar::Appearance {
         background: Background::Color(Color::from_rgb8(0xCE, 0xCE, 0xCE)),
         bar: Background::Color(bar_color_hsv.into()),
         border_radius: BorderRadius::from(3.0),
      }
   }
}

#[derive(Debug, Default, Clone, Copy, PartialEq)]
pub enum Container {
   #[default]
   Default,
   Tile {
      selected_color_fade_out: f32,
   },
}

impl container::StyleSheet for FastOtpTheme {
   type Style = Container;

   fn appearance(&self, style: &Self::Style) -> container::Appearance {
      match style {
         Container::Default => container::Appearance::default(),
         &Container::Tile {
            selected_color_fade_out,
         } => {
            let background = if selected_color_fade_out > 0.0 {
               let mut color_hsv: ColorHSV = Color::from_rgb8(0xA5, 0xF0, 0x84).into();
               color_hsv.s *= 1.0 - selected_color_fade_out;
               color_hsv.into()
            }
            else {
               Color::from_rgb8(0xF2, 0xF2, 0xF2)
            };

            container::Appearance {
               background: Some(Background::Color(background)),
               border_color: Color::from_rgb8(0xE0, 0xE0, 0xE0),
               border_width: 2.0,
               border_radius: BorderRadius::from(5.0),
               ..container::Appearance::default()
            }
         }
      }
   }
}

#[derive(Debug, Default, Clone, Copy)]
pub enum Text {
   #[default]
   Default,
   Color(Color),
}

impl From<Color> for Text {
   fn from(color: Color) -> Self {
      Self::Color(color)
   }
}

impl text::StyleSheet for FastOtpTheme {
   type Style = Text;

   fn appearance(&self, style: Self::Style) -> text::Appearance {
      match style {
         Text::Default => text::Appearance::default(),
         Text::Color(color) => text::Appearance { color: Some(color) },
      }
   }
}

#[derive(Debug, Default, Clone, Copy)]
pub struct Scrollable;

macro_rules! scrollable_color {
   ($alpha_value:literal) => {
      Color::from_rgba8(0x66, 0x66, 0x66, $alpha_value)
   };
}

impl scrollable::StyleSheet for FastOtpTheme {
   type Style = Scrollable;

   fn active(&self, _style: &Self::Style) -> scrollable::Scrollbar {
      let border_radius = BorderRadius::from(3.0);

      scrollable::Scrollbar {
         background: Some(Background::Color(scrollable_color!(0.4))),
         border_radius,
         border_width: 0.0,
         border_color: Color::TRANSPARENT,
         scroller: scrollable::Scroller {
            color: scrollable_color!(0.5),
            border_radius,
            border_width: 0.5,
            border_color: Color::BLACK,
         },
      }
   }

   fn hovered(&self, style: &Self::Style, _is_mouse_over_scrollbar: bool) -> scrollable::Scrollbar {
      let active = self.active(style);

      scrollable::Scrollbar {
         scroller: scrollable::Scroller {
            color: scrollable_color!(0.75),
            ..active.scroller
         },
         ..active
      }
   }
}

impl text_input::StyleSheet for FastOtpTheme {
   type Style = ();

   fn active(&self, _style: &Self::Style) -> Appearance {
      Appearance {
         background: Background::Color(Color::WHITE),
         border_radius: BorderRadius::from(3.0),
         border_width: 1.1,
         border_color: gray_text_color!(),
         icon_color: Color::default(),
      }
   }

   fn focused(&self, style: &Self::Style) -> Appearance {
      let active = self.active(style);

      Appearance {
         border_width: 1.6,
         border_color: Color::BLACK,
         ..active
      }
   }

   fn placeholder_color(&self, _style: &Self::Style) -> Color {
      Color::from_rgb8(0xA5, 0xA5, 0xA5)
   }

   fn value_color(&self, _style: &Self::Style) -> Color {
      Color::BLACK
   }

   fn disabled_color(&self, style: &Self::Style) -> Color {
      self.placeholder_color(style)
   }

   fn selection_color(&self, _style: &Self::Style) -> Color {
      Color::from_rgb8(0xD8, 0xD8, 0xD8)
   }

   fn hovered(&self, style: &Self::Style) -> Appearance {
      let active = self.active(style);

      Appearance {
         border_width: 1.3,
         border_color: Color::from_rgb8(0x4C, 0x4C, 0x4C),
         ..active
      }
   }

   fn disabled(&self, style: &Self::Style) -> Appearance {
      self.active(style)
   }
}

impl svg::StyleSheet for FastOtpTheme {
   type Style = ();

   fn appearance(&self, _style: &Self::Style) -> svg::Appearance {
      svg::Appearance { color: None }
   }
}
