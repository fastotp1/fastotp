/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! The [Iced](https://github.com/hecrj/iced) version of the application

#![allow(
   clippy::doc_markdown,
   clippy::items_after_statements,
   clippy::missing_fields_in_debug,
   clippy::missing_panics_doc,
   clippy::suspicious_else_formatting
)]
#![deny(
   array_into_iter,
   clippy::rest_pat_in_fully_bound_structs,
   clippy::separated_literal_suffix,
   missing_copy_implementations,
   missing_debug_implementations,
   missing_docs,
   non_camel_case_types,
   private_in_public,
   rust_2018_idioms,
   trivial_casts,
   trivial_numeric_casts,
   type_alias_bounds,
   unknown_lints,
   unsafe_code,
   unused,
   unused_import_braces,
   while_true
)]
#![feature(async_closure, trait_alias, type_alias_impl_trait, panic_info_message)]
#![windows_subsystem = "windows"] // To hide the console window in Windows

mod application;
mod create_entry;
mod fonts;
mod icons;
mod logos;
mod otp_display;
mod settings;
mod theme;
mod unlocking;
mod widget_styling_utils;

#[cfg(not(debug_assertions))]
use std::{
   io::Write,
   panic::{self, PanicInfo},
};

use application::FastOtpIcedGUI;
use iced::{
   window,
   window::{icon, Icon},
   Application,
   Settings,
};

fn main() {
   #[cfg(not(debug_assertions))]
   install_panic_hook();
   #[cfg(feature = "logging")]
   init_logging();

   FastOtpIcedGUI::run(Settings {
      window: window::Settings {
         size: (1160, 540),
         position: window::Position::Centered,
         icon: window_icon(),
         ..window::Settings::default()
      },
      antialiasing: true,
      ..Settings::default()
   })
   .expect("Failure to start application");
}

#[cfg(not(debug_assertions))]
fn install_panic_hook() {
   panic::set_hook(Box::new(move |panic_info: &'_ PanicInfo<'_>| {
      use std::{backtrace::Backtrace, fs::OpenOptions};

      let timestamp = chrono::Local::now().format("[%Y-%m-%d %H:%M:%S]");
      let panic_msg = panic_info
         .message()
         .map(move |m| format!("{}", m))
         .unwrap_or_else(|| String::from(""));
      let bt = Backtrace::force_capture();

      let file_path = {
         let mut file_path = std::env::current_dir().unwrap();
         file_path.push("fastotp_panics.log");
         file_path
      };

      let mut file = OpenOptions::new()
         .write(true)
         .create(true)
         .append(true)
         .open(file_path)
         .unwrap();

      file.write(format!("{timestamp}:\nError:\n{panic_msg}\nBacktrace:\n{bt:#?}\n\n\n").as_bytes());
   }));
}

#[cfg(feature = "logging")]
fn init_logging() {
   let file_path = {
      let mut file_path = std::env::current_dir().unwrap();
      file_path.push("fastotp.log");
      file_path
   };

   fern::Dispatch::new()
      .format(|out, message, record| {
         out.finish(format_args!(
            "{}[{}][{}] {}",
            chrono::Local::now().format("[%Y-%m-%d %H:%M:%S]"),
            record.level(),
            record.target(),
            message
         ))
      })
      .level(log::LevelFilter::Error)
      .chain(fern::log_file(file_path).unwrap())
      .apply()
      .expect("Failure to instantiate logger");
}

fn window_icon() -> Option<Icon> {
   let raw_image = image::load_from_memory(include_bytes!("../assets/window_icon.png")).ok()?;
   let loaded_image = raw_image.as_rgba8().map(|rgba_image| {
      (
         rgba_image.width(),
         rgba_image.height(),
         rgba_image.pixels().fold(Vec::<u8>::new(), |mut pixels, next| {
            pixels.extend_from_slice(&next.0);
            pixels
         }),
      )
   })?;

   icon::from_rgba(loaded_image.2, loaded_image.0, loaded_image.1).ok()
}
