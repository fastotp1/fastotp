/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! Collection of utilities for styling widgets

use std::borrow::Cow;

use iced::{
   alignment::Horizontal,
   widget::{Button, Text},
   Color,
   Renderer,
};
use iced_core::Length;
use iced_widget::TextInput;

use crate::{fonts, theme::FastOtpTheme};

/// Utility to add an `is_enabled` method to a [Button]
pub trait ButtonExt<Cmd>: Sized {
   fn enable(self, enabled: bool, c: Cmd) -> Self;
}

impl<'state, Cmd: Clone> ButtonExt<Cmd> for Button<'state, Cmd, Renderer<FastOtpTheme>> {
   #[inline]
   fn enable(self, enabled: bool, c: Cmd) -> Self {
      if enabled {
         return self.on_press(c);
      }
      self
   }
}

#[derive(Debug, Clone, Copy, PartialEq)]
#[allow(missing_docs)]
pub struct ColorHSV {
   pub h: f32,
   pub s: f32,
   pub v: f32,
   pub a: f32,
}

impl From<Color> for ColorHSV {
   #[allow(
      clippy::many_single_char_names,
      clippy::float_arithmetic,
      clippy::cast_sign_loss,
      clippy::cast_possible_truncation,
      clippy::modulo_arithmetic,
      clippy::expect_used
   )]
   fn from(Color { r, g, b, a }: Color) -> Self {
      let v = *[r, g, b]
         .iter()
         .max_by(|a, b| a.partial_cmp(b).expect("impossible"))
         .expect("impossible");
      let c_min = *[r, g, b]
         .iter()
         .min_by(|a, b| a.partial_cmp(b).expect("impossible"))
         .expect("impossible");

      let chroma = v - c_min;
      let h = if chroma == 0.0 {
         0.0
      }
      else if (v - r).abs() < f32::EPSILON {
         ((g - b) / chroma) % 6.0
      }
      else if (v - g).abs() < f32::EPSILON {
         ((b - r) / chroma) + 2.0
      }
      else {
         ((r - g) / chroma) + 4.0
      };

      let h = h * 60.0;

      let s = if v == 0.0 { chroma } else { chroma / v };

      Self { h, s, v, a }
   }
}

impl From<ColorHSV> for Color {
   #[allow(
      clippy::many_single_char_names,
      clippy::float_arithmetic,
      clippy::cast_sign_loss,
      clippy::cast_possible_truncation,
      clippy::modulo_arithmetic
   )]
   fn from(ColorHSV { h, s, v, a }: ColorHSV) -> Self {
      let h = h / 60.0;
      let c = v * s;
      let x = c * (1.0 - f32::abs((h % 2.0) - 1.0));

      let h = h as u32;
      let (r, g, b) = if h < 1 {
         (c, x, 0.0)
      }
      else if (1..2).contains(&h) {
         (x, c, 0.0)
      }
      else if (2..3).contains(&h) {
         (0.0, c, x)
      }
      else if (3..4).contains(&h) {
         (0.0, x, c)
      }
      else if (4..5).contains(&h) {
         (x, 0.0, c)
      }
      else if (5..6).contains(&h) {
         (c, 0.0, x)
      }
      else {
         (0.0, 0.0, 0.0)
      };

      let m = v - c;

      let mut color = Self::from_rgb(r + m, g + m, b + m);
      color.a = a;
      color
   }
}

/// Helper trait that permits us to chain builder type operations more easily
pub trait And: Sized {
   #[inline]
   fn and<F: FnOnce(Self) -> Self>(self, f: F) -> Self {
      f(self)
   }
}

impl<T> And for T {}

/// Define color of a gray text
#[macro_export]
macro_rules! gray_text_color {
   () => {
      iced::Color::from_rgba8(0x7F, 0x7F, 0x7F, 1.0)
   };
}

pub const BUTTONS_TEXT_SIZE: u16 = 22;

/// Creates a standard title widget
pub fn title_text<'content>(label: impl Into<Cow<'content, str>>) -> Text<'content, Renderer<FastOtpTheme>> {
   Text::new(label)
      .width(Length::Fill)
      .font(fonts::MULI_BOLD)
      .size(45)
      .style(Color::from_rgb8(0x72, 0x72, 0x72))
      .horizontal_alignment(Horizontal::Center)
}

/// Creates a standard input field widget
pub fn input_field<'a, Message: Clone>(
   placeholder: &str,
   value: &str,
) -> TextInput<'a, Message, Renderer<FastOtpTheme>> {
   TextInput::new(placeholder, value)
      .font(fonts::MULI_REGULAR)
      .padding(10)
      .size(20)
}

/// Creates a standard information message widget
pub fn info_message_text<'content>(
   label: impl Into<Cow<'content, str>>,
) -> Text<'content, Renderer<FastOtpTheme>> {
   Text::new(label)
      .font(fonts::MULI_REGULAR)
      .width(Length::Fill)
      .size(25)
      .style(gray_text_color!())
      .horizontal_alignment(Horizontal::Center)
}
