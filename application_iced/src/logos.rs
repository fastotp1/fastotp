/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

use std::{
   cmp::Ordering,
   collections::{BinaryHeap, HashMap},
};

use domain::entities::entry::{self, Entries, Entry};
use fuzzy_matcher::{skim::SkimMatcherV2, FuzzyMatcher};
use rust_embed::RustEmbed;

/// Stores in memory all the logos for the issuers
#[derive(RustEmbed)]
#[folder = "logos/"]
pub struct Logos;

/// Generates a map from entries which assigns the closest logo to an entry id.
///
/// To select the logos, the issuer and the description are taken into account to determine proximity
/// to the logos' names.
pub fn match_logos(entries: &Entries) -> HashMap<entry::Id, String> {
   let mut cache = HashMap::new();
   let fuzzy_matcher = SkimMatcherV2::default().ignore_case();

   for entry in &entries.values {
      let path = closest_logo_filename(&fuzzy_matcher, entry);

      if let Some(v) = path {
         cache.insert(entry.id, v);
      }
   }

   cache
}

/// Assigns the closest logo to an entry id.
///
/// To select the logo, the issuer and the description are taken into account to determine proximity
/// the logos' names.
pub fn matching_logo(otp: &Entry) -> Option<String> {
   let fuzzy_matcher = SkimMatcherV2::default().ignore_case();

   closest_logo_filename(&fuzzy_matcher, otp)
}

fn closest_logo_filename(fuzzy_matcher: &SkimMatcherV2, otp: &Entry) -> Option<String> {
   let logos_paths = Logos::iter();
   let mut matches: BinaryHeap<ScoredItem<'_>> = logos_paths
      .filter_map(|filename| {
         let mut score = fuzzy_matcher.fuzzy_match(
            &filename,
            &format!("{}{}", &otp.issuer, &otp.description).replace(' ', ""),
         );

         if score.is_none() {
            score = fuzzy_matcher.fuzzy_match(&filename, &otp.issuer.replace(' ', ""));
         }

         score.map(|s| {
            ScoredItem {
               score: s,
               value: filename,
            }
         })
      })
      .collect();

   let mut filtered_and_ranked = Vec::with_capacity(matches.len());
   while let Some(choice) = matches.pop() {
      filtered_and_ranked.push(choice.value);
   }

   filtered_and_ranked.first().cloned().map(|s| s.to_string())
}

#[derive(Debug, Eq)]
struct ScoredItem<'value> {
   score: i64,
   value: std::borrow::Cow<'value, str>,
}

impl<'value> PartialEq for ScoredItem<'value> {
   fn eq(&self, other: &Self) -> bool {
      self.score == other.score
   }
}

impl<'value> PartialOrd for ScoredItem<'value> {
   fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
      self.score.partial_cmp(&other.score).and_then(|o| {
         match o {
            Ordering::Equal => {
               // If the scores are the same, prefer items whose value has the shortest length
               // This means that it contains the same information but with less data.
               self.value.len().partial_cmp(&other.value.len()).map(|o2| {
                  match o2 {
                     Ordering::Less => Ordering::Greater,
                     Ordering::Greater => Ordering::Less,
                     Ordering::Equal => o2,
                  }
               })
            }
            Ordering::Less | Ordering::Greater => Some(o),
         }
      })
   }
}

impl<'value> Ord for ScoredItem<'value> {
   fn cmp(&self, other: &Self) -> Ordering {
      self.score.cmp(&other.score)
   }
}
