/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! A self contained module for the entire entry creation, edition, and deletion functionality, including GUI rendering using
//! [Iced](https://github.com/hecrj/iced)

mod subscription;
mod view;

use std::sync::{Arc, RwLock};

use futures::FutureExt;
use iced::{widget, Element, Renderer, Subscription};
use presentation::{
   create_entry::{Command, EntryCreation, State},
   Navigator,
   Screen,
};
pub use view::ISSUER_ID as FIRST_FOCUS_ID;

use crate::{application, application::SharedLogosPaths, create_entry::view::View, theme::FastOtpTheme};

/// Commands solely for the GUI to interpret. They do not alter the application state, only view state
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum ViewCommand {
   HidePasswords,
   ShowPasswords,
   PreviousField,
   NextField,
}

/// Permits differentiate between commands that modify GUI-only state vs underlying application state
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum Message {
   Model(Command),
   View(ViewCommand),
}

impl Navigator for Message {
   fn is_navigation(&self) -> Option<Screen> {
      match *self {
         Self::Model(ref presentation_cmd) => presentation_cmd.is_navigation(),
         Self::View(_) => None,
      }
   }
}

/// Represents the complete functionality for managing the lifecycle of an entry in the application
pub struct Module {
   presenter: Arc<RwLock<EntryCreation>>,
   view: View,
   issuers_logos: SharedLogosPaths,
}

impl Module {
   pub fn new(presenter: EntryCreation, issuers_logos: SharedLogosPaths) -> Self {
      Self {
         presenter: Arc::new(RwLock::new(presenter)),
         view: View::new(),
         issuers_logos,
      }
   }

   pub fn update(&mut self, msg: Message) -> iced::Command<application::Message> {
      match msg {
         Message::Model(cmd) => {
            let reset_hide_password = self.reset_password_visibility(&cmd);

            let new_cmd = self.presenter.write().unwrap().interpret(cmd);

            let command = match new_cmd {
               None => iced::Command::none(),
               Some(future) => {
                  let issuers_logos = Arc::clone(&self.issuers_logos);
                  let f = future.map(move |c| {
                     // If an entry was modified, make sure the logo matches
                     if let Command::CreatedOrUpdated(Ok(ref entry)) = c {
                        let matching_logo = crate::logos::matching_logo(entry);
                        if let Some(logo_path) = matching_logo {
                           issuers_logos.write().unwrap().insert(entry.id, logo_path);
                        }
                     }

                     c
                  });

                  iced::Command::perform(f, Message::Model)
               }
            };

            iced::Command::batch([reset_hide_password, command]).map(application::Message::CreateEntry)
         }
         Message::View(view_cmd) => {
            match view_cmd {
               ViewCommand::PreviousField => widget::focus_previous(),
               ViewCommand::NextField => widget::focus_next(),
               ViewCommand::HidePasswords | ViewCommand::ShowPasswords => {
                  self.view.update(view_cmd).map(application::Message::CreateEntry)
               }
            }
         }
      }
   }

   fn reset_password_visibility(&mut self, cmd: &Command) -> iced_runtime::Command<Message> {
      let is_not_deleting = self.presenter.read().unwrap().view_model().state != State::AwaitingDeleteConfirmation;

      match cmd {
         Command::Cancel if is_not_deleting => {
            iced::Command::perform(async move { ViewCommand::HidePasswords }, Message::View)
         }
         Command::SetIssuer(_)
         | Command::SetDescription(_)
         | Command::SetSecret(_)
         | Command::CreateOrUpdate
         | Command::CreatedOrUpdated(_)
         | Command::Cancel
         | Command::Load(_)
         | Command::Edit(_)
         | Command::AwaitDeleteConfirmation
         | Command::Delete
         | Command::Deleted(_)
         | Command::GoTo(_) => iced::Command::none(),
      }
   }

   pub fn view(&self) -> Element<'_, application::Message, Renderer<FastOtpTheme>> {
      let presenter = self.presenter.read().unwrap();

      self
         .view
         .render(presenter.view_model())
         .map(application::Message::CreateEntry)
   }

   pub fn subscription(&self, screen: Screen) -> Subscription<application::Message> {
      Subscription::from_recipe(subscription::IoEvents {
         mute_io_events: screen != Screen::CreateEntry,
      })
      .map(application::Message::CreateEntry)
   }
}
