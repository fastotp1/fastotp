/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! GUI rendering of the view for the entry creation, edition, and deletion functionality

use iced::{
   widget::{text_input, Button, Column, Row, Text},
   Alignment,
   Color,
   Element,
   Length,
   Renderer,
};
use once_cell::sync::Lazy;
use presentation::create_entry::{Command, State, ViewModel};

use crate::{
   create_entry::{Message, ViewCommand},
   fonts,
   icons,
   theme,
   theme::FastOtpTheme,
   widget_styling_utils,
   widget_styling_utils::{And, ButtonExt},
};

pub static ISSUER_ID: Lazy<text_input::Id> = Lazy::new(text_input::Id::unique);
static DESCRIPTION_ID: Lazy<text_input::Id> = Lazy::new(text_input::Id::unique);
static SECRET_ID: Lazy<text_input::Id> = Lazy::new(text_input::Id::unique);

/// Visual state for the create entry module
#[derive(Debug, Clone)]
pub(super) struct View {
   passwords_visible: bool,
}

impl View {
   pub fn new() -> Self {
      Self {
         passwords_visible: false,
      }
   }

   pub fn update(&mut self, view_cmd: ViewCommand) -> iced::Command<Message> {
      self.passwords_visible = matches!(view_cmd, ViewCommand::ShowPasswords);

      iced::Command::none()
   }

   pub fn render(&self, view_model: ViewModel) -> Element<'_, Message, Renderer<FastOtpTheme>> {
      let title = widget_styling_utils::title_text(view_model.labels.title);

      let issuer = widget_styling_utils::input_field(view_model.labels.issuer.as_str(), &view_model.issuer)
         .id(ISSUER_ID.clone())
         .on_input(|text| Message::Model(Command::SetIssuer(text)));

      let description =
         widget_styling_utils::input_field(view_model.labels.description.as_str(), &view_model.description)
            .id(DESCRIPTION_ID.clone())
            .on_input(|text| Message::Model(Command::SetDescription(text)));

      let show_secret = self.passwords_visible;
      let secret = widget_styling_utils::input_field(view_model.labels.secret.as_str(), &view_model.secret)
         .id(SECRET_ID.clone())
         .and(|mut txt| {
            if !show_secret {
               txt = txt.password();
            }
            txt
         })
         .on_input(|text| Message::Model(Command::SetSecret(text)));

      let (visibility_icon, visibility_cmd) = if show_secret {
         (icons::visible(), ViewCommand::HidePasswords)
      }
      else {
         (icons::hidden(), ViewCommand::ShowPasswords)
      };

      let secret_with_hiding = Row::new()
         .align_items(Alignment::Center)
         .spacing(10)
         .push(secret.width(Length::FillPortion(28)))
         .push(
            Button::new(visibility_icon.size(widget_styling_utils::BUTTONS_TEXT_SIZE))
               .padding(0)
               .width(Length::Fixed(f32::from(widget_styling_utils::BUTTONS_TEXT_SIZE)))
               .on_press(Message::View(visibility_cmd))
               .style(theme::Buttons::PasswordVisibility)
               .width(Length::FillPortion(1)),
         );

      let mut buttons_row = Row::new().spacing(20).align_items(Alignment::Center);

      if let Some(delete_labels) = view_model.labels.delete.as_ref() {
         let (delete_lbl, cmd) = if view_model.state == State::AwaitingDeleteConfirmation {
            (delete_labels.ask_delete.clone(), Command::Delete)
         }
         else {
            (delete_labels.delete.clone(), Command::AwaitDeleteConfirmation)
         };

         buttons_row = buttons_row.push(
            Button::new(
               Row::new()
                  .spacing(10)
                  .push(
                     Text::new(delete_lbl)
                        .font(fonts::MULI_SEMIBOLD)
                        .size(widget_styling_utils::BUTTONS_TEXT_SIZE),
                  )
                  .push(icons::delete().style(Color::WHITE)),
            )
            .enable(view_model.state.isnt_processing(), Message::Model(cmd))
            .style(theme::Buttons::Delete)
            .padding(10),
         );
      }

      buttons_row = buttons_row
         .push(
            Button::new(
               Text::new(view_model.labels.cancel)
                  .font(fonts::MULI_SEMIBOLD)
                  .size(widget_styling_utils::BUTTONS_TEXT_SIZE),
            )
            .enable(view_model.state.isnt_processing(), Message::Model(Command::Cancel))
            .style(theme::Buttons::Cancel)
            .padding(10),
         )
         .push(
            Button::new(
               Text::new(view_model.labels.create_edit)
                  .font(fonts::MULI_SEMIBOLD)
                  .size(widget_styling_utils::BUTTONS_TEXT_SIZE),
            )
            .enable(view_model.state.can_create(), Message::Model(Command::CreateOrUpdate))
            .padding(10)
            .style(theme::Buttons::Create),
         );

      let state = view_model.state;
      let delete_lbl = view_model.labels.delete;

      let buttons_and_msg = Column::new()
         .align_items(Alignment::Center)
         .spacing(15)
         .push(buttons_row)
         .and(|col| {
            if let State::ProcessingFailed(error_msg) = state {
               let informative_message = widget_styling_utils::info_message_text(error_msg);

               col.push(informative_message)
            }
            else if State::AwaitingDeleteConfirmation == state {
               let informative_message =
                  widget_styling_utils::info_message_text(delete_lbl.unwrap().confirm_delete);

               col.push(informative_message)
            }
            else {
               col
            }
         })
         .push(widget_styling_utils::info_message_text(view_model.labels.shortcuts));

      let mut buttons_and_qr_code = Row::new().align_items(Alignment::Start).spacing(0);

      if !view_model.qr_code.is_empty() && show_secret {
         use iced_core::svg::Handle;
         use iced_widget::Svg;

         const QR_CODE_SIZE: f32 = 400.0;

         buttons_and_qr_code = buttons_and_qr_code.push(
            Svg::new(Handle::from_memory(view_model.qr_code))
               .width(Length::Fixed(QR_CODE_SIZE))
               .height(Length::Fixed(QR_CODE_SIZE)),
         );
      }

      buttons_and_qr_code = buttons_and_qr_code.push(buttons_and_msg);

      Column::new()
         .align_items(Alignment::Center)
         .spacing(15)
         .push(title)
         .push(issuer)
         .push(description)
         .push(secret_with_hiding)
         .push(buttons_and_qr_code)
         .into()
   }
}
