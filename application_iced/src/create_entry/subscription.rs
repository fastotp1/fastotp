/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! See [IoEvents]

use futures::stream::BoxStream;
use iced_core::{event::Status, Event as InputEvent, Hasher};
use iced_runtime::futures::subscription::Recipe;
use presentation::create_entry::Command;

use crate::create_entry::{Message, ViewCommand};

/// Controls the consumption of IO events (mouse and keyboard) of the create entry screen functionality
pub struct IoEvents {
   pub mute_io_events: bool,
}

impl Recipe for IoEvents {
   type Output = Message;

   fn hash(&self, state: &mut Hasher) {
      use std::hash::Hash;

      self.mute_io_events.hash(state);
      "CreateEntry.CommandsSubscription".hash(state);
   }

   fn stream(self: Box<Self>, input: BoxStream<'_, (InputEvent, Status)>) -> BoxStream<'_, Self::Output> {
      use futures::StreamExt;
      use iced_core::keyboard::{Event, KeyCode, Modifiers};

      if self.mute_io_events {
         return futures::stream::empty().boxed();
      }

      let input_as_commands = input.filter_map(move |(io_event, _)| {
         const NO_MODIFIER: Modifiers = Modifiers::empty();

         let cmd = if let InputEvent::Keyboard(Event::KeyReleased {
            key_code,
            modifiers: active_modifiers @ (NO_MODIFIER | Modifiers::CTRL | Modifiers::SHIFT),
         }) = io_event
         {
            if key_code == KeyCode::Escape && active_modifiers.is_empty() {
               Some(Message::Model(Command::Cancel))
            }
            else if key_code == KeyCode::Enter {
               if active_modifiers.control() {
                  Some(Message::Model(Command::Delete))
               }
               else {
                  Some(Message::Model(Command::CreateOrUpdate))
               }
            }
            else if key_code == KeyCode::D && active_modifiers.control() {
               Some(Message::Model(Command::AwaitDeleteConfirmation))
            }
            else if key_code == KeyCode::Tab {
               let view_cmd = if active_modifiers.shift() {
                  ViewCommand::PreviousField
               }
               else {
                  ViewCommand::NextField
               };

               Some(Message::View(view_cmd))
            }
            else {
               None
            }
         }
         else {
            None
         };

         async move { cmd }
      });

      input_as_commands.boxed()
   }
}
