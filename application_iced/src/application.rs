/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! Application frontend implementation using [Iced](https://github.com/hecrj/iced)

use std::{
   collections::HashMap,
   sync::{Arc, RwLock},
};

use domain::{
   entities::entry,
   use_cases::{
      change_password::ChangePassword as ChangePasswordUseCase,
      create_entry::EntryCreation as EntryCreationUseCase,
      delete_entry::EntryDeletion,
      otp_generation::OtpGeneration,
      qr_generation::QrGeneration,
      unlocking::{unlocking::Unlocking as UnlockingUseCase, Argon2dHasher},
      SharedRepository,
      SharedStorage,
   },
};
use iced::{
   alignment::{Horizontal, Vertical},
   font,
   widget::{text_input, Container},
   Application,
   Command,
   Element,
   Length,
   Renderer,
   Subscription,
};
use persistence::{encrypted_data_serde_capnp::EncryptedDataCapnp, entries_serde_capnp::EntriesCapnp};
use presentation::{
   change_password::{ChangePassword, Command as ChangePasswordCommand},
   create_entry::{Command as CreateEntryCommand, EntryCreation},
   otp_display::{Action as OtpDisplayAction, Command as OtpDisplayCommand, OtpDisplay},
   unlocking::{Action as UnlockingAction, Command as UnlockingCommand, Unlocking},
   Navigator,
   Screen,
};

use crate::{
   create_entry,
   fonts,
   icons,
   otp_display,
   settings,
   settings::Messages,
   theme::FastOtpTheme,
   unlocking,
};

#[derive(Clone, Debug)]
pub enum Message {
   FontLoaded(Result<(), font::Error>),
   Unlocking(unlocking::Message),
   LogoLoading(SharedRepository, SharedLogosPaths, OtpDisplayCommand),
   OtpDisplay(otp_display::Message),
   CreateEntry(create_entry::Message),
   Settings(Messages),
}

impl Navigator for Message {
   fn is_navigation(&self) -> Option<Screen> {
      match *self {
         Self::FontLoaded(_) => None,
         Self::Unlocking(ref cmd) => cmd.is_navigation(),
         Self::LogoLoading(_, _, ref cmd) => cmd.is_navigation(),
         Self::OtpDisplay(ref cmd) => cmd.is_navigation(),
         Self::CreateEntry(ref cmd) => cmd.is_navigation(),
         Self::Settings(ref cmd) => cmd.is_navigation(),
      }
   }
}

/*
   We can safely ignore this lint since we will only ever have one instance of the enum for the lifetime
   of the application, plus the fact that the smallest variant is only used before the biggest portion of the
   application runs.
*/
#[allow(clippy::large_enum_variant)]
/// The possible global states of the application. Either locked or unlocked. Used to enforce that
/// no access to the rest of the application is even possible without first unlocking it first.
enum AppState {
   Locked {
      unlocking: unlocking::Module<async_std::fs::File, Argon2dHasher>,
   },
   Unlocked {
      create_entry: create_entry::Module,
      otp_display: otp_display::Module,
      settings: settings::Module,
   },
}

pub struct FastOtpIcedGUI {
   current_screen: Screen,
   state: AppState,
}

impl Application for FastOtpIcedGUI {
   type Executor = iced::executor::Default;
   type Flags = ();
   type Message = Message;
   type Theme = FastOtpTheme;

   fn new(_flags: ()) -> (Self, Command<Message>) {
      let file_res = open_entries_file().expect("Failed to open file:");

      let unlocking_presenter = Unlocking::new(
         UnlockingUseCase::new(file_res.file.into(), Argon2dHasher),
         file_res.is_new,
      );

      (
         Self {
            current_screen: Screen::Unlocking,
            state: AppState::Locked {
               unlocking: unlocking::Module::new(unlocking_presenter),
            },
         },
         Command::batch([
            font::load(icons::ICONS_BYTES),
            font::load(fonts::MULI_REGULAR_BYTES),
            font::load(fonts::MULI_SEMIBOLD_BYTES),
            font::load(fonts::MULI_BOLD_BYTES),
         ])
         .map(Message::FontLoaded),
      )
   }

   fn title(&self) -> String {
      String::from("FastOTP")
   }

   fn update(&mut self, message: Self::Message) -> Command<Message> {
      let final_message = if let Some(screen) = message.is_navigation() {
         self.current_screen = screen;

         match message {
            Message::Unlocking(unlocking::Message::Model(UnlockingCommand {
               action: Some(UnlockingAction::RepositoryStorage((repository, storage))),
               ..
            })) => {
               let shared_repo = Arc::clone(&repository);
               let logos_paths_shared = self.unlocked_initialization(repository, storage);

               Message::LogoLoading(
                  shared_repo,
                  logos_paths_shared,
                  OtpDisplayCommand {
                     action: Some(OtpDisplayAction::FilterBy(String::new())),
                     go_to: None,
                  },
               )
            }
            Message::OtpDisplay(otp_display::Message::Model(OtpDisplayCommand {
               action: Some(OtpDisplayAction::EditEntry(ref id)),
               ..
            })) => Message::CreateEntry(create_entry::Message::Model(CreateEntryCommand::Load(*id))),
            Message::FontLoaded(_)
            | Message::Unlocking(_)
            | Message::LogoLoading(..)
            | Message::OtpDisplay(_)
            | Message::CreateEntry(_)
            | Message::Settings(_) => message,
         }
      }
      else {
         message
      };

      match self.state {
         AppState::Locked { ref mut unlocking } => {
            match final_message {
               Message::FontLoaded(_) => text_input::focus(unlocking::FIRST_FOCUS_ID.clone()),
               Message::Unlocking(msg) => unlocking.update::<EncryptedDataCapnp, EntriesCapnp>(msg),
               Message::LogoLoading(..)
               | Message::OtpDisplay(_)
               | Message::CreateEntry(_)
               | Message::Settings(_) => Command::none(),
            }
         }
         AppState::Unlocked {
            ref mut create_entry,
            ref mut otp_display,
            ref mut settings,
         } => {
            match final_message {
               Message::FontLoaded(_) | Message::Unlocking(_) => Command::none(),
               Message::LogoLoading(repository, logos_paths_shared, otp_display_cmd) => {
                  use domain::entities::entry::Entries;
                  use futures::FutureExt;

                  let logos_paths_cache = async move || {
                     let entries = repository
                        .read()
                        .await
                        .all()
                        .await
                        .unwrap_or_else(|_| Entries::default());

                     crate::logos::match_logos(&entries)
                  };
                  let logos_paths_cache = logos_paths_cache().map(move |matched_paths| {
                     logos_paths_shared.write().unwrap().extend(matched_paths);
                  });

                  let initial_focus_cmd = text_input::focus(otp_display::FIRST_FOCUS_ID.clone());

                  let show_otps_cmd = Command::perform(logos_paths_cache.boxed(), move |_| {
                     Message::OtpDisplay(otp_display::Message::Model(otp_display_cmd))
                  });

                  Command::batch([initial_focus_cmd, show_otps_cmd])
               }
               Message::OtpDisplay(msg) => {
                  let focus_create_entry_first_field = matches!(
                     msg,
                     otp_display::Message::Model(OtpDisplayCommand {
                        action: None,
                        go_to: Some(Screen::CreateEntry),
                     })
                  );

                  let focus_settings_first_field = matches!(
                     msg,
                     otp_display::Message::Model(OtpDisplayCommand {
                        action: None,
                        go_to: Some(Screen::ChangePassword),
                     })
                  );

                  let update_cmd = otp_display.update(msg);

                  if focus_create_entry_first_field {
                     let initial_focus_cmd = text_input::focus(create_entry::FIRST_FOCUS_ID.clone());

                     Command::batch([initial_focus_cmd, update_cmd])
                  }
                  else if focus_settings_first_field {
                     let initial_focus_cmd = text_input::focus(settings::FIRST_FOCUS_ID.clone());

                     Command::batch([initial_focus_cmd, update_cmd])
                  }
                  else {
                     update_cmd
                  }
               }
               Message::CreateEntry(msg) => {
                  let focus_first_field = matches!(
                     msg,
                     create_entry::Message::Model(CreateEntryCommand::GoTo(Screen::OtpDisplay))
                  );

                  let update_cmd = create_entry.update(msg);

                  if focus_first_field {
                     let initial_focus_cmd = text_input::focus(otp_display::FIRST_FOCUS_ID.clone());

                     Command::batch([initial_focus_cmd, update_cmd])
                  }
                  else {
                     update_cmd
                  }
               }
               Message::Settings(msg) => {
                  let focus_first_field = matches!(
                     msg,
                     settings::Messages::ChangePassword(settings::ChangePasswordMessage::Model(
                        ChangePasswordCommand::GoTo(Screen::OtpDisplay),
                     )),
                  );

                  let update_cmd = settings.update(msg);

                  if focus_first_field {
                     let initial_focus_cmd = text_input::focus(otp_display::FIRST_FOCUS_ID.clone());

                     Command::batch([initial_focus_cmd, update_cmd])
                  }
                  else {
                     update_cmd
                  }
               }
            }
         }
      }
   }

   fn view(&self) -> Element<'_, Message, Renderer<FastOtpTheme>> {
      let content: Element<'_, Message, Renderer<FastOtpTheme>> = match self.state {
         AppState::Locked { ref unlocking } => unlocking.view(),
         AppState::Unlocked {
            ref create_entry,
            ref otp_display,
            ref settings,
         } => {
            match self.current_screen {
               Screen::CreateEntry => create_entry.view(),
               Screen::OtpDisplay => otp_display.view(),
               Screen::Unlocking => unreachable!(),
               Screen::ChangePassword => settings.view(),
            }
         }
      };

      Container::new(content)
         .width(Length::Fill)
         .height(Length::Fill)
         .align_x(Horizontal::Center)
         .align_y(Vertical::Top)
         .padding(24)
         .into()
   }

   fn subscription(&self) -> Subscription<Message> {
      match self.state {
         AppState::Locked { ref unlocking } => unlocking.subscription(self.current_screen),
         AppState::Unlocked {
            ref otp_display,
            ref create_entry,
            ref settings,
         } => {
            let create_entry_events = create_entry.subscription(self.current_screen);
            let otp_display_events = otp_display.subscription(self.current_screen);
            let settings_events = settings.subscription(self.current_screen);

            Subscription::batch(vec![create_entry_events, otp_display_events, settings_events])
         }
      }
   }
}

/// Helper type to represent a single shared instance of the logos paths of each entry.
/// It exists as a shared state since we continue the initialization of the application as normal and
/// at the same time we add the logos later. This has as consequence that for a brief moment the logs
/// may not appear on screen if there are too many, in practice should be very rare.
pub type SharedLogosPaths = Arc<RwLock<HashMap<entry::Id, String>>>;

impl FastOtpIcedGUI {
   /// Uses the received shared resources to initialize all the dependencies of the application.
   /// Returns a shared reference to the cache of logos' paths to be filled
   /// up by an [Command].
   fn unlocked_initialization(
      &mut self,
      repository: SharedRepository,
      storage: SharedStorage,
   ) -> SharedLogosPaths {
      let logos_paths = Arc::new(RwLock::new(HashMap::new()));

      let create_entry_presenter = EntryCreation::new(
         EntryCreationUseCase::new(Arc::clone(&repository)),
         EntryDeletion::new(Arc::clone(&repository)),
         QrGeneration::new(),
      );

      let otp_display_presenter = OtpDisplay::new(OtpGeneration::new(Arc::clone(&repository)));

      let change_password = ChangePassword::new(ChangePasswordUseCase::new(repository, storage));

      self.state = AppState::Unlocked {
         create_entry: create_entry::Module::new(create_entry_presenter, Arc::clone(&logos_paths)),
         otp_display: otp_display::Module::new(otp_display_presenter, Arc::clone(&logos_paths)),
         settings: settings::Module::new(settings::ChangePasswordModule::new(change_password)),
      };

      logos_paths
   }
}

struct OpenedFile {
   file: std::fs::File,
   is_new: bool,
}

/// Creates or opens the main storage file used to persists user data
fn open_entries_file() -> Result<OpenedFile, std::io::Error> {
   use std::{fs::OpenOptions, ops::Not};

   let mut file_path = std::env::current_dir().unwrap();
   file_path.push("secrets.fastotp");

   // Ignoring https://en.wikipedia.org/wiki/Time-of-check_to_time-of-use because nothing else should
   // create the application file other than the application itself.
   let is_new = file_path.exists().not();

   let file = OpenOptions::new()
      .append(false)
      .read(true)
      .write(true)
      .create(true)
      .open(file_path)?;

   Ok(OpenedFile { file, is_new })
}
