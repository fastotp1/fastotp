/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! See [IoEvents]

use std::{rc::Rc, sync::RwLock};

use futures::{stream::BoxStream, StreamExt};
use iced::{event::Status, Event as InputEvent};
use iced_core::Hasher;
use iced_runtime::futures::subscription::Recipe;
use presentation::{
   otp_display::{Action, Command, OtpDisplay as Presenter},
   Screen,
};

/// Controls the consumption of IO events (mouse and keyboard) of screen to display TOTP codes functionality.
/// Additionally it also listens to the events stream of the presenter
pub struct IoEvents {
   pub mute_io_events: bool,
   pub presenter: Rc<RwLock<Presenter>>,
}

impl Recipe for IoEvents {
   type Output = Command;

   fn hash(&self, state: &mut Hasher) {
      use std::hash::Hash;

      self.mute_io_events.hash(state);
      "OtpDisplay.CommandsSubscription".hash(state);
   }

   fn stream(self: Box<Self>, input: BoxStream<'_, (InputEvent, Status)>) -> BoxStream<'_, Self::Output> {
      use futures::stream::select;
      use iced_core::keyboard::{Event, KeyCode, Modifiers};

      let presenter_events = self.presenter.read().unwrap().update_events();

      if self.mute_io_events {
         return presenter_events;
      }

      let input_as_commands = input.filter_map(move |(io_event, _)| {
         const NO_MODIFIER: Modifiers = Modifiers::empty();

         let cmd = if let InputEvent::Keyboard(Event::KeyReleased {
            key_code,
            modifiers: active_modifiers @ (Modifiers::CTRL | NO_MODIFIER),
         }) = io_event
         {
            if active_modifiers.control() {
               if key_code == KeyCode::Enter {
                  Some(Command {
                     action: Some(Action::EditEntryAt {
                        position: 0,
                        requires_single_otp: true,
                     }),
                     go_to: None,
                  })
               }
               else if key_code == KeyCode::N {
                  Some(Command {
                     action: None,
                     go_to: Some(Screen::CreateEntry),
                  })
               }
               else {
                  None
               }
            }
            else if key_code == KeyCode::Escape {
               Some(Command {
                  action: Some(Action::CleanFilter),
                  go_to: None,
               })
            }
            else if key_code == KeyCode::Enter {
               Some(Command {
                  action: Some(Action::TryCopyOtpAt {
                     position: 0,
                     requires_single_otp: true,
                  }),
                  go_to: None,
               })
            }
            else {
               None
            }
         }
         else {
            None
         };

         async move { cmd }
      });

      select(input_as_commands, presenter_events).boxed()
   }
}
