/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! GUI for rendering a single TOTP code: logo, entry information, code, and actions.

use iced::{
   alignment::{Horizontal, Vertical},
   widget::{Button, Column, Container, Row, Text},
   Alignment,
   Element,
   Length,
   Renderer,
};
use iced_core::Color;
use iced_widget::{Space, Svg};
use presentation::otp_display::{Action, Command, OtpData};

use crate::{
   application::SharedLogosPaths,
   fonts,
   gray_text_color,
   icons,
   logos::Logos,
   otp_display::Message,
   theme,
   theme::FastOtpTheme,
};

/// Contains all the view states (filter state, scroll position, TOTP codes grid tile elements, etc.)
#[derive(Clone, Debug)]
pub(super) struct Tile {
   abs_position: usize,
   logos_paths_cache: SharedLogosPaths,
   edit_label: String,
   otp_data: OtpData,
   selected_color_fade_out: f32,
}

impl Tile {
   pub fn new(
      abs_position: usize,
      logos_paths_cache: SharedLogosPaths,
      edit_label: String,
      otp_data: OtpData,
      selected_color_fade_out: f32,
   ) -> Self {
      Self {
         abs_position,
         logos_paths_cache,
         edit_label,
         otp_data,
         selected_color_fade_out,
      }
   }

   pub fn view(self) -> Element<'static, Message, Renderer<FastOtpTheme>> {
      const LOGO_SIZE: f32 = 44.0;

      let svg: Element<'_, Message, Renderer<FastOtpTheme>> = self
         .logos_paths_cache
         .read()
         .unwrap()
         .get(&self.otp_data.entry_id)
         .map_or_else(
            || {
               Container::new(Text::new(""))
                  .width(Length::Fixed(LOGO_SIZE))
                  .height(Length::Fixed(LOGO_SIZE))
                  .into()
            },
            |filename| {
               use iced_core::svg::Handle;

               let bytes = Logos::get(filename).unwrap().data.into_owned();

               Svg::new(Handle::from_memory(bytes))
                  .width(Length::Fixed(LOGO_SIZE))
                  .height(Length::Fixed(LOGO_SIZE))
                  .into()
            },
         );

      let col = Column::new()
         .align_items(Alignment::Start)
         .spacing(4)
         .push(
            Text::new(self.otp_data.issuer)
               .font(fonts::MULI_SEMIBOLD)
               .size(20)
               .style(Color::BLACK)
               .horizontal_alignment(Horizontal::Left),
         )
         .push(
            Text::new(self.otp_data.description)
               .width(Length::Fill)
               .font(fonts::MULI_REGULAR)
               .size(17)
               .style(gray_text_color!())
               .horizontal_alignment(Horizontal::Left),
         )
         .push(
            Button::new(
               Row::new()
                  .align_items(Alignment::Start)
                  .padding(0)
                  .spacing(4)
                  .push(
                     Text::new(self.otp_data.otp)
                        .font(fonts::MULI_BOLD)
                        .size(17)
                        .horizontal_alignment(Horizontal::Left),
                  )
                  .push(
                     Column::new()
                        .align_items(Alignment::Start)
                        .padding(0)
                        .spacing(0)
                        .push(Space::new(Length::Fill, Length::Fixed(3.0)))
                        .push(icons::copy().size(14)),
                  ),
            )
            .padding(0)
            .width(Length::Shrink)
            .on_press(Message::Model(Command {
               action: Some(Action::TryCopyOtpAt {
                  position: self.abs_position,
                  requires_single_otp: false,
               }),
               go_to: None,
            }))
            .style(theme::Buttons::OtpCode),
         );

      let icon_and_edit_col = Column::new()
         .align_items(Alignment::Start)
         .spacing(0)
         .push(Space::new(Length::Shrink, Length::Fixed(4.0)))
         .push(svg)
         .push(Space::new(Length::Shrink, Length::Fixed(9.5)))
         .push(
            Button::new(
               Row::new()
                  .align_items(Alignment::Start)
                  .padding(0)
                  .spacing(0)
                  .push(
                     Text::new(self.edit_label)
                        .font(fonts::MULI_REGULAR)
                        .size(16)
                        .horizontal_alignment(Horizontal::Left),
                  )
                  .push(
                     Column::new()
                        .align_items(Alignment::Center)
                        .padding(0)
                        .spacing(0)
                        .push(Space::new(Length::Shrink, Length::Fixed(3.1)))
                        .push(icons::edit().size(12)),
                  ),
            )
            .padding(0)
            .width(Length::Shrink)
            .on_press(Message::Model(Command {
               action: Some(Action::EditEntryAt {
                  position: self.abs_position,
                  requires_single_otp: false,
               }),
               go_to: None,
            }))
            .style(theme::Buttons::OtpCode),
         );

      let row = Row::new()
         .spacing(10)
         .push(icon_and_edit_col)
         .push(col)
         .align_items(Alignment::Start);

      Container::new(row)
         .width(Length::Fill)
         .height(Length::Shrink)
         .align_x(Horizontal::Left)
         .align_y(Vertical::Center)
         .padding(6)
         .style(theme::Container::Tile {
            selected_color_fade_out: self.selected_color_fade_out,
         })
         .into()
   }
}
