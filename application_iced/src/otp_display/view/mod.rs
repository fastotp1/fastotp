/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! GUI rendering of the view for the main screen of the application, that is searching and displaying
//! TOTP generated codes.

mod tile;

use std::{sync::Arc, time::Duration};

use iced::{
   alignment::Horizontal,
   widget::{text_input, Button, Column, Container, Row, Scrollable, Text},
   Alignment,
   Element,
   Length,
   Renderer,
};
use iced_widget::{ProgressBar, Space};
use once_cell::sync::Lazy;
use presentation::{
   otp_display::{Action, Command, OtpsViewModel, ViewModel},
   Screen,
};

use crate::{
   application::SharedLogosPaths,
   fonts,
   gray_text_color,
   icons,
   otp_display::{view::tile::Tile, Message},
   theme,
   theme::FastOtpTheme,
   widget_styling_utils,
};

pub static SEARCH_FIELD_ID: Lazy<text_input::Id> = Lazy::new(text_input::Id::unique);

/// Duration for highlight transition after selecting an TOTP entry
const FADE_OUT_TIME: Duration = Duration::from_millis(500);

/// Contains all the view states (filter state, scroll position, TOTP tile elements, etc.)
/// for the view that renders the application data of the TOTP codes to display
#[derive(Debug, Clone)]
pub(super) struct View {
   logos_paths_cache: SharedLogosPaths,
}

impl View {
   pub fn new(logos_paths_cache: SharedLogosPaths) -> Self {
      Self { logos_paths_cache }
   }

   pub fn render(&self, view_model: ViewModel) -> Element<'_, Message, Renderer<FastOtpTheme>> {
      let search_field = Row::new()
         .align_items(Alignment::Center)
         .padding(0)
         .spacing(8)
         .push(
            widget_styling_utils::input_field(&view_model.labels.search, &view_model.filter)
               .id(SEARCH_FIELD_ID.clone())
               .width(Length::FillPortion(28))
               .on_input(|input| {
                  Message::Model(Command {
                     action: Some(Action::FilterBy(input)),
                     go_to: None,
                  })
               }),
         )
         .push(
            Button::new(icons::settings().size(25))
               .padding(0)
               .width(Length::FillPortion(1))
               .on_press(Message::Model(Command {
                  action: None,
                  go_to: Some(Screen::ChangePassword),
               }))
               .style(theme::Buttons::OtpCode),
         );

      let edit_label = view_model.labels.edit;
      let otps_view: Container<'_, Message, Renderer<FastOtpTheme>> = match view_model.otps {
         OtpsViewModel::SystemTimeBefore1970 => {
            let empty_msg = Text::new(view_model.labels.system_time_backwards)
               .width(Length::Fill)
               .size(40)
               .style(gray_text_color!())
               .horizontal_alignment(Horizontal::Center);

            Container::new(empty_msg).width(Length::Fill).height(Length::Shrink)
         }
         OtpsViewModel::NoResults => {
            let empty_msg = Text::new(view_model.labels.no_results)
               .width(Length::Fill)
               .size(40)
               .style(gray_text_color!())
               .horizontal_alignment(Horizontal::Center);

            Container::new(empty_msg).width(Length::Fill).height(Length::Shrink)
         }
         OtpsViewModel::Results {
            time_range,
            time_left,
            time_left_str,
            otps,
            last_copied_otp,
         } => {
            const NUM_COLS: usize = 5;

            let time_left_as_percentage = time_left / time_range.end();
            let time_left_progress = ProgressBar::new(time_range, time_left)
               .height(Length::Fixed(10.0))
               .width(Length::Fill)
               .style(theme::ProgressBar {
                  percentage_of_h: time_left_as_percentage,
               });

            let time_left_label = Text::new(time_left_str)
               .font(fonts::MULI_SEMIBOLD)
               .size(25)
               .style(gray_text_color!())
               .horizontal_alignment(Horizontal::Center);

            let mut time_left_and_otps = Column::new()
               .align_items(Alignment::Center)
               .spacing(12)
               .push(time_left_label)
               .push(time_left_progress)
               .push(Space::new(Length::Fill, Length::Fixed(3.0)));

            let show_single_otp_help_msg = otps.len() == 1;
            let copy_highlight_fade_out = last_copied_otp.map(|(when, otp)| {
               let fade_out = if when.elapsed() <= FADE_OUT_TIME {
                  when.elapsed().as_millis() as f32 / FADE_OUT_TIME.as_millis() as f32
               }
               else {
                  0.0
               };

               (fade_out, otp)
            });
            let selected = copy_highlight_fade_out.as_ref();

            let logos_paths_cache = Arc::clone(&self.logos_paths_cache);

            let otps_grid: Element<'_, Message, Renderer<FastOtpTheme>> = otps
               .into_iter()
               .enumerate()
               .map(|(i, otp_data)| {
                  let fade_out = selected
                     .filter(|(_, selected_otp)| selected_otp == &otp_data)
                     .map_or_else(|| 0.0f32, |(fade_out, _)| *fade_out);

                  Tile::new(
                     i,
                     Arc::clone(&logos_paths_cache),
                     edit_label.clone(),
                     otp_data,
                     fade_out,
                  )
               })
               .collect::<Vec<_>>()
               .chunks(NUM_COLS)
               .map(<[Tile]>::to_vec)
               .fold(
                  Column::new().align_items(Alignment::Start).spacing(30),
                  |col, elements| {
                     col.push(
                        elements
                           .into_iter()
                           .map(Tile::view)
                           .fold(Row::new().spacing(30).align_items(Alignment::Center), Row::push),
                     )
                  },
               )
               .into();

            time_left_and_otps = time_left_and_otps.push(Scrollable::new(otps_grid));

            if show_single_otp_help_msg {
               time_left_and_otps = time_left_and_otps.push(widget_styling_utils::info_message_text(
                  view_model.labels.informative_messages,
               ));
            }

            Container::new(time_left_and_otps)
               .width(Length::Fill)
               .height(Length::Shrink)
         }
      };

      let content: Element<'_, _, Renderer<FastOtpTheme>> = Column::new()
         .align_items(Alignment::Center)
         .spacing(12)
         .push(search_field)
         .push(otps_view)
         .into();

      content
   }
}
