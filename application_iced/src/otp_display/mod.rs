/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! A self contained module for the entire TOTP entries display functionality, including GUI rendering using
//! [Iced](https://github.com/hecrj/iced)

mod subscription;
mod view;

use std::{rc::Rc, sync::RwLock};

use iced::{widget::text_input, Element, Renderer, Subscription};
use presentation::{
   otp_display::{Action, Command, OtpDisplay},
   Navigator,
   Screen,
};
pub use view::SEARCH_FIELD_ID as FIRST_FOCUS_ID;

use crate::{
   application::{self, SharedLogosPaths},
   otp_display::view::View,
   theme::FastOtpTheme,
};

/// Permits differentiate between commands that modify GUI-only state vs underlying application state
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum Message {
   Model(Command),
}

impl Navigator for Message {
   fn is_navigation(&self) -> Option<Screen> {
      match *self {
         Self::Model(ref presentation_cmd) => presentation_cmd.is_navigation(),
      }
   }
}

/// Represents the complete functionality for displaying TOTP entries in the application
pub struct Module {
   presenter: Rc<RwLock<OtpDisplay>>,
   view: View,
}

impl Module {
   pub fn new(presenter: OtpDisplay, logos_paths_cache: SharedLogosPaths) -> Self {
      Self {
         presenter: Rc::new(RwLock::new(presenter)),
         view: View::new(logos_paths_cache),
      }
   }

   pub fn update(&mut self, msg: Message) -> iced::Command<application::Message> {
      match msg {
         Message::Model(cmd) => {
            let focus_cmd = if cmd
               == (Command {
                  action: Some(Action::CleanFilter),
                  go_to: None,
               }) {
               text_input::focus(view::SEARCH_FIELD_ID.clone())
            }
            else {
               iced::Command::none()
            };

            let model_cmd = self
               .presenter
               .write()
               .unwrap()
               .interpret(cmd)
               .map_or_else(iced::Command::none, |future| {
                  iced::Command::perform(future, Message::Model)
               })
               .map(application::Message::OtpDisplay);

            iced::Command::batch([focus_cmd, model_cmd])
         }
      }
   }

   pub fn view(&self) -> Element<'_, application::Message, Renderer<FastOtpTheme>> {
      let presenter = self.presenter.read().unwrap();

      self
         .view
         .render(presenter.view_model())
         .map(application::Message::OtpDisplay)
   }

   pub fn subscription(&self, screen: Screen) -> Subscription<application::Message> {
      Subscription::from_recipe(subscription::IoEvents {
         mute_io_events: screen != Screen::OtpDisplay,
         presenter: Rc::clone(&self.presenter),
      })
      .map(Message::Model)
      .map(application::Message::OtpDisplay)
   }
}
