/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

//! Used to set the application icon when built for Windows

#[cfg(windows)]
fn main() {
   winres::WindowsResource::new()
      .set_icon("assets/app_icon_windows.ico")
      .compile()
      .expect("couldn't generate resource file while building executable.");
}

#[cfg(unix)]
fn main() {}
